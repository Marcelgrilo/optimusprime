-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 02, 2015 at 12:25 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `motortools`
--

-- --------------------------------------------------------

--
-- Table structure for table `maintenance_def`
--

CREATE TABLE IF NOT EXISTS `maintenance_def` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `part_name` varchar(50) NOT NULL,
  `months` int(11) NOT NULL,
  `distance` int(11) NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `vehicle_id` (`vehicle_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `maintenance_vehicle`
--

CREATE TABLE IF NOT EXISTS `maintenance_vehicle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `current_km` int(11) NOT NULL,
  `relative_date` int(11) NOT NULL,
  `maintenance_def` int(11) NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `maintenance_def` (`maintenance_def`,`vehicle_id`),
  KEY `vehicle_id` (`vehicle_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `repair_shop`
--

CREATE TABLE IF NOT EXISTS `repair_shop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `phone_number` varchar(15) NOT NULL,
  `street_line_1` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `lat` varchar(100) NOT NULL,
  `long` varchar(100) NOT NULL,
  `cellphone` varchar(15) NOT NULL,
  `cep` varchar(8) NOT NULL,
  `number` varchar(10) NOT NULL,
  `street_line_2` varchar(255) NOT NULL,
  `country` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `email` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `phone_number` varchar(12) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phone_number` (`phone_number`,`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_def`
--

CREATE TABLE IF NOT EXISTS `vehicle_def` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand` varchar(35) NOT NULL,
  `model` varchar(35) NOT NULL,
  `year` year(4) NOT NULL,
  `version` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `vehicle_def`
--

INSERT INTO `vehicle_def` (`id`, `brand`, `model`, `year`, `version`) VALUES
(1, 'Fiat', 'Palio Weekend', 2014, '1.6 Manual'),
(2, 'Honda', 'Civic', 2014, 'Sedan LXS 1.8 Flex 16v automático 4p');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_user`
--

CREATE TABLE IF NOT EXISTS `vehicle_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `km` int(11) NOT NULL,
  `electric_windows` tinyint(1) NOT NULL,
  `electric_lock` tinyint(1) NOT NULL,
  `plate` varchar(7) NOT NULL,
  `purchase_year` year(4) NOT NULL,
  `color` varchar(15) NOT NULL,
  `doors` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `definition_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `plate` (`plate`),
  KEY `owner_id` (`owner_id`,`definition_id`),
  KEY `definition_id` (`definition_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `maintenance_vehicle`
--
ALTER TABLE `maintenance_vehicle`
  ADD CONSTRAINT `maintenance_definition` FOREIGN KEY (`maintenance_def`) REFERENCES `maintenance_def` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `vehicle_maintenance` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicle_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `vehicle_user`
--
ALTER TABLE `vehicle_user`
  ADD CONSTRAINT `vehicle_definition` FOREIGN KEY (`definition_id`) REFERENCES `vehicle_def` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `vehicle_ownership` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
