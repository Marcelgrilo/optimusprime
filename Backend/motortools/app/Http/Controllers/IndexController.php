<?php namespace App\Http\Controllers;

use App\Vehicle;

class IndexController extends Controller 
{
	/**
	 * Create a new controller instance.
	 *
	 * @return string
	 */
	public function getUrl()
	{
		return json_encode(array('Marcas'=>'/index/brands',
								 'Modelos'=>'/index/models/*marca*',
								 'Anos'=>'/index/years/*marca*/*modelo*',
								 'Versão'=>'/index/years/*marca*/*modelo*/*ano*',
			));
	}

	public function getBrands()
	{
		return response()->json(Vehicle::getBrands());
	}

	public function postModels()
	{
		$brand = \Input::get('brand');

		return response()->json(Vehicle::getModels($brand));
	}

	public function postYears()
	{
		$brand = \Input::get('brand');

		$model = \Input::get('model');

		return response()->json(Vehicle::getYears($brand, $model));
	}

	public function postVersions()
	{
		$brand = \Input::get('brand');

		$model = \Input::get('model');

		$year =  \Input::get('year');

		return response()->json(Vehicle::getVersions($brand, $model, $year));
	}


	public function postVehicleId()
	{
		$brand = \Input::get('brand');

		$model = \Input::get('model');

		$year =  \Input::get('year');

		$version = \Input::get('version');

		return response()->json(Vehicle::getVehicleId($brand, $model, $year, $version));
	}

	public function postMaintenaceDefinitions()
	{

		$car_id = \Input::get('car_id');

		return response()->json(Vehicle::getMaintenaceDefinitions($car_id));
	}
}