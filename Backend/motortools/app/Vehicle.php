<?php namespace App;


class Vehicle
{
	public static function getBrands()
	{
		$result = \DB::table('vehicle_def')->select('brand')->get();

		return $result;
	}

	public static function getModels($brand)
	{
		$result = \DB::table('vehicle_def')->select('model')->distinct()->where('brand', '=', $brand)->get();

		return $result;
	}

	public static function getYears($brand, $model)
	{
		$result = \DB::table('vehicle_def')->select('year')->distinct()->where('brand', '=', $brand)->where('model', '=', $model)->get();

		return $result;
	}

	public static function getVersions($brand, $model, $year)
	{
		$result = \DB::table('vehicle_def')->select('version')->distinct()->where('brand', '=', $brand)->where('model', '=', $model)->where('year', '=', $year)->get();

		return $result;
	}

	public static function getVehicleId($brand, $model, $year, $version)
	{
		$result = \DB::table('vehicle_def')->select('id')->distinct()->where('brand', '=', $brand)->where('model', '=', $model)->where('year', '=', $year)->where('version', '=', $version)->get();

		return $result;
	}

	public static function getMaintenanceDefinitions($car_id)
	{
		$result = \DB::table('maintenance_def')->where('vehicle_id', '=', $car_id)->get();

		return $result;
	}
}
