﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Threading;


public class CalendarController : MonoBehaviour
{
		System.DateTime now;

		int month = 0;


		void OnEnable ()
		{
				month = 0;
				Test ();

		}


		public void NextMonth ()
		{
				month++;
				Test ();
		}


		public void PrevMonth ()
		{
				month--;
				Test ();
		}


		void Test ()
		{

				// reset the labels
				foreach (Transform t in transform)
				{
						t.GetComponent<Text> ().text = "-";
				}


				now = System.DateTime.Now;
				Debug.Log ("now " + now.ToString ());

				//count from day 01 of this month
				var i = now.AddMonths (month);
				Debug.Log ("i " + i.ToString ());
				var j = now.AddMonths (month);
				Debug.Log ("j " + j.ToString ());
				//var i = now.AddDays (-( now.Day - 1 ));
				//i = now.AddDays (-( i.Day ));
				i = i.AddDays (-( i.Day - 1 ));

				Debug.Log ("i " + i.ToString ());

				// get the number of the day of the weak to populate the calendar form the correct day.
				int count = GetDayOfTheWeak (i.DayOfWeek);
				Debug.Log (i.DayOfWeek + "   " + count);

				while (i.Month == j.Month)
				{
						//i.DayOfWeek;
						Debug.Log (i + "  " + ( i.DayOfWeek ) + "   " + GetMonthIndex (count) + "  " + count);
						transform.GetChild (GetMonthIndex (count)).GetComponent<Text> ().text = i.DayOfWeek.ToString () + "\n" + i.Day;


						i = i.AddDays (1);

						count++;
						// NGUI add a new button for this day
				}

				// NGUI reposition all buttons with UIGrid

		}


		static int GetDayOfTheWeak (System.DayOfWeek day)
		{
				switch (day)
				{
						default:
						case System.DayOfWeek.Monday:
								return 0;
						case System.DayOfWeek.Tuesday:
								return 1;
						case System.DayOfWeek.Wednesday:
								return 2;
						case System.DayOfWeek.Thursday:
								return 3;
						case System.DayOfWeek.Friday:
								return 4;
						case System.DayOfWeek.Saturday:
								return 5;
						case System.DayOfWeek.Sunday:
								return 6;
				}
		}


		static int GetMonthIndex (int value)
		{
				return ( ( ( value / 7 ) * 7 ) + ( value % 7 ) );
		}

		// Update is called once per frame
		void Update ()
		{
	
		}
}
