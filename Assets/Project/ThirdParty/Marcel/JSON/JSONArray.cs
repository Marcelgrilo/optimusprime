﻿namespace Marcel.JSON
{


		using System.Collections;
		using System.Collections.Generic;


		public class JSONArray : JSONNode, IEnumerable
		{


				private List<JSONNode> m_List = new List<JSONNode> ();


				public override JSONNode this [int aIndex]
				{
						get
						{
								if (aIndex < 0 || aIndex >= m_List.Count)
										return new JSONLazyCreator (this);
								return m_List [aIndex];
						}
						set
						{
								if (aIndex < 0 || aIndex >= m_List.Count)
										m_List.Add (value);
								else
										m_List [aIndex] = value;
						}
				}


				public override JSONNode this [string aKey]
				{
						get{ return new JSONLazyCreator (this); }
						set{ m_List.Add (value); }
				}


				public override int Count
				{
						get { return m_List.Count; }
				}


				public override void Add (string aKey, JSONNode aItem)
				{
						m_List.Add (aItem);
				}


				public override JSONNode Remove (int aIndex)
				{
						if (aIndex < 0 || aIndex >= m_List.Count)
								return null;
						JSONNode tmp = m_List [aIndex];
						m_List.RemoveAt (aIndex);
						return tmp;
				}


				public override JSONNode Remove (JSONNode aNode)
				{
						m_List.Remove (aNode);
						return aNode;
				}


				public override IEnumerable<JSONNode> Children
				{
						get
						{
								foreach (JSONNode N in m_List)
										yield return N;
						}
				}


				public IEnumerator GetEnumerator ()
				{
						foreach (JSONNode N in m_List)
								yield return N;
				}


				public override string ToString ()
				{
						string result = "[ ";
						foreach (JSONNode N in m_List)
						{
								if (result.Length > 2)
										result += ", ";
								result += N.ToString ();
						}
						result += " ]";
						return result;
				}


				public override string ToString (string aPrefix)
				{
						string result = "[ ";
						foreach (JSONNode N in m_List)
						{
								if (result.Length > 3)
										result += ", ";
								result += "\n" + aPrefix + "   ";
								result += N.ToString (aPrefix + "   ");
						}
						result += "\n" + aPrefix + "]";
						return result;
				}


				public override string ToJSON (int prefix)
				{
						string s = new string (' ', ( prefix + 1 ) * 2);
						string ret = "[ ";
						foreach (JSONNode n in m_List)
						{
								if (ret.Length > 3)
										ret += ", ";
								ret += "\n" + s;
								ret += n.ToJSON (prefix + 1);

						}
						ret += "\n" + s + "]";
						return ret;
				}


				public override void Serialize (System.IO.BinaryWriter aWriter)
				{
						aWriter.Write ((byte)JSONBinaryTag.Array);
						aWriter.Write (m_List.Count);
						for (int i = 0; i < m_List.Count; i++)
						{
								m_List [i].Serialize (aWriter);
						}
				}
		}
}