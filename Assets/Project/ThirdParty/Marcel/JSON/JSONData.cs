﻿namespace Marcel.JSON
{


		using System;


		public class JSONData : JSONNode
		{
				private string m_Data;


				public override string Value
				{
						get { return m_Data; }
						set
						{
								m_Data = value;
								Tag = JSONBinaryTag.Value;
						}
				}


				public JSONData (string aData)
				{
						m_Data = aData;
						Tag = JSONBinaryTag.Value;
				}


				public JSONData (float aData)
				{
						AsFloat = aData;
				}


				public JSONData (double aData)
				{
						AsDouble = aData;
				}


				public JSONData (bool aData)
				{
						AsBool = aData;
				}


				public JSONData (int aData)
				{
						AsInt = aData;
				}


				public override string ToString ()
				{
						return "\"" + Escape (m_Data) + "\"";
				}


				public override string ToString (string aPrefix)
				{
						return "\"" + Escape (m_Data) + "\"";
				}


				public override string ToJSON (int prefix)
				{
						switch (Tag)
						{
								case JSONBinaryTag.DoubleValue:
								case JSONBinaryTag.FloatValue:
								case JSONBinaryTag.IntValue:
										return m_Data;
								case JSONBinaryTag.Value:
										return string.Format ("\"{0}\"", Escape (m_Data));
								default:
										throw new NotSupportedException ("This shouldn't be here: " + Tag.ToString ());
						}
				}


				public override void Serialize (System.IO.BinaryWriter aWriter)
				{
						var tmp = new JSONData ("");

						tmp.AsInt = AsInt;
						if (tmp.m_Data == this.m_Data)
						{
								aWriter.Write ((byte)JSONBinaryTag.IntValue);
								aWriter.Write (AsInt);
								return;
						}
						tmp.AsFloat = AsFloat;
						if (tmp.m_Data == this.m_Data)
						{
								aWriter.Write ((byte)JSONBinaryTag.FloatValue);
								aWriter.Write (AsFloat);
								return;
						}
						tmp.AsDouble = AsDouble;
						if (tmp.m_Data == this.m_Data)
						{
								aWriter.Write ((byte)JSONBinaryTag.DoubleValue);
								aWriter.Write (AsDouble);
								return;
						}

						tmp.AsBool = AsBool;
						if (tmp.m_Data == this.m_Data)
						{
								aWriter.Write ((byte)JSONBinaryTag.BoolValue);
								aWriter.Write (AsBool);
								return;
						}
						aWriter.Write ((byte)JSONBinaryTag.Value);
						aWriter.Write (m_Data);
				}
		}


}