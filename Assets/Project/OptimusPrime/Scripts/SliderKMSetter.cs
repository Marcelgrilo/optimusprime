﻿using System.Collections.Generic;
using MotorTools.Controllers;
using MotorTools.Data;
using MotorTools.Models;
using MotorTools.Views;

namespace MotorTools.Utils
{
	using UnityEngine;

	public class SliderKMSetter : MonoBehaviour, IVehicleListenner
	{
		public UILabel CurrentKmLabel;
		public UISlider Slider;

		private int myKmIncrement = 5000;
		private int myKmSlots = 5;
		private DashboardView myDashboarView;

		public void OnCurrentKmChange (float value)
		{
			//Debug.Log("CurrentKmChanged");
			//var vehicle = VehicleController.Instance.SelectedVehiacle;
			StartLabels();
			// Debug.Log (vehiacle.CurrentKm.ToString ()); 
			//Vector2 interval = GetIncrementInterval ((int) vehicle.CurrentKm);
			//Slider.value = Utils.percentage (interval.x, interval.y, vehicle.CurrentKm);
		}

		public void OnPerDayKmChange (float value)
		{
		}



		public void OnValueChange (float value)
		{
			var vehicle = VehicleController.Instance.SelectedVehiacle;

			vehicle.CalendarSliderKm = value * myKmIncrement * myKmSlots + GetIncrementInterval ((int) vehicle.CurrentKm).x;

			CurrentKmLabel.text = "Revisar em "+((int) vehicle.CalendarSliderKm) + " Kms";//Localization.Get (Strings.Km)"";

			if (myDashboarView == null)
			{
				myDashboarView = GameObject.FindObjectOfType<DashboardView>();
			}
			myDashboarView.UpdateRevisionItems();
			
			//VehicleController.Instance.SelectedVehiacle.RevisionsOnKm((int) vehicle.CalendarSliderKm);
			
		}



		public void StartLabels ()
		{
			var vehicle = VehicleController.Instance.SelectedVehiacle;

			Vector2 interval = GetIncrementInterval ((int) vehicle.CurrentKm);

			int incValue = (int) interval.x;
			foreach (Transform t in transform)
			{
				t.GetComponent<UILabel> ().text = incValue.ToString ();
				incValue += myKmIncrement;
			}

			CurrentKmLabel.text = (vehicle.CurrentKm) + Localization.Get (Strings.KmsAtual);
			Slider.value = Utils.percentage (interval.x, interval.y, vehicle.CurrentKm);
		}



		private Vector2 GetIncrementInterval (int value)
		{
			int gap = Mathf.Max( 0, ((value / myKmIncrement) - 1) );

			int a = value / (myKmSlots * myKmIncrement) + 1;
			int min =  ((myKmSlots * myKmIncrement * a) - (myKmSlots * myKmIncrement));
			min = min < 0 ? 0 : min;

			return new Vector2(min + gap * myKmIncrement, myKmSlots * myKmIncrement * a + gap * myKmIncrement);
		}



		private void OnDisable ()
		{
			VehicleController.Instance.SelectedVehiacle.RemoveCurrentKmListener (this);
			VehicleController.Instance.SelectedVehiacle.RemovePerDayKmListener (this);
			Slider.onDragFinished -= OnDragFinished;
		}



		private void OnDragFinished ()
		{
			//Debug.Log ("Slider Drag Finished");
		}



		private void OnEnable ()
		{
			Slider.onDragFinished += OnDragFinished;
			VehicleController.Instance.SelectedVehiacle.AddCurrentKmListener (this);
			VehicleController.Instance.SelectedVehiacle.AddPerDayKmListener (this);
			StartLabels ();
			OnCurrentKmChange (VehicleController.Instance.SelectedVehiacle.CurrentKm);
			OnPerDayKmChange (VehicleController.Instance.SelectedVehiacle.PerDayKm);
		}
	}
}