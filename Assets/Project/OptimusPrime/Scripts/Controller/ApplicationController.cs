﻿namespace MotorTools.Controllers
{


	using UnityEngine;
	using System.Collections;
	using MotorTools.Models;
	using MotorTools.Views;
	using MotorTools.Data;
	using System.Collections.Generic;
	using Marcel.JSON;

	public class ApplicationController : Singleton<ApplicationController>
	{

		#region AppStartupData
		public LoginView FirstView;
		public IView TestView;
		#endregion AppStartupData



		#region UserData
		/// <summary>
		/// TODO: colocar esta classe nos modelos.
		/// </summary>
		public class User
		{
			public LocationInfo CurrentLocation
			{ get; set; }

			public string Name
			{ get; set; }

			public string Phone
			{ get; set; }

			public string Email
			{ get; set; }

			public string Password
			{ get; set; }
		}
		public User CurrentUser
		{ get; set; }
		#endregion UserData







		#region SelectedRepairShop
		private RepairShop _selectedRepairShop;
		public RepairShop SelectedRepairShop
		{
			get
			{
				if (_selectedRepairShop == null)
					SelectedRepairShop = new RepairShop(
						"-1",
						"Test RepairShop",
						"TestDescription",
						"000000000", "", "", "", "", "", "", "", "", "", "", "", "");
				return _selectedRepairShop;
			}
			set
			{
				_selectedRepairShop = value;
			}
		}

		public void SelectRepairShop(string id)
		{
			StartCoroutine(CoSelectRepairShop(id));
		}
		public IEnumerator CoSelectRepairShop(string id)
		{
			//
			// inicia a tela de loading.
			LoadingView.Instance.Enable();
			//
			// inicializando os forms.
			var form = new WWWForm();
			form.AddField(Strings.Id, id);
			//
			// monta a requisicao com url e formulario.
			var request = new WWW(URLs.BaseURL + URLs.Slash + URLs.RepairShop, form);
			//
			// espera pela requisicao.
			yield return request;
			// se retornou  erro ou vazio para.
			if (string.IsNullOrEmpty(request.text) || !string.IsNullOrEmpty(request.error))
				yield break;
			//
			// cria o repairshop.
			RepairShop r = RepairShop.CreateRepairShop(JSON.Parse(request.text));
			//
			// se nao conseguiu criar o repairshop para
			if (r == null)
				yield break;
			//
			// seleciona a repairshop.
			SelectedRepairShop = r;
			//
			// Sai da tela de loading
			LoadingView.Instance.Disable();
		}
		#endregion SelectedRepairShop



		#region UserRegistering
		public int KMPerDay
		{
			get
			{
				return PlayerPrefs.GetInt("UserRegisteringKMPerDay", 0);
			}
			set
			{
				PlayerPrefs.SetInt("UserRegisteringKMPerDay", value);
			}
		}
		/// <summary>
		/// Classe representa o item de revisao inserido pelo usuario.
		/// </summary>
		public class UserRegisterItem
		{
			public string Id;
			public string MaintenaceType;
			public string Name;
			public string Description;
			public string Km;
			public string Month;
			public string CarId;

			public JSONNode ToJsonNode()
			{
				JSONNode node = new JSONClass();
				node.Add("id", Id);
				node.Add("type", MaintenaceType);
				node.Add("name", Name);
				node.Add("description", Description);
				node.Add("km", Km);
				node.Add("month", Month);
				node.Add("carId", CarId);
				return node;
			}
			public static UserRegisterItem CreateFromJson(JSONNode node)
			{
				try
				{
					UserRegisterItem item = new UserRegisterItem();
					item.Id             = node["id"].Value;
					item.MaintenaceType = node["type"].Value;
					item.Name           = node["name"].Value;
					item.Description    = node["description"].Value;
					item.Km             = node["km"].Value;
					item.Month          = node["month"].Value;
					item.CarId          = node["carId"].Value;
					return item;
				}
				catch
				{

				}
				return null;
			}
		}

		/// <sumary>
		/// lista de itens registrados pelo usuario.
		/// </sumary>
		List<UserRegisterItem> myUserRegisterItems;
		public List<UserRegisterItem> UserRegisterItems
		{
			get
			{
				if (myUserRegisterItems == null)
				{
					myUserRegisterItems = new List<UserRegisterItem>();
				}
				return myUserRegisterItems;
			}

		}


		/// <sumary>
		/// Adiciona um item de revisao na lsita de itens de revisao de usuarios.
		/// </sumary>
		public IEnumerator AddUserRegisterItem(UserRegisterItem item)
		{
			if (string.IsNullOrEmpty(item.Id))
			{
				item.Id = Strings.GetRevisionIconsIndex(item.MaintenaceType) + VehicleController.Instance.SelectedVehiacle.CarId + item.Km + item.Month;
			}
			item.CarId = VehicleController.Instance.SelectedVehiacle.CarId;

			UserRegisterItems.Add(item);
			SaveUserRegisterItems();
			yield return null;
		}


		/// <sumary>
		/// lista de itens registrados pelo usuario.
		/// </sumary>
		public void SaveUserRegisterItems()
		{
			int i = 0;
			PlayerPrefs.SetInt("UserRegisterItemCount", UserRegisterItems.Count);
			foreach (UserRegisterItem item in UserRegisterItems)
			{
				PlayerPrefs.SetString("UserRegisterItem:" + i, item.ToJsonNode().ToString());
				i++;
			}
		}
		public void LoadUserRegisterItems()
		{
			myUserRegisterItems = new List<UserRegisterItem>();
			PlayerPrefs.GetInt("UserRegisterItemCount", 0);
			for (int i = 0; i < PlayerPrefs.GetInt("UserRegisterItemCount", 0); i++)
			{
				UserRegisterItem item = UserRegisterItem.CreateFromJson(JSON.Parse(PlayerPrefs.GetString("UserRegisterItem:" + i, "")));
				if (item != null)
				{
					myUserRegisterItems.Add(item);
				}
			}
		}
		public void JSonTest()
		{
			UserRegisterItem item = new UserRegisterItem();
			item.Id = "1";
			item.MaintenaceType = "ico-forracao-interna-bancos";
			item.Name = "aaasdf";
			item.Description = "asd";
			item.Km = "55";
			item.Month = "3";
			item.CarId = "11";
			Debug.Log(item.ToJsonNode().ToString());
			UserRegisterItem item2 = UserRegisterItem.CreateFromJson(JSON.Parse(item.ToJsonNode().ToString()));
			Debug.Log((item2 == null) + " " + item.Id);
		}
		#endregion UserRegistering



		// Use this for initialization
		void Start()
		{
			Localization.language = Strings.Ptbr;
			LoadingView.Instance.Enable();
			LoadUserRegisterItems();
			StartCoroutine(VerifyQuit());
			StartCoroutine(Initialize());
		}



		IEnumerator Initialize()
		{
			//yield return new WaitForSeconds (5.0f);

			if (TestView != null)
				TestView.ActivateView();
			else
				FirstView.ActivateView();

			//
			//TestFor the webcamView.
			/*{
				Debug.Log("%%%%%%%%%%%%%%%% 0");
				yield return new WaitForSeconds(5);
				WebcamView.Instance.GetType();
				Debug.Log("%%%%%%%%%%%%%%%% 1");
				WebcamView.Instance.Enable();

				yield return new WaitForSeconds(15);
				Debug.Log("%%%%%%%%%%%%%%%% 2");
				WebcamView.Instance.Disable();

				yield return new WaitForSeconds(5);
				Debug.Log("%%%%%%%%%%%%%%%% 3");
				WebcamView.Instance.Enable();

				yield return new WaitForSeconds(5);
				Debug.Log("%%%%%%%%%%%%%%%% 4");
				WebcamView.Instance.OnSwitchCameraButton();

				yield return new WaitForSeconds(5);
				Debug.Log("%%%%%%%%%%%%%%%% 5");
				WebcamView.Instance.Disable();

				yield return new WaitForSeconds(5);
			}*/
			yield return 0;
		}





		public void SetLocalization(string local)
		{
			Localization.language = local;
		}




		float quitTimer = 0;
		IEnumerator VerifyQuit()
		{
			while (true)
			{
				if (Input.GetKey(KeyCode.Escape))
				{
					quitTimer += 1;
					if (quitTimer >= 3)
					{
						Application.Quit();
						yield break;
					}
				}
				else
				{
					quitTimer = 0;
				}

				yield return new WaitForSeconds(1);
			}
		}
	}


}
