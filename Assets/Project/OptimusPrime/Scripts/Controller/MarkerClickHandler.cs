﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MotorTools.Views;
using MotorTools.Models;
public class MarkerClickHandler : MonoBehaviour 
{

	private DashboardView db;

	public List<RevisionItem> MarkerInfo  = new List<RevisionItem>();

	void OnEnable()
	{
		EventDelegate.Remove(GetComponent<UIButton>().onClick, OnClick);
		EventDelegate.Add(GetComponent<UIButton>().onClick, OnClick);
	}

	public void OnClick()
	{
		if(db == null)
		{
			db = GameObject.FindObjectOfType<DashboardView>();
		}
		db.CalendarMarkerClicked(MarkerInfo);
	}


}
