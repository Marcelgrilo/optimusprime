﻿using MotorTools.Views;


namespace MotorTools.Controllers
{
	
	
		using UnityEngine;
		using System.Collections;
		using MotorTools.Models;
	
	
		public class VehicleController : Singleton<VehicleController>
		{
		
				Vehicle selectedVehiacle; //= new Vehicle ("a", "b", 1990, "c", 0);
		
		
				public Vehicle SelectedVehiacle
				{
						get
						{
								if (selectedVehiacle == null)
								{
										selectedVehiacle = Vehicle.LoadVehicleLoginData ();
								}
								return selectedVehiacle;
						}
						set
						{
								selectedVehiacle = value;
						}
				}
		
		
		
				// Use this for initialization
				void Start ()
				{
						StartCoroutine (Initialize ());
				}
		
		
				IEnumerator Initialize ()
				{
						yield return 0;
				}

		}
	
	
}
