﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using MotorTools.Controllers;
using MotorTools.Models;
using UnityEngine;

public class NGuiCalendarController : MonoBehaviour
{
	#region Static Colors

	private static readonly Color32 disabledColor = new Color32 (128, 128, 128, 255);
	private static readonly Color32 disabledWeekDayColor = new Color32 (180, 128, 100, 255);
	private static readonly Color32 normalColor = new Color32 (64, 64, 64, 255);
	private static readonly Color32 weekDayColor = new Color32 (212, 76, 0, 255);

	#endregion Static Colors

	#region Properties

	public Transform DayNames;
	public Transform MonthDays;
	public Transform WeekDays;

	[Space (10)]
	public UILabel MonthYearLabel;


	#endregion Properties

	#region Attributes

	private UIWidget calendarWidget;
	private bool isOpened = false;
	private Transform items;

	//int currentMonth;
	private int monthSelectedOnCalendar = 0;

	private UITable table;



	private UIWidget CalendarWidget
	{
		get
		{
			if (calendarWidget == null)
			{
				calendarWidget = GetComponent<UIWidget> ();
			}
			return calendarWidget;
		}
	}



	private Transform Items
	{
		get
		{
			if (items == null)
			{
				items = transform.parent.FindChild ("ItemTable");
			}
			return items;
		}
	}



	private UITable Table
	{
		get
		{
			if (table == null)
			{
				table = transform.parent.GetComponent<UITable> ();
			}
			return table;
		}
	}



	#endregion Attributes
	#region Public Methods



	public void UpdateCalendar ()
	{
		ResetMarkers ();
		Open ();
		Close ();
	}



	public void ChangeState ()
	{
		if (isOpened)
		{
			Close ();
		}
		else
		{
			Open ();
		}
	}
	 


	public void Open ()
	{
		if (isOpened)
		{
			return;
		}
		StartCoroutine (EnableCalendar (true));
	}



	public void Close ()
	{
		if (!isOpened)
		{
			return;
		}
		StartCoroutine (EnableCalendar (false));
	}



	public void NextMonth ()
	{
		if (!isOpened)
		{
			Open ();
		}
		monthSelectedOnCalendar++;
		CalendarSetup (DateTime.Now.AddMonths (monthSelectedOnCalendar));
	}



	public void PreviousMonth ()
	{
		if (!isOpened)
		{
			Open ();
		}
		monthSelectedOnCalendar--;
		CalendarSetup (DateTime.Now.AddMonths (monthSelectedOnCalendar));
	}



	#endregion Public Methods
	#region Methods



	private void ResetMarkers ()
	{
		WeekDays.gameObject.SetActive (true);
		foreach (Transform t in WeekDays)
		{
			t.GetComponentInChildren<UISprite> ().enabled = false;
			t.GetComponentInChildren<UIButton>().enabled = false;
			t.GetComponentInChildren<BoxCollider>().enabled = false;
			t.GetComponentInChildren<MarkerClickHandler>().MarkerInfo.Clear();
		}
		WeekDays.gameObject.SetActive (false);
		MonthDays.gameObject.SetActive (true);
		foreach (Transform t in MonthDays)
		{
			t.GetComponentInChildren<UISprite> ().enabled = false;
			t.GetComponentInChildren<UIButton>().enabled = false;
			t.GetComponentInChildren<BoxCollider>().enabled = false;
			t.GetComponentInChildren<MarkerClickHandler>().MarkerInfo.Clear();
		}
		MonthDays.gameObject.SetActive (false);
	}



	private IEnumerator EnableCalendar (bool flag)
	{
		isOpened = flag;
		DateTime now = DateTime.Now;

		//currentMonth = now.Month;
		monthSelectedOnCalendar = 0;

		CalendarSetup (now);

		Items.localPosition = new Vector3 (0, flag ? -440 : -640, 0);
		CalendarWidget.SetDimensions (CalendarWidget.width, flag ? 640 : 420);
		CalendarWidget.ResizeCollider ();
		yield return 0;
		Table.Reposition ();
	}



	private void CalendarSetup (DateTime date)
	{
		MonthDays.gameObject.SetActive (isOpened);
		WeekDays.gameObject.SetActive (!isOpened);

		MonthYearLabel.text = Localization.Get (date.ToString ("MMMM", CultureInfo.InvariantCulture).ToLower ()) + "  " + date.ToString ("yyyy", CultureInfo.InvariantCulture);

		DisplayWeekHeader (date);
		if (isOpened)
		{
			DisplayMonth (date);
		}
		else
		{
			DisplayWeek (date);
		}
	}



	private void DisplayMonth (DateTime date)
	{
		int month = date.Month;

		//
		//this line was used to make the entire colum of this week day to be colored.
		//
		//int dayOfWeek = GetDayOfTheWeak (date.DayOfWeek);

		// subtrais os dias para comecar do comeco do mes
		date = date.AddDays (-(date.Day - 1));

		// subtrai os dias da semana para comecar de segunda feira independente de ser fora do mes.
		date = date.AddDays (-GetDayOfTheWeak (date.DayOfWeek));

		//
		//this line was used to make the entire colum of this week day to be colored.
		//
		//int rowCounter = 0;
		foreach (Transform t in MonthDays)
		{
			t.GetComponent<UILabel> ().text = String.Format ("{0:00}", date.Day);
			
			//
			//this line was used to make the entire colum of this week day to be colored.
			//
			//EnableDay (date.Month == month, rowCounter % 7 == dayOfWeek, t, date);

			EnableDay(date.Month == month, date.Day == DateTime.Now.Day && date.Month == DateTime.Now.Month, t, date);

			date = date.AddDays (1);

			//
			//this line was used to make the entire colum of this week day to be colored.
			//
			//rowCounter++;
		}
	}



	private void DisplayWeek (DateTime date)
	{
		int month = date.Month;

		int dayOfWeek = GetDayOfTheWeak (date.DayOfWeek);

		// subtract the number of days to start the calendar labeling and coloring.
		date = date.AddDays (-dayOfWeek);

		foreach (Transform t in WeekDays)
		{
			t.GetComponent<UILabel> ().text = String.Format ("{0:00}", date.Day);

			EnableDay (date.Month == month, dayOfWeek == t.GetSiblingIndex (), t, date);

			date = date.AddDays (1);
		}
	}



	private void DisplayWeekHeader (DateTime date)
	{
		// int month = date.Month;

		int dayOfWeek = GetDayOfTheWeak (date.DayOfWeek);

		date = date.AddDays (-dayOfWeek);

		foreach (Transform t in DayNames)
		{
			SetDayColor (true, dayOfWeek == t.GetSiblingIndex (), t);

			date = date.AddDays (1);
		}
	}



	private void EnableDay (bool enableDay, bool isDayOfWeek, Transform t, DateTime day)
	{
		SetDayColor (enableDay, isDayOfWeek, t);

		// configurando o marcador.
		t.GetComponentInChildren<UISprite> ().enabled = false;
		t.GetComponentInChildren<UIButton>().enabled = false;
		t.GetComponentInChildren<BoxCollider>().enabled = false;
		t.GetComponentInChildren<MarkerClickHandler>().MarkerInfo.Clear();

		List<SchedulableRevision> list = VehicleController.Instance.SelectedVehiacle.RevisionsOnDay(day);

		if (list.Count > 0)
		{
			// ativa o marcador de manutencao se este dia tem manutencao.

			for (int i = 0; i < list.Count; i++)
			{
				t.GetComponentInChildren<MarkerClickHandler>().MarkerInfo.Add(list[i].RevisionItemObject);
			}
			t.GetComponentInChildren<UISprite>().enabled = true;
			t.GetComponentInChildren<UIButton>().enabled = true;
			t.GetComponentInChildren<BoxCollider>().enabled = true;
		}
	}



	private void SetDayColor (bool enableDay, bool isDayOfWeek, Transform t)
	{
		t.GetComponent<UILabel> ().color =
		enableDay ?
		(isDayOfWeek ? weekDayColor : normalColor) :
		(isDayOfWeek ? disabledWeekDayColor : disabledColor);
	}



	/// <summary>
	/// Swipe listener to open and close the calendar.
	/// </summary>
	/// <param name="fingerIndex">the finger index</param>
	/// <param name="startPos"> the start position</param>
	/// <param name="direction">the direction</param>
	/// <param name="velocity">the velocity</param>
	private void OnFingerSwipe(int fingerIndex, Vector2 startPos, FingerGestures.SwipeDirection direction, float velocity)
	{
		Ray ray = transform.parent.parent.parent.GetComponentInChildren<Camera>().ScreenPointToRay(startPos);
		RaycastHit hit;
		
		if (Physics.Raycast(ray, out hit))
		{
			if (hit.collider.gameObject == gameObject)
			{
				if (direction == FingerGestures.SwipeDirection.Down)
				{
					Open();
				}
				else if (direction == FingerGestures.SwipeDirection.Up)
				{
					Close();
				}
			}
		}
	}



	#endregion Methods
	#region MonoBehaviour Methods


	/// <summary>
	/// Start is called on the frame when a script is enabled just 
	/// before any of the Update methods is called the first time.
	/// </summary>
	private void Strat ()
	{
		UpdateCalendar ();
	}



	/// <summary>
	/// This function is called when the object becomes enabled and active.
	/// </summary>
	void OnEnable () 
	{
		FingerGestures.OnFingerSwipe += OnFingerSwipe;
	}



	/// <summary>
	/// This function is called when the behaviour becomes disabled () or inactive.
	/// </summary>
	void OnDisable ()
	{
		FingerGestures.OnFingerSwipe -= OnFingerSwipe;
	}


	#endregion MonoBehaviour Methods
	#region Static Methods



	/// <summary>
	/// Given a dayOfWeek it gives the index used by this program.
	/// </summary>
	/// <param name="day"></param>
	/// <returns></returns>
	private static int GetDayOfTheWeak (DayOfWeek day)
	{
		switch (day)
		{
			default:
			case DayOfWeek.Monday:
			return 0;

			case DayOfWeek.Tuesday:
			return 1;

			case DayOfWeek.Wednesday:
			return 2;

			case DayOfWeek.Thursday:
			return 3;

			case DayOfWeek.Friday:
			return 4;

			case DayOfWeek.Saturday:
			return 5;

			case DayOfWeek.Sunday:
			return 6;
		}
	}



	private static int GetMonthIndex (int value)
	{
		return (((value / 7) * 7) + (value % 7));
	}

	#endregion Static Methods
}