﻿using UnityEngine;
using System.Collections;
using MotorTools.Views;
using MotorTools.Data;

namespace MotorTools.Views.Utils
{

	/// <summary>
	/// Classe gestora da webview que carrega o mapa na tela do app.
	/// </summary>
	public class LoadMap : MonoBehaviour
	{
		/// <summary>
		/// Current GPS data 
		/// </summary>
		public LocationInfo CurrentLocation
		{ get; set; }



		/// <summary>
		/// The web view.
		/// </summary>
		public UniWebView webView;



		public MapView MapView
		{ get; set; }



		/// <summary>
		/// Quando o objeto é habilitado.
		/// </summary>
		void OnEnable()
		{
			StartCoroutine(StartMap());
		}



		/// <summary>
		/// Corrotina responsavel por inicializar as funções de ler o ultimo ponto de gps e inicializar o mapa.
		/// </summary>
		/// <returns>ienumerator para a corrotina.</returns>
		public IEnumerator StartMap()
		{
			Debug.Log("LoadMap:StartMap");
			yield return StartCoroutine(LoadLastGpsData());
			yield return StartCoroutine(LoadWebViewMapOnScreen());
		}



		/// <summary>
		/// Corrotina responsavel por recarregar todo o processo deste objeto.
		/// </summary>
		/// <returns>ienumerator para a corrotina.</returns>
		public IEnumerator ReloadAll()
		{
			Debug.Log("LoadMap:ReloadAll");
			yield return StartCoroutine(DisableWebView());
			yield return StartCoroutine(LoadLastGpsData());
			yield return StartCoroutine(LoadWebViewMapOnScreen());
		}



		/// <summary>
		/// Corrotina responsavel por carregar o  ultimo valor do gps do dispositivo.
		/// </summary>
		/// <returns>ienumerator para a corrotina.</returns>
		public IEnumerator LoadLastGpsData()
		{
			Debug.Log("LoadMap:LoadLastGpsData");
			CurrentLocation = new LocationInfo();

			//webView = GetComponent<UniWebView>();

			// Start GPS service
			if (!Input.location.isEnabledByUser)
				Debug.Log("Not started");

			Input.location.Start();

			// Wait until service initializes
			int maxWait = 1;
			while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
			{
				yield return new WaitForSeconds(1);
				maxWait--;
			}
			// Service didn't initialize in 20 seconds
			if (maxWait < 1)
			{
				Debug.Log("Timed out");
				//yield break;
			}

			// Connection has failed
			if (Input.location.status == LocationServiceStatus.Failed)
			{
				Debug.Log("Unable to determine device location");
				//yield break;
			}
			else
			{
				// Access granted and location value could be retrieved
				Debug.Log("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);
			}

			// Stop service if there is no need to query location updates continuously
			Input.location.Stop();


			// Get Latest Location data
			CurrentLocation = Input.location.lastData;


			yield return null;
		}



		/// <summary>
		/// Corrotina responsavel por configurar e carregar todo o mapa na tela.
		/// </summary>
		/// <returns>ienumerator para a corrotina.</returns>
		public IEnumerator LoadWebViewMapOnScreen()
		{
			Debug.Log("LoadMap:LoadWebViewMapOnScreen");
			//set the webview to be zoomable.
			webView.zoomEnable = true;
			// Set the webview size leaving a blank space at the top to show the apps ActionBar
			webView.insets = new UniWebViewEdgeInsets
			(
			100 * Screen.height / 1280,
			Screen.width / 720,
			100 * Screen.height / 1280,
			Screen.width / 720
			);

			webView.SetSpinnerLabelText("Carregando...");

			// Start the webview component with the provided url supplying the location coordinates
			webView.url = URLs.BaseURL + URLs.Slash + URLs.Map +URLs.Slash + CurrentLocation.latitude + URLs.Slash +CurrentLocation.longitude;
			if (CurrentLocation.latitude == 0 && CurrentLocation.longitude == 0)
			{
				//posicao de sao paulo.
				webView.url = URLs.BaseURL + URLs.Slash + URLs.Map +URLs.Slash + "-23.6824124" + URLs.Slash + "-46.5952992";
				//"http://52.11.115.146/motortools/index/map/-23.6824124/-46.5952992";
			}
			//webView.url="http://uniwebview.onevcat.com/reference/class_uni_web_view.html#a26affd5bd72d77b9f2af8b05f7bc98ba";
			//Debug.Log("52.11.115.146/motortools/index/map/" + CurrentLocation.latitude + "/" + CurrentLocation.longitude);
			webView.enabled = true;
			// Load the webview
			webView.Load();


			webView.OnLoadComplete += OnLoadComplete;
			webView.OnReceivedMessage += OnReceivedMessage;
			//webView.OnEvalJavaScriptFinished += OnEvalJavaScriptFinished;

			yield return null;
		}



		/// <summary>
		/// Corrotina responsavel por desabilitar a webview.
		/// </summary>
		/// <returns>ienumerator para a corrotina.</returns>
		public IEnumerator DisableWebView()
		{
			//Debug.Log("LoadMap:DisableWebView");
			webView.OnLoadComplete -= OnLoadComplete;
			webView.OnReceivedMessage -= OnReceivedMessage;
			webView.Stop();
			webView.Hide();
			yield return null;
		}



		/// <summary>
		/// Raises the load complete event.
		/// </summary>
		/// <param name="webView">Web view.</param>
		/// <param name="success">If set to <c>true</c> success.</param>
		/// <param name="errorMessage">Error message.</param>
		void OnLoadComplete(UniWebView webView, bool success, string errorMessage)
		{
			//Debug.Log("LoadMap:OnLoadComplete(" + success + ")");
			if (success)
			{
				// Displays the map page.
				webView.Show();
			}
			else
			{
				// TODO: mostrar que o mapa nao pode ser carregado.
				Debug.LogError("Something wrong in webview loading: " + errorMessage);
			}
		}


		/// <summary>
		/// Raises the received message event.
		/// </summary>
		/// <param name="webView">Web view.</param>
		/// <param name="message">Message.</param>
		void OnReceivedMessage(UniWebView webView, UniWebViewMessage message)
		{
			//Debug.Log("LoadMap:OnReceivedMessage:\n" + message.rawMessage);
			//DONE: Handle the inputs from webview, like going to the next sceen
			MapView.RepairShopSelectedOnMap(message.args["id"], CurrentLocation);
		}





	}
}