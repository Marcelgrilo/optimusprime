﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EngineAnimator : MonoBehaviour
{

		public GameObject [] Engine;
	
		public float Speed = 10;
	
		public bool Direction;

		bool isEnabled;
		Coroutine routine;

		void Start ()
		{
				isEnabled = false;
		}

	
		public void EnableEngine (bool flag)
		{
				if (isEnabled == flag)
				{
						return;
				}
				//Debug.Log (isEnabled + "   " + flag);
				isEnabled = flag;
				if (routine != null)
				{
						StopCoroutine (routine);
				}
				if (flag)
				{
						routine = StartCoroutine (EngineOn ());
				}
				
		}
	
	
		IEnumerator EngineOn ()
		{
				while (isEnabled)
				{
						Engine [0].transform.Rotate 
						(
							Vector3.forward * (Direction ? 1 : -1), Speed * Time.deltaTime * Mathf.PI * 2 * 1.55f
						);
						Engine [1].transform.Rotate 
								(
							Vector3.forward * (Direction ? -1 : 1), Speed * Time.deltaTime * Mathf.PI * 2 * 2.22f
						);
						Engine [2].transform.Rotate 
						(
							Vector3.forward * (Direction ? 1 : -1), Speed * Time.deltaTime * Mathf.PI * 2 * 3.33f
						);
						yield return new WaitForEndOfFrame ();
				}
				yield break;
		}
}
