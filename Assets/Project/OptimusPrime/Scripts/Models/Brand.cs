﻿
namespace MotorTools.Models
{
	public class Brand
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public Brand(string id, string name)
		{
			Id = id;
			Name = name;
		}
	}
}