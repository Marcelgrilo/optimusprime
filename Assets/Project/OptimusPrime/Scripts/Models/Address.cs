﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Marcel.JSON;
using MotorTools.Data;
using UnityEngine;

namespace MotorTools.Models
{
	public class Address
	{
		/// <summary>
		/// Id do endereco.
		/// </summary>
		public string Id { get; set; }
		/// <summary>
		/// O texto que o usuario insere.
		/// </summary>
		public string UserInputAddress { get; set; }
		/// <summary>
		/// O CEp do endereco.
		/// </summary>
		public string CEP { get; set; }
		/// <summary>
		/// O endereco do endereco.
		/// </summary>
		public string StreetLine1 { get; set; }
		/// <summary>
		/// O endereco do endereco.
		/// </summary>
		public string StreetLine2 { get; set; }
		/// <summary>
		/// O numero do endereco.
		/// </summary>
		public string Number { get; set; }
		/// <summary>
		/// A cidade do endereco.
		/// </summary>
		public string City { get; set; }
		/// <summary>
		/// O estado do endereco.
		/// </summary>
		public string State { get; set; }
		/// <summary>
		/// O pais do endereco.
		/// </summary>
		public string Country { get; set; }
		/// <summary>
		/// Posicao latitudinal do endereco.
		/// </summary>
		public string Latitude { get; set; }
		/// <summary>
		/// Posicao longitudinal do endereco.
		/// </summary>
		public string Longitude { get; set; }
		/// <summary>
		/// Endereco formatado para apresentacao.
		/// </summary>
		public string FormattedAddress { get; set; }



		/// <summary>
		/// CRia um objeto do tipo Address.
		/// </summary>
		/// <param name="id"></param>
		/// <param name="userInputAddress"></param>
		/// <param name="cep"></param>
		/// <param name="streetLine1"></param>
		/// <param name="streetLine2"></param>
		/// <param name="number"></param>
		/// <param name="city"></param>
		/// <param name="state"></param>
		/// <param name="country"></param>
		/// <param name="latitude"></param>
		/// <param name="longitude"></param>
		/// <param name="formattedAddress"></param>
		public Address(string id, string userInputAddress, string cep, string streetLine1, string streetLine2, string number, string city, string state, string country, string latitude, string longitude, string formattedAddress)
		{ 
			this.Id = id;
			this.UserInputAddress = userInputAddress;
			this.CEP = cep;

			this.StreetLine1 = streetLine1;
			this.StreetLine2 = streetLine2;
			this.Number = number;

			this.City = city;
			this.State = state;
			this.Country = country;

			this.Latitude = latitude;
			this.Longitude = longitude;
			this.FormattedAddress = formattedAddress;
		}

		public JSONNode ToJson()
		{
			JSONNode node = new JSONClass(); ;

			node.Add("id", Id);
			node.Add("userInputAddress", UserInputAddress);
			node.Add("cep", CEP);
			node.Add("streetLine1", StreetLine1);
			node.Add("streetLine2", StreetLine2);
			node.Add("number", Number);
			node.Add("city", City);
			node.Add("state", State);
			node.Add("country", Country);
			node.Add("lat", Latitude);
			node.Add("lng", Longitude);
			node.Add("formattedAddress", FormattedAddress);

			return node;
		}


		public override bool Equals(object obj)
		{
			if (obj.GetType() == this.GetType())
			{
				var other = (Address)obj;
				return
					(
					other.Id == this.Id &&
					other.Latitude == this.Latitude &&
					other.Longitude == this.Longitude &&
					other.Number == this.Number &&
					other.State == this.State &&
					other.CEP == this.CEP &&
					other.City == this.City &&
					other.Country == this.Country &&
					other.FormattedAddress == this.FormattedAddress &&
					other.StreetLine1 == this.StreetLine1 &&
					other.StreetLine2 == this.StreetLine2 &&
					other.UserInputAddress == this.UserInputAddress
					);
			}
			return false;
		}

		public override int GetHashCode()
		{
			return base.GetHashCode() ^ 
				Id.GetHashCode() ^ 
				Latitude.GetHashCode()^
				Longitude.GetHashCode() ^
				Number.GetHashCode() ^
				State.GetHashCode() ^
				CEP.GetHashCode() ^
				City.GetHashCode() ^
				Country.GetHashCode() ^
				FormattedAddress.GetHashCode() ^
				StreetLine1.GetHashCode() ^
				StreetLine2.GetHashCode() ^
				UserInputAddress.GetHashCode();
		}

		#region StaticMethods

		public static Address CreateFromGoogleJson(string googleResponse, string inputString)
		{
			return CreateFromGoogleJson(JSON.Parse(googleResponse),  inputString);
        }
		public static Address CreateFromGoogleJson(JSONNode node, string inputString)
		{
			return new Address
					(
						"X",
						inputString,
						node[Strings.JSONResults][0][Strings.JSONAddressComponents][0][Strings.JSONPostalCodePrefix][Strings.JSONLongName].Value,
						node[Strings.JSONResults][0][Strings.JSONAddressComponents][0][Strings.JSONRoute][Strings.JSONLongName].Value,
						node[Strings.JSONResults][0][Strings.JSONAddressComponents][0][Strings.JSONNeighborhood][Strings.JSONLongName].Value,
						node[Strings.JSONResults][0][Strings.JSONAddressComponents][0][Strings.JSONStreetNumber][Strings.JSONLongName].Value,
						node[Strings.JSONResults][0][Strings.JSONAddressComponents][0][Strings.JSONLocality][Strings.JSONLongName].Value,
						node[Strings.JSONResults][0][Strings.JSONAddressComponents][0][Strings.JSONAministrativeAreaLevel_1][Strings.JSONLongName].Value,
						node[Strings.JSONResults][0][Strings.JSONAddressComponents][0][Strings.JSONCountry][Strings.JSONLongName].Value,
						node[Strings.JSONResults][0][Strings.JSONGeometry][Strings.JSONLocation][Strings.JSONLat].Value,
						node[Strings.JSONResults][0][Strings.JSONGeometry][Strings.JSONLocation][Strings.JSONLng].Value,
						node[Strings.JSONResults][0][Strings.JSONFormattedAddress].Value
					);
		}

	

		/// <summary>
		/// Cria um endereco a partir de uma string json.
		/// </summary>
		/// <param name="json">string contendo os dados em formato json</param>
		/// <returns>objeto do tipo RepairShop.</returns>
		public static Address CreateAddress(string json)
		{
			try
			{
				return CreateAddress(JSONNode.Parse(json));
			}
			catch
			{
				return null;
			}
		}



		/// <summary>
		/// Cria um endereco a partir de um json node.
		/// </summary>
		/// <param name="node"> json node</param>
		/// <returns>objeto do tipo RepairShop.</returns>
		public static Address CreateAddress(JSONNode node)
		{
			if (string.IsNullOrEmpty(node["id"].Value))
			{
				return null;
			}
			return new Address
						(
							node["id"].Value,
							node["userInputAddress"].Value,
							node["cep"].Value,
							node["streetLine1"].Value,
							node["streetLine2"].Value,
							node["number"].Value,
							node["city"].Value,
							node["state"].Value,
							node["country"].Value,
							node["lat"].Value,
							node["lng"].Value,
							node["formattedAddress"].Value
			);
		}


		public static void TestSaveAddress()
		{
			Address a = new Address(0 + "", "Endereço a", "", "", "", "", "", "", "", "0", "0", "");
			Address b = new Address(1 + "", "Endereço b", "", "", "", "", "", "", "", "1", "0", "");
			Address c = new Address(2 + "", "Endereço c", "", "", "", "", "", "", "", "1", "2", "");
			Address cc = new Address(3 + "", "Endereço cClone", "", "", "", "", "", "", "", "1", "2", "");

			List<Address> addresses = new List<Address>();

			for (int i = 0; i < 5; i++)
			{
				addresses.Add(new Address(i + "list", "list Endereço " + i, "", "", "", "", "", "", "", "-1"+i, "-1"+i, ""));
			}

			SaveAddress(a);
			GetAllAddresses();
            SaveAddress(b);
			GetAllAddresses();
            SaveAddress(c);
			GetAllAddresses();
            SaveAddress(cc);
			GetAllAddresses();
            SaveAddresses(addresses.ToArray<Address>());
			GetAllAddresses();
			PlayerPrefs.SetString("FavoriteAddresses", "");
		}

		public static void SaveAddresses(Address[] addresses)
		{
			foreach (var v in addresses)
				SaveAddress(v);
		}

		public static void SaveAddress(Address a)
		{
			string stored = PlayerPrefs.GetString("FavoriteAddresses","");


			if (string.IsNullOrEmpty(stored))
			{
				JSONNode node = new JSONArray();
				node.Add(a.ToJson());
				PlayerPrefs.SetString("FavoriteAddresses",node.ToString());
			}
			else
			{
				JSONNode node = JSON.Parse(stored);

				for (int i = 0; i < node.Count; i++)
				{
					if (node[i]["lat"].Value == a.Latitude && node[i]["lng"].Value == a.Longitude && node[i]["cep"].Value == a.CEP) return;
				}

				node.Add(a.ToJson());
				PlayerPrefs.SetString("FavoriteAddresses", node.ToString());
			}
		}

		public static Address[] GetAllAddresses()
		{
			Address[] addresses;
			string stored = PlayerPrefs.GetString("FavoriteAddresses", "");

			if (string.IsNullOrEmpty(stored))
			{
				return null;
			}
			else
			{
				JSONNode node = JSON.Parse(stored);
				Debug.Log("count: " + node.Count + "\n\n" + node.ToString());
				addresses = new Address[node.Count];
				for (int i = 0; i < node.Count; i++)
					addresses[i] = Address.CreateAddress(node[i]);

				return addresses;
			}

		}
		public static void RemoveAddresses(Address a)
		{
			string stored = PlayerPrefs.GetString("FavoriteAddresses", "");


			if (string.IsNullOrEmpty(stored))
			{
				return;
			}
			else
			{
				JSONNode node = JSON.Parse(stored);

				int toRemove = -1;
				
				for (int i = 0; i < node.Count; i++)
				{
					if (node[i]["lat"].Value == a.Latitude && node[i]["lng"].Value == a.Longitude && node[i]["cep"].Value == a.CEP) toRemove = i;
				}
				if(toRemove!=-1)
					node.Remove(toRemove);

				PlayerPrefs.SetString("FavoriteAddresses", node.ToString());
			}
		}
		#endregion StaticMethods
	}
}
