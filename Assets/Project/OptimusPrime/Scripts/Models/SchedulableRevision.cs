﻿namespace MotorTools.Models
{
	using System;

	/// <summary>
	/// A schedulable Revision.
	/// </summary>
	public class SchedulableRevision
	{
		#region Properties

		/// <summary>
		/// The date that this revision will ocour.
		/// </summary>
		public DateTime Date;

		/// <summary>
		/// The kilometer that this revision will ocour.
		/// </summary>
		public int Km;

		/// <summary>
		/// 
		/// </summary>
		public RevisionItem RevisionItemObject; 
		
		#endregion Properties
		#region Constructor

		/// <summary>
		/// The constructor for a SchedulableRevision.
		/// </summary>
		/// <param name="date"> The cate</param>
		/// <param name="km"> the kilometer</param>
		public SchedulableRevision (DateTime date, int km, RevisionItem revisionItemObjectReference)
		{
			Date = date;
			Km = km;
			RevisionItemObject = revisionItemObjectReference;
		}

		#endregion Constructor
		#region Object Overrided Methods

		/// <summary>
		/// The overriden method for comparison.
		/// </summary>
		/// <param name="obj">the object to compare</param>
		/// <returns>TRUE if the object is the same in type and parameters, FALSE otherwise.</returns>
		public override bool Equals (object obj)
		{
			if (!obj.GetType ().Equals (this.GetType ()))
			{
				return false;
			}

			SchedulableRevision other = (SchedulableRevision) obj;
			if (
					other.Date != this.Date ||
					other.Km != this.Km
				)
			{
				return false;
			}

			return true;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}


		#endregion Object Overrided Methods
	}
}