﻿namespace MotorTools.Models
{
	/// <summary>
	/// The class model that contains all the data downloaded from
	/// the server for a revision item of a determined carId.
	/// </summary>
	public class RevisionItem
	{
		#region Enums


		public enum Type
		{
			Acessorios, 
			AcessoriosSeguranca,
			ArrefecimentoLubrificacao,
			ChassiLataria,
			Direcao,
			Eletrica,
			Exaustao,
			Filtros,
			ForracaoInternaBancos,
			Freios,
			iluminacao,
			Motor,
			Painel,
			ReservatoriosNiveis,
			RodasPneus,
			SegurancaTravasAlarmes,
			Suspensao,
			Transmissao,
			Vidros
		}


		#endregion Enums
		#region Readonly Attributes

		/// <summary>
		/// The _id.
		/// </summary>
		private readonly string _id;



		/// <summary>
		/// the _partName.
		/// </summary>
		private readonly string _partName;



		/// <summary>
		/// the _months.
		/// </summary>
		private readonly string _months;



		/// <summary>
		/// The _distance.
		/// </summary>
		private readonly string _distance;



		/// <summary>
		/// The kmVariation represents the 
		/// </summary>
		private readonly string _kmVariation;



		/// <summary>
		/// the new variation on km.
		/// </summary>
		private readonly string _newDistance;
		


		/// <summary>
		/// The _vehicleId.
		/// </summary>
		private readonly string _vehicleId;



		/// <summary>
		/// The MaintenaceType represents the type and the icon.
		/// </summary>
		private readonly string _maintenaceType;



		#endregion Readonly Attributes
		#region Properties



		/// <summary>
		///  Gets the Id.
		/// </summary>
		public string Id
		{
			get
			{
				return _id;
			}
		}



		/// <summary>
		/// Gets the PartName.
		/// </summary>
		public string PartName
		{
			get
			{
				return _partName;
			}
		}



		/// <summary>
		/// Gets the Months.
		/// </summary>
		public string Months
		{
			get
			{
				return _months;
			}
		}



		/// <summary>
		/// Gets the Distance.
		/// </summary>
		public string Distance
		{
			get
			{
				return _distance;
			}
		}



		/// <summary>
		/// Gets the KmVariation.
		/// </summary>
		public string KmVariation
		{
			get
			{
				return _kmVariation;
			}
		}



		/// <summary>
		/// Gets the NewDistance.
		/// </summary>
		public string NewDistance
		{
			get
			{
				return _newDistance;
			}
		}



		/// <summary>
		/// Gets the VehicleId.
		/// </summary>
		public string VehicleId
		{
			get
			{
				return _vehicleId;
			}
		}



		/// <summary>
		/// Gets the _maintenaceType.
		/// </summary>
		public string MaintenaceType
		{
			get
			{
				return _maintenaceType;
			}
		}




		public int IntDistance
		{
			get;
			private set;
		}



		public int IntKmVariation
		{
			get;
			private set;
		}



		public int IntNewDistance
		{
			get;
			private set;
		}

		

		public int IntMonth
		{
			get;
			private set;
		}






		#endregion Properties
		#region Constructor



		/// <summary>
		/// The constructor for this revision item.
		/// </summary>
		/// <param name="id">the _id</param>
		/// <param name="partName">the _partName</param>
		/// <param name="months">the _months</param>
		/// <param name="distance">the _distance</param>
		/// <param name="vehicleId">the vehiacle _id</param>
		public RevisionItem (string id, string partName, string months, string distance, string vehicleId, string maintenaceType, string kmVariation, string newDistance)
		{
			_id = id;
			_partName = partName;
			_months = months;
			_distance = distance;
			_vehicleId = vehicleId;
			_maintenaceType = maintenaceType;
			_kmVariation = kmVariation;
			_newDistance = newDistance;

			IntDistance = int.Parse(distance);
			IntMonth = int.Parse(months);
			IntKmVariation = int.Parse(kmVariation);
			IntNewDistance = int.Parse(newDistance);
		}



		#endregion Constructor
		#region Object Override Methods



		/// <summary>
		/// The overrided ToString method.
		/// </summary>
		/// <returns>A description of this object attributes.</returns>
		public override string ToString ()
		{
			return string.Format ("[RevisionItem: id={0}, partName={1}, months={2}, distance={3}, vehicleId={4}]", _id, _partName, _months, _distance, _vehicleId);
		}



		/// <summary>
		///  The overrided Equals Method.
		/// </summary>
		/// <param name="obj">The object to compare</param>
		/// <returns>TRUE if the object is equals in type and attributes, FALSE otherwise.</returns>
		public override bool Equals (object obj)
		{
			if (!obj.GetType ().Equals (this.GetType ()))
			{
				return false;
			}

			RevisionItem other = (RevisionItem) obj;
			if (
				other._id != _id ||
				other._partName != _partName ||
				other._months != _months ||
				other._distance != _distance ||
				other._vehicleId != _vehicleId ||
				other._maintenaceType != _maintenaceType ||
				other._kmVariation != _kmVariation ||
				other._newDistance != _newDistance
				)
			{
				return false;
			}
			return true;
		}



		public override int GetHashCode()
		{
			return base.GetHashCode();
		}


		#endregion Object Override Methods
	}
}