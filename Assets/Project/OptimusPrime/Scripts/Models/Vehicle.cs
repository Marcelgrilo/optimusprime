﻿namespace MotorTools.Models
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using Marcel.DP.Message;
	using MotorTools.Data;
	using UnityEngine;

	public class Vehicle
	{
		private readonly string _brandId;
		private readonly string _model;
		private readonly int _year;
		private readonly string _version;
		private readonly float _initialKm;

		private string _carId;
		private float _perDayKm = 0;

		private readonly DateTime _initialDate;

		private float _calendarSliderKm = 0;
		private float _currentKm = 0;

		private List<RevisionItem> _revisionItens;
		private List<RevisionScheduler> _vehicleRevisionsList;



		public string BrandId
		{
			get
			{
				return _brandId;
			}
		}



		public float CalendarSliderKm
		{
			get { return _calendarSliderKm; }
			set { _calendarSliderKm = value; }
		}



		public string CarId
		{
			get
			{
				return _carId;
			}
			set
			{
				_carId = value;
			}
		}



		public float CurrentKm
		{
			get
			{
				CurrentKm = KilometersPassed (DateTime.Now) + InitialKm;
				return _currentKm;
			}
			set
			{
				if (value > _currentKm)
				{
					_currentKm = value;
					Messenger.Broadcast<float> (Strings.CurrentKm + GetHashCode (), _currentKm);
				}
			}
		}



		public float InitialKm
		{
			get
			{
				return _initialKm;
			}
		}



		public string Model
		{
			get
			{
				return _model;
			}
		}



		public float PerDayKm
		{
			get
			{
				return _perDayKm;
			}
			set
			{
				if (Math.Abs (value - _perDayKm) > 0.00001f)
				{
					_perDayKm = value;
					Messenger.Broadcast<float> (Strings.PerDayKm + GetHashCode (), _perDayKm);
				}
			}
		}



		public string Version
		{
			get
			{
				return _version;
			}
		}



		public int Year
		{
			get
			{
				return _year;
			}
		}



		private List<RevisionItem> RevisionItems
		{
			get
			{
				if (_revisionItens == null)
				{
					_revisionItens = new List<RevisionItem> ();
				}

				return _revisionItens;
			}
		}



		private List<RevisionScheduler> VehicleSchedulkedRevisionsList
		{
			get
			{
				if (_vehicleRevisionsList == null)
				{
					_vehicleRevisionsList = new List<RevisionScheduler> ();
				}

				return _vehicleRevisionsList;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="model"></param>
		/// <param name="year"></param>
		/// <param name="version"></param>
		/// <param name="initialKm"></param>
		/// <param name="initialDate"></param>
		public Vehicle (string brand, string model, int year, string version, float initialKm, DateTime initialDate)
		{
			this._brandId = brand;
			this._model = model;
			this._year = year;
			this._version = version;
			this._initialKm = initialKm;
			this._initialDate = initialDate;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="item"></param>
		public void AddRevItem (RevisionItem item)
		{
			RevisionItems.Add (item);
			VehicleSchedulkedRevisionsList.Add (new RevisionScheduler (item));
		}
		/// <summary>
		/// Resets the lists.
		/// </summary>
		public void ResetRevisions ()
		{
			_revisionItens = new List<RevisionItem> ();
			_vehicleRevisionsList = new List<RevisionScheduler> ();
		}

		/// <summary>
		/// The time passed since the initial registration of this vehicle.
		/// </summary>
		/// <param name="date">the date to verify</param>
		/// <returns>A time span (datepassed - initialDate)</returns>
		public TimeSpan TimeSinceStart (DateTime date)
		{
			return (date - _initialDate);
		}
		/// <summary>
		/// The kilometers passed sisce the start. 
		/// It is based on the perDayKm.
		/// </summary>
		/// <param name="date">the date to verify</param>
		/// <returns>the ammount of kilometers passed</returns>
		public int KilometersPassed (DateTime date)
		{
			return (int) (PerDayKm * TimeSinceStart (date).Days);
		}

		/// <summary>
		/// Gets a list of revisions, based on the kilometers passed and based on the Time since Start, for the date passed.
		/// </summary>
		/// <param name="date"></param>
		/// <returns></returns>
		public List<SchedulableRevision> RevisionsOnDay (DateTime date)
		{
			var list = new List<SchedulableRevision>();
			int kmPassed = KilometersPassed (date);
			//int months = (TimeSinceStart (date).Days / 30);
			int days = TimeSinceStart(date).Days ;

			foreach (RevisionScheduler rd in VehicleSchedulkedRevisionsList)
			{
				int kmCounter = (rd.RevisionItem.IntDistance == 0 ? 0 : kmPassed / rd.RevisionItem.IntDistance);
				if (kmPassed > rd.RevisionItem.IntKmVariation)
				{
					int countBeforeVariation = (rd.RevisionItem.IntDistance == 0 ? 0 : rd.RevisionItem.IntKmVariation / rd.RevisionItem.IntDistance);
					int countAfterVariation = (rd.RevisionItem.IntNewDistance == 0 ? 0 : (kmPassed - rd.RevisionItem.IntKmVariation) / rd.RevisionItem.IntNewDistance);
					kmCounter = countBeforeVariation + countAfterVariation;
				}


				if (
						//Logica Antiga
						//(kmPassed / (rd.RevisionItem.IntDistance == 0 ? 1 : rd.RevisionItem.IntDistance)) > rd.RevisionCounter ||
						//(months / (rd.RevisionItem.IntMonth == 0 ? 1 : rd.RevisionItem.IntMonth)) > rd.RevisionCounter
						kmCounter > rd.RevisionCounter ||
						(rd.RevisionItem.IntMonth == 0 ? 1 : days / rd.RevisionItem.IntMonth) > rd.RevisionCounter
					)
				{
					if(kmCounter!=0 || rd.RevisionItem.IntMonth != 0)
						rd.ScheduleRevision (date, kmPassed);
				}
			}

			foreach (RevisionScheduler rd in VehicleSchedulkedRevisionsList)
			{
				SchedulableRevision r = rd.Contains (date, kmPassed);
				if (r != null)
				{
					list.Add (r);
				}
			}

			//str += "\nListCount" + list.Count;
			//Debug.Log (str);
			return list;
		}


		/// <summary>
		/// Gets the revision items for the espedcific kilommeter.
		/// </summary>
		/// <param name="km">the current km</param>
		/// <returns>all the revision for this km.</returns>
		public List<RevisionItem> RevisionsOnKm(int km)
		{
			//string str = string.Empty;
			var list = new List<RevisionItem>();
			//foreach revisionItem
			foreach (RevisionItem item in RevisionItems)
			{
				int dist = item.IntDistance;
				if (!item.IntKmVariation.Equals(0) && km > item.IntKmVariation)
				{
					km -= item.IntKmVariation;
					dist = item.IntNewDistance;
				}

				//dont show if the dist==0;
				if (dist == 0)
				{
					continue;
				}

				// calculates the rest of the division on the currentkm per the item km.
				int rest = km %  dist;
				// calculates the interval as 5 times the perDayKm, i.e. the user have 5 days of interval for each revision.
				//int interval = (int) perDayKm * 5;
				int intervalToShow = 500;
				//str+=string.Format("km={0}\titemKm={1}\trest={2}\tinterval={3}\tItem-Rest={4}\n", km, item.IntDistance, rest, interval, item.IntDistance - rest);
				// verifies if the revision can be shown.
				if (intervalToShow >= dist - rest)
				{
					// se a lista ainda nao contem este item
					if (!list.Contains(item))
					{
						list.Add(item);
					}
				}

				// logica de rascunho nao otimizada.
				//if (item.IntKmVariation.Equals(0))
				//{
				//	// calculates the rest of the division on the currentkm per the item km.
				//	int rest = km % (item.IntDistance == 0 ? 1 : item.IntDistance);
				//	// calculates the interval as 5 times the perDayKm, i.e. the user have 5 days of interval for each revision.
				//	//int interval = (int) perDayKm * 5;
				//	int intervalToShow = 500;
				//	//str+=string.Format("km={0}\titemKm={1}\trest={2}\tinterval={3}\tItem-Rest={4}\n", km, item.IntDistance, rest, interval, item.IntDistance - rest);
				//	// verifies if the revision can be shown.
				//	if (intervalToShow >= item.IntDistance - rest)
				//	{
				//		// se a lista ainda nao contem este item
				//		if (!list.Contains(item))
				//		{
				//			list.Add(item);
				//		}
				//	}
				//}
				//else
				//{
				//	if (km > item.IntKmVariation)
				//	{
				//		km -= item.IntKmVariation;
				//		// calculates the rest of the division on the currentkm per the item km.
				//		int rest = km % (item.IntNewDistance == 0 ? 1 : item.IntNewDistance);
				//		// calculates the interval as 5 times the perDayKm, i.e. the user have 5 days of interval for each revision.
				//		//int interval = (int) perDayKm * 5;
				//		int intervalToShow = 500;
				//		//str+=string.Format("km={0}\titemKm={1}\trest={2}\tinterval={3}\tItem-Rest={4}\n", km, item.IntDistance, rest, interval, item.IntDistance - rest);
				//		// verifies if the revision can be shown.
				//		if (intervalToShow >= item.IntNewDistance - rest)
				//		{
				//			// se a lista ainda nao contem este item
				//			if (!list.Contains(item))
				//			{
				//				list.Add(item);
				//			}
				//		}
				//	}
				//	else
				//	{
				//		// calculates the rest of the division on the currentkm per the item km.
				//		int rest = km % (item.IntDistance == 0 ? 1 : item.IntDistance);
				//		// calculates the interval as 5 times the perDayKm, i.e. the user have 5 days of interval for each revision.
				//		//int interval = (int) perDayKm * 5;
				//		int intervalToShow = 500;
				//		//str+=string.Format("km={0}\titemKm={1}\trest={2}\tinterval={3}\tItem-Rest={4}\n", km, item.IntDistance, rest, interval, item.IntDistance - rest);
				//		// verifies if the revision can be shown.
				//		if (intervalToShow >= item.IntDistance - rest)
				//		{
				//			// se a lista ainda nao contem este item
				//			if (!list.Contains(item))
				//			{
				//				list.Add(item);
				//			}
				//		}
				//	}
				//}
			}
			//str += list.Count;
			//Debug.Log(str);
			return list;
		}

		
		public override string ToString ()
		{
			return string.Format ("[Vehicle: Brand={0}, Model={1}, Year={2}, Version={3}]", BrandId, Model, Year, Version);
		}

		#region listeners

		public void AddCurrentKmListener (IVehicleListenner calback)
		{
			Messenger.AddListener<float> (Strings.CurrentKm + GetHashCode (), calback.OnCurrentKmChange);
		}

		public void AddPerDayKmListener (IVehicleListenner calback)
		{
			Messenger.AddListener<float> (Strings.PerDayKm + GetHashCode (), calback.OnPerDayKmChange);
		}

		public void RemoveCurrentKmListener (IVehicleListenner calback)
		{
			Messenger.RemoveListener<float> (Strings.CurrentKm + GetHashCode (), calback.OnCurrentKmChange);
		}

		public void RemovePerDayKmListener (IVehicleListenner calback)
		{
			Messenger.RemoveListener<float> (Strings.PerDayKm + GetHashCode (), calback.OnPerDayKmChange);
		}


		#endregion listeners

		#region Local Data Storage

		public static Vehicle LoadVehicleLoginData ()
		{
			return
			new Vehicle
			(
					PlayerPrefs.GetString (Strings.SelectBrand, string.Empty),
					PlayerPrefs.GetString(Strings.SelectCar, string.Empty),
					PlayerPrefs.GetInt (Strings.SelectYear, 0),
					PlayerPrefs.GetString(Strings.SelectVersion, string.Empty),
					PlayerPrefs.GetFloat (Strings.SelectKm, 0f),
					new DateTime(long.Parse(PlayerPrefs.GetString(Strings.InitialDate, (DateTime.Now.Ticks.ToString()))))
			);
		}

		public void SaveVehicleLoginData ()
		{
			PlayerPrefs.SetString (Strings.SelectBrand, BrandId);
			PlayerPrefs.SetString (Strings.SelectCar, Model);
			PlayerPrefs.SetInt (Strings.SelectYear, Year);
			PlayerPrefs.SetString (Strings.SelectVersion, Version);
			PlayerPrefs.SetFloat (Strings.SelectKm, InitialKm);
			PlayerPrefs.SetString(Strings.InitialDate, _initialDate.Ticks.ToString());

			PlayerPrefs.Save ();
		}

		#endregion Local Data Storage
	}
}