﻿namespace MotorTools.Data
{


	using UnityEngine;
	using System.Collections;


	/// <summary>
	/// {"Marcas":"\/index\/brands",
	/// "Modelos":"\/index\/models\/*marca*",
	/// "Anos":"\/index\/years\/*marca*\/*modelo*",
	/// "Vers\u00e3o":"\/index\/years\/*marca*\/*modelo*\/*ano*"
	/// }
	/// </summary>

	public static class URLs
	{
		public static readonly string Slash = "/";

		public static readonly string BaseURL = "http://52.11.115.146/motortools/index";

		public static readonly string Map = "map";

		public static readonly string Brands = "brands";

		public static readonly string Brand = "brand";

		public static readonly string Models = "models";

		public static readonly string Model = "model";

		public static readonly string Years = "years";

		public static readonly string Year = "year";

		public static readonly string Versions = "versions";

		public static readonly string Version = "version";

		public static readonly string VehicleID = "vehicle-id";

		public static readonly string CarID = "car_id";

		public static string RepairShop = "repair-shop";

		public static readonly string MaintenanceDefinitions = "maintenance-definitions";

		public static readonly string GoogleGeoReferencing = "https://maps.googleapis.com/maps/api/geocode/json?address=";


		private static readonly string UnformatedReverseGeocoding = "https://maps.googleapis.com/maps/api/geocode/json?latlng={0},{1}&sensor=false";

		/// <summary>
		/// Devolce a url para qrequisicao de uma reversegeocoding.
		/// fonte: http://blogs.microsoft.co.il/gilf/2011/06/22/using-reverse-geocoding-to-find-an-address/
		/// </summary>
		/// <param name="lat"></param>
		/// <param name="lng"></param>
		/// <returns></returns>
		public static string ReverseGeocodeURL(string lat, string lng)
		{
			return string.Format(UnformatedReverseGeocoding, lat, lng);
		}
	}


}
