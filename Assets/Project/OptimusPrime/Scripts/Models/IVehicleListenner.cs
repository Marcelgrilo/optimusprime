﻿namespace MotorTools.Models
{
		public interface IVehicleListenner
		{
				void OnCurrentKmChange (float value);
	
	
				void OnPerDayKmChange (float value);
		}
}
