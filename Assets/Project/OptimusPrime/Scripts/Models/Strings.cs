﻿using System.Linq;
using UnityEngine;

namespace MotorTools.Data
{



	public static class Strings
	{
		public static readonly string Ptbr = "pt-br";

		public static readonly string SelectBrand = "selectBrand";

		public static readonly string SelectCar = "selectCar";

		public static readonly string SelectYear = "selectYear";

		public static readonly string SelectVersion = "selectVersion";

		public static readonly string SelectKm = "selectKm";
		public static readonly string SelectDialyKm = "selectDialyKm";

		public static readonly string SeeMore = "seeMore";

		public static readonly string Minimize = "minimize";

		public static readonly string CurrentKm = "CurrentKm";

		public static readonly string PerDayKm = "PerDayKm";

		public static readonly string InitialDate = "InitialDate";

		public static readonly string Km = "km";
		public static readonly string K = "k";
		public static readonly string KmsAtual = "actualKm";

		public static readonly string Id = "id";
		public static readonly string VehicleId = "vehicle_id";
		public static readonly string PartName = "part_name";
		public static readonly string Months = "months";
		public static readonly string Distance = "distance";
		public static readonly string MaintenaceType = "maintenance_type";
		public static readonly string KmVariation = "km_variation";
		public static readonly string NewDistance = "new_distance";

		public static readonly string NoRevisionItem = "noRevItem";

		public static readonly string RevisionItemType = "RevisionItemType";

		public static readonly string[] RevisionIcons =
		{
			"ico-abastecimento",
			"ico-acessorios",
			"ico-acessorios-seguranca",
			"ico-arrefecimento-lubrificacao",
			"ico-chassi-lataria",
			"ico-direcao",
			"ico-eletrica",
			"ico-exaustao",
			"ico-filtros",
			"ico-forracao-interna-bancos",
			"ico-freios",
			"ico-iluminacao",
			"ico-motor",
			"ico-painel",
			"ico-reservatorios-niveis",
			"ico-rodas-pneus",
			"ico-seguranca-travas-alarmes",
			"ico-suspensao",
			"ico-transmissao",
			"ico-vidros"
		};



		public static int GetRevisionIconsIndex(string icon)
		{
			for (int i = 0; i < RevisionItemType.Length; i++)
			{
				if (
						icon.Equals(RevisionIcons[i]) ||
						icon.ToLower().Equals
						(
							Localization.Get(RevisionIcons[i])
						)
					)
					return i;
			}
			return 0;
		}

		public static readonly string Quilometragem = "quilometragem";


		public static readonly string Periodo = "periodo";

		public static string FormatCurrentKm(float value)
		{
			//int aux = value;
			//int kNumber = 0;

			//while (aux % 1000 <  1000)
			//{
			//	aux = aux / 1000;
			//	kNumber++;
			//}



			//string result = ""+aux+" " +  ;
			//return result;

			float v = (value > 10000 ? (value / 1000) : value);
			return string.Format("{0:0.0}", v) + (string) (value > 1000 ? Strings.K : Strings.Km);
		}


		public static readonly string JSONStatus = "status";
		public static readonly string JSONResults = "results";
		public static readonly string JSONAddressComponents = "address_components";
		public static readonly string JSONGeometry = "geometry";
		public static readonly string JSONLocation = "location";
		public static readonly string JSONLat = "lat";
		public static readonly string JSONLng = "lng";
		public static readonly string JSONLongName = "long_name";
		public static readonly string JSONStreetNumber = "street_number";
		public static readonly string JSONRoute = "route";
		public static readonly string JSONNeighborhood = "neighborhood";
		public static readonly string JSONLocality = "locality";
		public static readonly string JSONAministrativeAreaLevel_1 = "administrative_area_level_1";
		public static readonly string JSONAministrativeAreaLevel_2 = "administrative_area_level_2";
		public static readonly string JSONCountry = "country";
		public static readonly string JSONPostalCodePrefix = "postal_code_prefix";
		public static readonly string JSONFormattedAddress = "formatted_address";


		public static string ScreenShotName(int width, int height, string type)
		{
			string directory = string.Format("{0}/screenshots/",
								 Application.persistentDataPath);
			if (!System.IO.Directory.Exists(directory))
			{
				System.IO.Directory.CreateDirectory(directory);
			}



			return string.Format("{0}/screenshots/screen{1}_{2}x{3}_{4}.png",
							 Application.persistentDataPath,
							 type,
							 width, height,
							 System.DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"));
		}

		public static string FormatPhoneNumber(string value)
		{

			if (string.IsNullOrEmpty(value))
			{
				return "";
			}
			else
			{
				string formated = "";
				char[] array = new string(value.ToCharArray().Where(c => char.IsDigit(c)).ToArray()).ToCharArray();

				if (array.Length < 4)
				{
					formated = new string(array);
				}
				else if (array.Length <= 5)
				{
					int scan = array.Length;
					int counter = 0;
					while (scan > 0)
					{
						scan--;
						if (counter == 4)
							formated = "-" + formated;
						counter++;
						formated = array[scan] + formated;
					}
				}
				// 8 digitos
				else if (array.Length <= 8)
				{
					//formated = string.Format ("{0:####-####}", double.Parse (value));
					int scan = array.Length;
					int counter = 0;
					while (scan > 0)
					{
						scan--;
						if (counter == 4)
							formated = "-" + formated;
						counter++;
						formated = array[scan] + formated;
					}
				}
				// 9 digitos
				else if (array.Length <= 9)
				{
					//formated = string.Format ("{0:#####-####}", double.Parse (value));
					int scan = array.Length;
					int counter = 0;
					while (scan > 0)
					{
						scan--;
						if (counter == 4)
							formated = "-" + formated;
						counter++;
						formated = array[scan] + formated;
					}
				}
				// 8 digitos mais ddd
				else if (array.Length <= 11)
				{
					//formated = string.Format ("{0:(###) ####-####}", double.Parse (value));
					int scan = array.Length;
					int counter = 0;
					while (scan > 0)
					{
						scan--;
						if (counter == 4)
							formated = "-" + formated;
						if (counter == 8)
							formated = ") " + formated;
						counter++;
						formated = array[scan] + formated;
					}
					formated = "(" + formated;
				}
				// 9 digitos mais ddd
				else if (array.Length <= 12)
				{
					//	formated = string.Format ("{0:(###) #####-####}", double.Parse (value));
					int scan = array.Length;
					int counter = 0;
					while (scan > 0)
					{
						scan--;
						if (counter == 4)
							formated = "-" + formated;
						if (counter == 9)
							formated = ") " + formated;
						counter++;
						formated = array[scan] + formated;
					}
					formated = "(" + formated;
				}
				// 8 digitos mais ddd + codigo pais
				else if (array.Length <= 14)
				{
					//formated = string.Format ("{0:### (###) ####-####}", double.Parse (value));
					int scan = array.Length;
					int counter = 0;
					while (scan > 0)
					{
						scan--;
						if (counter == 4)
							formated = "-" + formated;
						if (counter == 8)
							formated = ") " + formated;
						if (counter == 11)
							formated = " (" + formated;
						counter++;
						formated = array[scan] + formated;
					}

				}
				// 9 digitos mais ddd + codigo pais
				else if (array.Length <= 15)
				{
					//formated = string.Format ("{0:### (###) #####-####}", double.Parse (value));
					int scan = array.Length;
					int counter = 0;
					while (scan > 0)
					{
						scan--;
						if (counter == 4)
							formated = "-" + formated;
						if (counter == 9)
							formated = ") " + formated;
						if (counter == 12)
							formated = " (" + formated;
						counter++;
						formated = array[scan] + formated;
					}
				}
				// qualquer outro caso
				else
					formated = value;

				return formated;
			}

		}


	}


}
