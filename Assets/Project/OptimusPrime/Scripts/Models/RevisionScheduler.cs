﻿namespace MotorTools.Models
{
	using System;
	using System.Collections.Generic;
	using System.Linq;

	public class RevisionScheduler
	{
		public RevisionItem RevisionItem;

		private List<SchedulableRevision> revisions;

		public RevisionScheduler (RevisionItem revisionId)
		{
			RevisionItem = revisionId;
			revisions = new List<SchedulableRevision> ();
		}

		public int RevisionCounter
		{
			get { return revisions.Count; }
		}

		public SchedulableRevision Contains (DateTime date, int km)
		{
			if (revisions.Count == 0)
			{
				return null;
			}
			var result = revisions.FirstOrDefault (a => ((a.Date.Year == date.Year && a.Date.Month == date.Month && a.Date.Day == date.Day) || a.Km == km));
			return result;
		}

		public void ScheduleRevision (DateTime date, int km)
		{
			revisions.Add(new SchedulableRevision(date, km, RevisionItem));
		}
	}
}