﻿namespace MotorTools.Models
{

	using UnityEngine;
	using Marcel.JSON;
	using Controllers;

	public class RepairShop
	{
		/*
		 http://52.11.115.146/motortools/index/repair-shop
		 se mandar uma id=1, atravez de um form por post, retorna uma oficina com o seguinte json
		
		{
			id: "1"
			name: "M.A. Custons - Repair Shop"
			description: "M.A. Custons - Repair Shop, greate service, greate price!"
			phone_number: "+553536224414"
			street_line_1: "Rua da oficina"
			city: "Itajubá"
			lat: "-22.4186921"
			long: "-45.4466036"
			cellphone: "+5535385869575"
			cep: "3750000"
			number: "322"
			street_line_2: ""
			country: "Brazil"
			state: "Minas Gerais"
			email: "contato@macustons.com.br"
		}

		verificar qual e a url que retorna todas as repairshops das redondezas quando se passa uma posicao de gps para o form



			atualmente existem 4 oficinas cadastradas nop banco de dados.

		*/

		/// <summary>
		/// Id da oficina.
		/// </summary>
		public string Id
		{ get; set; }
		/// <summary>
		/// Nome da oficina.
		/// </summary>
		public string Name
		{ get; set; }
		/// <summary>
		/// Descricao da oficina.
		/// </summary>
		public string Description
		{ get; set; }
		/// <summary>
		/// O telefone de contato da oficina.
		/// </summary>
		public string Phone
		{ get; set; }
		/// <summary>
		/// O celular de contato da oficina.
		/// </summary>
		public string CellPhone
		{ get; set; }
		/// <summary>
		/// O CEP da oficina.
		/// </summary>
		public string CEP
		{ get; set; }
		/// <summary>
		/// O endereco da oficina.
		/// </summary>
		public string StreetLine1
		{ get; set; }
		/// <summary>
		/// O endereco da oficina.
		/// </summary>
		public string StreetLine2
		{ get; set; }
		/// <summary>
		/// O numero da oficina.
		/// </summary>
		public string Number
		{ get; set; }
		/// <summary>
		/// A cidade da oficina.
		/// </summary>
		public string City
		{ get; set; }
		/// <summary>
		/// O estado da oficina.
		/// </summary>
		public string State
		{ get; set; }
		/// <summary>
		/// O pais da oficina.
		/// </summary>
		public string Country
		{ get; set; }
		/// <summary>
		/// Posicao latitudinal da oficina.
		/// </summary>
		public string Latitude
		{ get; set; }
		/// <summary>
		/// Posicao longitudinal da oficina.
		/// </summary>
		public string Longitude
		{ get; set; }
		/// <summary>
		/// Periodo em que a oficina se encontra aberta
		/// </summary>
		public string WorkingPeriod
		{ get; set; }
		/// <summary>
		/// O email da oficina.
		/// </summary>
		public string Email
		{ get; set; }

		/// <summary>
		/// Oficina e a favorita do usuario.
		/// OBS: guardar estes dados no app.
		/// </summary>
		public bool IsFavorite
		{
			get
			{
				string str = PlayerPrefs.GetString("favoritesRepairShops", "");
				if (string.IsNullOrEmpty(str))
				{
					return false;
				}
				else
				{
					JSONNode data = JSON.Parse(str);
					for (int i = 0; i < data.Count; i++)
					{
						if (data[i].Value == Id)
						{
							return true;
						}
					}
				}
				return false;
			}
			set
			{
				if (value)
				{
					FavoriteRepairShop(int.Parse(Id));
				}
				else
				{
					UnfavoriteRepairShop(int.Parse(Id));
				}
			}
		}



		/// <summary>
		/// Construtor com todos os parametros do repairshop.
		/// </summary>
		/// <param name="id"></param>
		/// <param name="name"></param>
		/// <param name="desc"></param>
		/// <param name="phone"></param>
		/// <param name="cell"></param>
		/// <param name="cep"></param>
		/// <param name="streetLine1"></param>
		/// <param name="streetLine2"></param>
		/// <param name="number"></param>
		/// <param name="city"></param>
		/// <param name="state"></param>
		/// <param name="country"></param>
		/// <param name="email"></param>
		/// <param name="lat"></param>
		/// <param name="lng"></param>
		/// <param name="workingPeriod"></param>
		public RepairShop
			(
				string id,
				string name,
				string desc,
				string phone,
				string cell,
				string cep,
				string streetLine1,
				string streetLine2,
				string number,
				string city,
				string state,
				string country,
				string lat,
				string lng,
				string workingPeriod,
				string email
		)
		{
			Id = id;
			Name = name;
			Description = desc;
			Phone = phone;
			CellPhone = cell;
			CEP = cep;
			StreetLine1 = streetLine1;
			StreetLine2 = streetLine2;
			Number = number;
			City = city;
			State = state;
			Country = country;
			Latitude = lat;
			Longitude = lng;
			WorkingPeriod = workingPeriod;
			Email = email;

		}



		#region UserRepairShopStars


		public void FavoriteRepairShop(int id)
		{
			JSONNode data;
			string str = PlayerPrefs.GetString("favoritesRepairShops", "");
			if (string.IsNullOrEmpty(str))
			{
				data = new JSONArray();
				data.Add("id", id.ToString());
			}
			else
			{
				data = JSON.Parse(str);
				for (int i = 0; i < data.Count; i++)
				{
					if (data[i].Value == id.ToString())
					{
						return;
					}
				}
				data.Add("", id.ToString());
			}
			PlayerPrefs.SetString("favoritesRepairShops", data.ToString());
		}



		public void UnfavoriteRepairShop(int id)
		{
			JSONNode data;
			string str = PlayerPrefs.GetString("favoritesRepairShops", "");
			if (string.IsNullOrEmpty(str))
			{
				return;
			}
			else
			{
				data = JSON.Parse(str);
				for (int i = 0; i < data.Count; i++)
				{
					if (data[i].Value == id.ToString())
					{
						data.Remove(i);
					}
				}
			}
			PlayerPrefs.SetString("favoritesRepairShops", data.ToString());
		}

		#endregion UserRepairShopStars



		#region StaticMethods

		/// <summary>
		/// Cria uma oficina a partir de uma string json.
		/// </summary>
		/// <param name="json">string contendo os dados em formato json</param>
		/// <returns>objeto do tipo RepairShop.</returns>
		public static RepairShop CreateRepairShop(string json)
		{
			try
			{
				return CreateRepairShop(JSONNode.Parse(json));
			}
			catch
			{
				return null;
			}
		}



		/// <summary>
		/// Cria uma oficina a partir de um json node.
		/// </summary>
		/// <param name="node"> json node</param>
		/// <returns>objeto do tipo RepairShop.</returns>
		public static RepairShop CreateRepairShop(JSONNode node)
		{
			if (string.IsNullOrEmpty(node["id"].Value))
			{
				return null;
			}
			return new RepairShop
						(
							node["id"].Value,
							node["name"].Value,
							node["description"].Value,
							node["phone"].Value,
							node["cell"].Value,
							node["cep"].Value,
							node["streetLine1"].Value,
							node["streetLine2"].Value,
							node["number"].Value,
							node["city"].Value,
							node["state"].Value,
							node["country"].Value,
							node["lat"].Value,
							node["long"].Value,
							node["workingPeriod"].Value,
							node["email"].Value
			);
		}



		/// <summary>
		/// Gets all the favorites repairshops marked by the user.
		/// </summary>
		/// <returns>array with all favorite repair shops.</returns>
		public static int[] GetAllFavoriteIDs()
		{
			int[] favorites;

			JSONNode data;
			string str = PlayerPrefs.GetString("favoritesRepairShops", "");
			if (string.IsNullOrEmpty(str))
			{
				favorites = null;
			}
			else
			{
				data = JSON.Parse(str);
				favorites = new int[data.Count];
				for (int i = 0; i < data.Count; i++)
				{
					favorites[i] = int.Parse(data[i].Value);
				}
			}

			return favorites;

		}
		#endregion StaticMethods
	}
}
