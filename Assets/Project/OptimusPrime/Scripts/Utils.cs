﻿namespace MotorTools.Utils
{
	using System.Collections;
	using UnityEngine;

	// namespace marcel.utils 
	/// <summary>
	/// Utils. A lot of functions without instantiation of objects. 
	/// </summary>

	public class Utils
	{
		public static string ColorToHex(Color32 color)
		{
			string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
			return hex;
		}

		public static Color HexToColor(string hex)
		{
			byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
			byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
			byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
			return new Color32(r, g, b, 255);
		}

		/// <summary>
		/// Calculates the percentage of a given current value between the specified [Min, Max] values. 
		/// </summary>
		/// <param name="paramMax">
		/// Parameter max. 
		/// </param>
		/// <param name="paramMin">
		/// Parameter minimum. 
		/// </param>
		/// <param name="paramCurrent">
		/// Parameter current. 
		/// </param>
		public static float percentage(float paramMin, float paramMax, float paramCurrent)
		{
			if ((paramMax - paramMin) == 0)
				return paramCurrent > 0 ? 1 : paramCurrent < 0 ? -1 : 0;

			return (float) ((paramCurrent - paramMin) / (paramMax - paramMin));
		}

		/// <summary>
		/// Calculates the percentage of a given current value between the specified [0, Max] values. 
		/// </summary>
		/// <param name="paramMax">
		/// Parameter max. 
		/// </param>
		/// <param name="paramCurrent">
		/// Parameter current. 
		/// </param>
		public static float percentage(float paramMax, float paramCurrent)
		{
			return percentage(0.0f, paramMax, paramCurrent);
		}






		/// <summary>
		/// Radius of the Earth in Kilometers.
		/// </summary>
		public const double EARTH_RADIUS_KM = 6371;

		/// <summary>
		/// Converts an angle to a radian.
		/// </summary>
		/// <param name="input">The angle that is to be converted.</param>
		/// <returns>The angle in radians.</returns>
		public static double ToRad(double input)
		{
			return input * (System.Math.PI / 180);
		}

		/// <summary>
		/// Calculates the distance between two geo-points in kilometers using the Haversine algorithm.
		/// </summary>
		/// <param name="latitude1">first point lat</param>
		/// <param name="longitude1">first point lng</param>
		/// <param name="latitude2">seccond point lat</param>
		/// <param name="longitude2">seccond point lng</param>
		/// <returns>A double indicating the distance between the points in KM.</returns>
		public static double GetDistanceKM(double latitude1, double longitude1, double latitude2, double longitude2)
		{
			double dLat = ToRad(latitude2 - latitude1);
			double dLon = ToRad(longitude2 - longitude1);

			double a = System.Math.Pow(System.Math.Sin(dLat / 2), 2) +
					   System.Math.Cos(ToRad(latitude1)) * System.Math.Cos(ToRad(latitude2)) *
					   System.Math.Pow(System.Math.Sin(dLon / 2), 2);

			double c = 2 * System.Math.Atan2(System.Math.Sqrt(a), System.Math.Sqrt(1 - a));

			double distance = EARTH_RADIUS_KM * c;
			return distance;
		}

	}
}