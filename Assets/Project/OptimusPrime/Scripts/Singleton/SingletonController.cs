﻿using UnityEngine;
using System.Collections;

public class SingletonController<T> : Singleton<T> where T : MonoBehaviour
{

#region Attributes


	protected bool hasInitiated = false;


#endregion Attributes
#region Properties


	public bool HasInitiated
	{
		get{return hasInitiated;}
	}


#endregion Properties
#region Methods

	
	public virtual IEnumerator Init()
	{
		yield break;
	}

	protected void Initiated()
	{
		hasInitiated = true;
	}


#endregion Methods
#region MonobehaviourMethods





#endregion MonobehaviourMethods

}
