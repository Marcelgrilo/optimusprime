﻿namespace MotorTools.Views
{
	using UnityEngine;
	using System.Collections;
	using System.Collections.Generic;
	using MotorTools.Models;
	using Data;
	using Marcel.JSON;

	public class FavoriteAddressesView : IView
	{

		/// <summary>
		/// Lista de modelos de enderecos .
		/// </summary>
		private List<Address> _addressesModels;


		/// <summary>
		/// Componente da view usado para faer u viewport da janela.
		/// </summary>
		public GameObject AddressViewUI;

		/// <summary>
		/// Prefab do item de lista.
		/// </summary>
		public GameObject AddressItemPrefab;

		/// <summary>
		/// O gameobject da Camera dragavel.
		/// </summary>
		public GameObject DraggableCameraObject;

		/// <summary>
		/// Transform pai dos objetos da lista.
		/// </summary>
		public Transform ListHolder;

		/// <summary>
		/// Campo de texto para inserção do endereco.
		/// </summary>
		[Space(10)]
		public UIInput InputField;

		/// <summary>
		/// Botao de adicionar enderecos na lista de enderecos.
		/// </summary>
		public GameObject AddAddressButton;

		/// <summary>
		/// Botao de busca de servicos.
		/// </summary>
		[Space(10)]
		public GameObject SearchServiceButton;

		/// <summary>
		/// UnityMonoBehaviour Awake: roda antes de toda a inicialização deste script.
		/// </summary>
		public void Awake()
		{
			_addressesModels = new List<Address>();
		}



		/// <summary>
		/// Popula a lista de endereços com os endereços do usuario.
		/// </summary>
		/// <returns></returns>
		public IEnumerator PopulateList()
		{
			LoadingView.Instance.Enable();
			//
			// Desliga a camera para que o usuario nao observe a remocao dos itens de revisao antigos.
			DraggableCameraObject.GetComponent<Camera>().enabled = false;
			yield return null;
			//
			// Limpa a lista de repairShop models.
			_addressesModels.Clear();
			//
			// Garante que nenhum item esteja no transform onde serao instanciados os novos itens.
			foreach (Transform v in ListHolder)
			{
				GameObject.Destroy(v.gameObject);
			}
			// 
			// Agora que nao possui mais itens na lista nem no transform liga a camera para que o usuario acompanhe o carregamento de itens.
			DraggableCameraObject.GetComponent<Camera>().enabled = true;
			yield return null;
			//
			// busca por tosos os enderecos favoritos na menoria do dispositivo.
			Address[] addresses = Address.GetAllAddresses();
			if (addresses != null && addresses.Length > 0)
			{
				foreach (var v in addresses)
				{
					AddAddressItemPanel(v);
				}
			}
			UnselectAll();
			//
			// FUTURO: buscar por todos os endereços favoritos no server.

			yield return null;

			LoadingView.Instance.Disable();

		}



		/// <summary>
		/// Adiciona um endereço inserido pelo usuario.
		/// </summary>
		/// <param name="a"></param>
		private void AddAddressItemPanel(Address a)
		{
			_addressesModels.Add(a);

			GameObject panel = Object.Instantiate(AddressItemPrefab);

			panel.GetComponent<AddressItemPanel>().Set(a, OnAddresItemPanelSelected, OnRemoveAddress);

			panel.transform.SetParent(ListHolder);
			panel.transform.localScale = Vector3.one;

			panel.GetComponent<UIDragCamera>().draggableCamera = DraggableCameraObject.GetComponent<UIDraggableCamera>();

			panel.GetComponent<AddressItemPanel>().OnSelectAddress();

			//	EventDelegate.Add(AlgumBotao.onClick, AlgumaFuncao);

			ListHolder.GetComponent<UITable>().repositionNow = true;
		}



		/// <summary>
		/// Indice do item da lista selecionado.
		/// </summary>
		int selectedIndex = -1;



		/// <summary>
		/// Metodo chamado quando um item de endereco e selecionado na lista de enderecos.
		/// </summary>
		/// <param name="enable">valor de ativacao/desativacao.</param>
		/// <param name="index">indice do item na lista.</param>
		public void OnAddresItemPanelSelected(bool enable, int index)
		{
			if (selectedIndex == -1)
			{
				ListHolder.GetChild(index).GetComponent<AddressItemPanel>().SetSelected(enable);
				selectedIndex = index;
			}
			else
			{
				if (selectedIndex == index)
				{
					ListHolder.GetChild(index).GetComponent<AddressItemPanel>().SetSelected(false);
					selectedIndex = -1;
				}
				else
				{
					ListHolder.GetChild(selectedIndex).GetComponent<AddressItemPanel>().SetSelected(!enable);
					ListHolder.GetChild(index).GetComponent<AddressItemPanel>().SetSelected(enable);
					selectedIndex = index;
				}
			}
			SetSearchServiceButton(selectedIndex != -1);
		}

		public void UnselectAll()
		{
			foreach (Transform t in ListHolder)
			{
				t.GetComponent<AddressItemPanel>().SetSelected(false);
			}
			selectedIndex = -1;
			SetSearchServiceButton(selectedIndex != -1);
		}

		/// <summary>
		/// Metodo chamado quando se remove um item da lista.
		/// </summary>
		/// <param name="index">indice a ser removido.</param>
		public void OnRemoveAddress(int index)
		{
			if (ListHolder.GetChild(index).GetSiblingIndex() == selectedIndex)
				selectedIndex = -1;

			Address.RemoveAddresses(ListHolder.GetChild(index).GetComponent<AddressItemPanel>().Model);
			_addressesModels.Remove(ListHolder.GetChild(index).GetComponent<AddressItemPanel>().Model);
			Destroy(ListHolder.GetChild(index).gameObject);
			UnselectAll();
			ListHolder.GetComponent<UITable>().repositionNow = true;
		}


		/// <summary>
		/// havilita ou desabilita o botao de encontrar servicos de acordo com o endereco selecionado.
		/// </summary>
		/// <param name="enable"></param>
		public void SetSearchServiceButton(bool enable)
		{
			SearchServiceButton.SetActive(enable);
		}


		/// <summary>
		/// Endereco validado.
		/// </summary>
		private Address _validatedAddress;
		private Address ValidatedAddress
		{
			get { return _validatedAddress; }
			set
			{
				_validatedAddress = value;
				AddAddressButton.SetActive(_validatedAddress != null);
			}
		}


		/// <summary>
		/// valida se um endereco e valido com georeferencia.
		/// </summary>
		/// <returns></returns>
		private IEnumerator ValidateAddress()
		{
			if (!string.IsNullOrEmpty(InputField.value) && InputField.value.Length >= 15)
			{
				//
				// Criando a url com o endereco do usuario.
				var uri = new System.Uri(URLs.GoogleGeoReferencing + InputField.value);
				//
				// Georefereincing.
				var www = new WWW(uri.AbsoluteUri);
				//
				// Espera pela resposta.
				yield return www;
				//
				// Se houver algum erro.
				if (!string.IsNullOrEmpty(www.error))
				{
					ValidatedAddress = null;
					yield break;
				}
				//
				// Convertendo o dado recebido para json.
				JSONNode node = JSON.Parse(www.text);
				//
				// Se houve erro no retorno dos dados.
				if (node["status"].Value != "OK")
				{
					ValidatedAddress = null;

					yield break;
				}
				//
				// criando o endereço para o app.
				ValidatedAddress = Address.CreateFromGoogleJson(node, InputField.value);
				AddAddressButton.SetActive(true);
				yield break;
			}
			else
				ValidatedAddress = null;
			yield return null;
		}



		#region ViewEvents


		/// <summary>
		/// Input field submit listenner 
		/// </summary>
		public void OnInputAddressSubmited()
		{
			Debug.Log("OnInputAddressSubmited");
			//
			//validates the user input address.
			StopCoroutine(ValidateAddress());
			ValidatedAddress = null;
			StartCoroutine(ValidateAddress());
		}
		/// <summary>
		/// Input field change listenner
		/// </summary>
		public void OnInputAddressChanged()
		{
			Debug.Log("OnInputAddressChanged");
			//
			//validates the user input address.
			StopCoroutine(ValidateAddress());
			ValidatedAddress = null;
			StartCoroutine(ValidateAddress());
		}



		public void OnAddAddress()
		{
			if (_validatedAddress != null)
			{
				AddAddressItemPanel(_validatedAddress);
				Address.SaveAddress(_validatedAddress);
				InputField.value = "";
				ValidatedAddress = null;
			}
		}



		public void OnSearchServiceButton()
		{
			//TODO: Ir para a tela de lista de oficinas ou para a tela do mapa.
		}



		#endregion ViewEvents




		#region IViewOverrides

		public override void ActivateView()
		{
			base.ActivateView();
			AddressViewUI.SetActive(true);
			AddAddressButton.SetActive(false);
			StartCoroutine(PopulateList());
		}



		public override void DeactivateView()
		{
			base.DeactivateView();
			StopCoroutine(PopulateList());
			AddressViewUI.SetActive(false);
			Address.SaveAddresses(_addressesModels.ToArray());
		}



		public override void Back()
		{
			base.Back();
		}

		#endregion IViewOverrides


	}
}