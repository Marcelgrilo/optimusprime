﻿namespace MotorTools.Views
{

	using UnityEngine;
	using System.Collections;
	using System.Collections.Generic;
	using Marcel.JSON;
	using MotorTools.Models;
	using MotorTools.Data;
	using Controllers;

	public class FavoriteRepairShopView : IView
	{

		/// <summary>
		/// Lista dos modelos carregados.
		/// </summary>
		private List<RepairShop> _repairShopModelList;
		/// <summary>
		/// A lista de oficinas instanciadas.
		/// </summary>
		private List<RepairShopItemPanel> _repairShopPanels;


		/// <summary>
		/// Prefab do painel de repair shop.
		/// </summary>u 
		public GameObject RepairShopPanelPrefab;
		/// <summary>
		/// Instancia do botao presente no LastItemPrefab, que é o ultimo objeto da lista.
		/// </summary>
		public GameObject RequestAllBudgetButton;
		/// <summary>
		/// View contendo a lista escrolavel de oficinas.
		/// </summary>
		public GameObject UIRepairShopView;
		/// <summary>
		/// 
		/// </summary>
		public GameObject DraggableCameraObject;
		/// <summary>
		/// Transform onde serao instanciados os paineis das oficinas.
		/// </summary>
		public Transform ListHolder;

		/// <summary>
		/// A view a ser ativada quando um botao é pressionado.
		/// </summary>
		public IView ContactRepairShopview;



		/// <summary>
		/// UnityMonoBehaviour Awake: roda antes de toda a inicialização deste script.
		/// </summary>
		public void Awake()
		{
			_repairShopPanels = new List<RepairShopItemPanel>();
			_repairShopModelList = new List<RepairShop>();
		}



		#region IViewMethods


		/// <summary>
		/// Ativa esta view e inicializa seus elementos.
		/// </summary>
		public override void ActivateView()
		{
			base.ActivateView();
			//
			// Ativa a view dragavel.
			UIRepairShopView.SetActive(true);
			//
			// Populates the list.
			StartCoroutine(PopulateList());
		}



		/// <summary>
		/// Ao desativar esta view.
		/// </summary>
		public override void DeactivateView()
		{
			base.DeactivateView();
			//
			// Stop this coroutine.
			StopCoroutine(PopulateList());
			//
			// Desativa a view dragavel.
			UIRepairShopView.SetActive(false);
		}



		/// <summary>
		/// Volta para a tela anterior.
		/// </summary>
		public override void Back()
		{
			base.Back();
		}



		#endregion IViewMethods



		/// <summary>
		/// Verifica se ha pelomenos um checkmark ativo para habilitar o botao, caso contrario mantém ele desabilitado.
		/// </summary>
		public void OnAnyCheckMarkClicked()
		{
			foreach (var v in _repairShopPanels)
			{
				if (v.IsCheck)
				{
					ApplicationController.Instance.SelectedRepairShop=v.Model;
					RequestAllBudgetButton.SetActive(true);
					return;
				}
			}
			RequestAllBudgetButton.SetActive(false);
		}



		/// <summary>
		/// Quando se faz a requisição de varias oficinas.
		/// </summary>
		public void OnRequestPrices()
		{
			//Debug.Log("OnRequestPrices clicked");
			if (ApplicationController.Instance.SelectedRepairShop!=null)
			{
				ContactRepairShopview.ActivateView();
			}
		}




		/// <summary>
		/// Inicializa as listas de oficinas.
		/// </summary>
		/// <returns>Ienumerator da corotina.</returns>
		private IEnumerator PopulateList()
		{
			LoadingView.Instance.Enable();
			//
			// Desliga a camera para que o usuario nao observe a remocao dos itens de revisao antigos.
			DraggableCameraObject.GetComponent<Camera>().enabled = false;
			yield return null;
			//
			// Limpa a lista de repairShop models.
			_repairShopModelList.Clear();
			//
			// Remove todos os itens na lista de paineis
			_repairShopPanels.Clear();
			//
			// Garante que nenhum item esteja no transform onde serao instanciados os novos itens.
			foreach (Transform v in ListHolder)
			{
				try
				{
					v.GetComponent<RepairShopItemPanel>().RemoveStarListenner(OnStarChanged);
				}
				catch { }
				GameObject.Destroy(v.gameObject);
			}
			// 
			// Agora que nao possui mais itens na lista nem no transform liga a camera para que o usuario acompanhe o carregamento de itens.
			DraggableCameraObject.GetComponent<Camera>().enabled = true;
			yield return null;
			//
			// busca por todos os ids de oficinas favoritas.
			int[] favoriteIds = RepairShop.GetAllFavoriteIDs();
			// 
			// se nao ha oficinas favoritas sai do metodo.
			if (favoriteIds == null)
				yield break;
			//
			// inicia o carregamento do banco de dados 
			JSONNode node = new JSONClass();
			//
			//busca por todas as oficinas.
			//TODO: fazer com que a requisição seja apenas uma, ou seja, envia-se uma lista de ids
			for (int i = 0; i < favoriteIds.Length; i++)
			{
				//
				// inicializando os forms.
				var form = new WWWForm();
				form.AddField(Strings.Id, favoriteIds[i]);
				//
				// monta a requisicao com url e formulario.
				var request = new WWW(URLs.BaseURL + URLs.Slash + URLs.RepairShop, form);
				//
				// espera pela requisicao.
				yield return request;
				//Debug.Log(request.text);
				//
				// adiciona o resultado no json... 
				node.Add(JSON.Parse(request.text));
			}

			//
			// parse do json recebido do banco de dados e criacao do painel de dados.
			for (int i = 0; i < node.Count; i++)
			{
				yield return null;
				RepairShop r = RepairShop.CreateRepairShop(node[i]);

				if (r == null)
				{
					//
					// isso significa que o registro desta oficina nao existe mais por algum motivo.
					continue;
				}

				_repairShopModelList.Add(r);

				GameObject panel = Object.Instantiate(RepairShopPanelPrefab);

				panel.GetComponent<RepairShopItemPanel>().Set(r, ContactRepairShopview);
				panel.GetComponent<RepairShopItemPanel>().AddStarListenner(OnStarChanged);
				_repairShopPanels.Add(panel.GetComponent<RepairShopItemPanel>());

				panel.transform.SetParent(ListHolder);
				panel.transform.localScale = Vector3.one;

				panel.GetComponent<UIDragCamera>().draggableCamera = DraggableCameraObject.GetComponent<UIDraggableCamera>();

				EventDelegate.Add(panel.transform.FindChild("Checkbox").gameObject.GetComponent<UIButton>().onClick, OnAnyCheckMarkClicked);

				ListHolder.GetComponent<UITable>().repositionNow = true;
			}


			RequestAllBudgetButton.SetActive(false);
			EventDelegate.Remove(RequestAllBudgetButton.GetComponent<UIButton>().onClick, OnRequestPrices);
			EventDelegate.Add(RequestAllBudgetButton.GetComponent<UIButton>().onClick, OnRequestPrices);

			yield return null;

			LoadingView.Instance.Disable();
		}


		public void OnStarChanged(bool state, int index)
		{
			//
			// Se desmarcou.
			if (!state)
			{
				_repairShopModelList.Remove(ListHolder.GetChild(index).GetComponent<RepairShopItemPanel>().Model);
				try
				{
					ListHolder.GetChild(index).GetComponent<RepairShopItemPanel>().RemoveStarListenner(OnStarChanged);
				}
				catch { }
				Destroy(ListHolder.GetChild(index).gameObject);
				ListHolder.GetComponent<UITable>().repositionNow = true;
			}
		}



	}
}