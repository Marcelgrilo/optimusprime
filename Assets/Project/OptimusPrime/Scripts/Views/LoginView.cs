﻿

namespace MotorTools.Views
{


	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using Marcel.JSON;
	using System.Linq;
	using MotorTools.Controllers;
	using MotorTools.Data;
	using MotorTools.Models;
	using MotorTools.Views.Utils;



	public class LoginView : IView
	{

		public EngineAnimator EngineAnimator;

		JSONNode jsonData;

		public DashboardView DashboardView;



		[Space(10)]
		public UIButton
				UibBrands;
		public UILabel UilBrands;
		Brand selectedBrand = null;
		List<Brand> brandsList = new List<Brand>();


		[Space(10)]
		public UIButton
				UibCars;
		public UILabel UilCars;
		string selectedCar = "";
		List<string> carsList = new List<string>();


		[Space(10)]
		public UIButton
				UibYears;
		public UILabel UilYears;
		string selectedYear = "";
		List<string> yearsList = new List<string>();


		[Space(10)]
		public UIButton
				UibVersions;
		public UILabel UilVersions;
		string selectedVersion = "";
		List<string> versionsList = new List<string>();


		[Space(10)]
		public UILabel
				UilKm;
		public UIButton UibKm;


		[Space(10)]
		public GameObject
				ButtonOk;


		[Space(20)]
		public SelectNumberView
				SelectNumberView;
		[Space(5)]
		public Transform SelectNumberTopLeft;
		public Transform SelectNumberBottomRight;
		[Space(5)]
		public Transform SelectTopLeft;
		public Transform SelectBottomRight;

		[Space(20)]
		public GameObject
				SelectBlockInput;
		public GameObject SelectNumberBlockInput;



		public override void ActivateView()
		{
			base.ActivateView();


			EngineAnimator.EnableEngine(true);
			if (VehicleController.Instance.SelectedVehiacle == null)
			{
				VehicleController.Instance.SelectedVehiacle = Vehicle.LoadVehicleLoginData();
			}
			if (VehicleController.Instance.SelectedVehiacle.Year == 0)
			{
				StartCoroutine(PopulateBrands());
			}
			else
			{
				StartCoroutine(PopulateFromSavedLoginData());

			}
		}


		public override void DeactivateView()
		{
			base.DeactivateView();

			EngineAnimator.EnableEngine(false);

			EventDelegate.Remove(UibBrands.onClick, BrandsClicked);

			Clear(LoginClear.All);
		}


		public void Login()
		{
			DashboardView.ActivateView();
		}




		IEnumerator PopulateFromSavedLoginData()
		{
			//yield break;
			LoadingView.Instance.Enable();

			Clear(LoginClear.All);
			if (Application.internetReachability != NetworkReachability.NotReachable)
			{
				yield return StartCoroutine(PopulateBrands());
				LoadingView.Instance.Enable();
			}
			yield return null;
			//selectedBrand = VehicleController.Instance.SelectedVehiacle.Brand;
			//UilBrands.text = selectedBrand.ToUpper();
			selectedBrand = brandsList.First(a => a.Id == VehicleController.Instance.SelectedVehiacle.BrandId);
			UilBrands.text = selectedBrand.Name.ToUpper();
			Clear(LoginClear.CarAhead);
			if (Application.internetReachability != NetworkReachability.NotReachable)
			{
				//StopCoroutine(PopulateCars(selectedBrand));
				//yield return StartCoroutine(PopulateCars(selectedBrand));
				StopCoroutine(PopulateCars(selectedBrand.Id));
				yield return StartCoroutine(PopulateCars(selectedBrand.Id));
				LoadingView.Instance.Enable();
			}
			yield return null;
			selectedCar = VehicleController.Instance.SelectedVehiacle.Model;
			UilCars.text = selectedCar.ToUpper();

			Clear(LoginClear.YearAhead);
			if (Application.internetReachability != NetworkReachability.NotReachable)
			{
				//StopCoroutine(PopulateYear(selectedBrand.Id, selectedCar));
				//yield return StartCoroutine(PopulateYear(selectedBrand.Id, selectedCar));
				StopCoroutine(PopulateYear(selectedBrand.Id, selectedCar));
				yield return StartCoroutine(PopulateYear(selectedBrand.Id, selectedCar));
				LoadingView.Instance.Enable();
			}
			yield return null;
			selectedYear = VehicleController.Instance.SelectedVehiacle.Year.ToString();
			UilYears.text = selectedYear;

			Clear(LoginClear.VersionAhead);
			if (Application.internetReachability != NetworkReachability.NotReachable)
			{
				//StopCoroutine(PopulateVersion(selectedBrand.Id, selectedCar, selectedYear));
				//yield return StartCoroutine(PopulateVersion(selectedBrand.Id, selectedCar, selectedYear));
				StopCoroutine(PopulateVersion(selectedBrand.Id, selectedCar, selectedYear));
				yield return StartCoroutine(PopulateVersion(selectedBrand.Id, selectedCar, selectedYear));
				LoadingView.Instance.Enable();
			}
			yield return null;
			selectedVersion = VehicleController.Instance.SelectedVehiacle.Version;
			UilVersions.text = selectedVersion.ToUpper();

			Clear(LoginClear.KmAhead);
			UibKm.enabled = true;
			EventDelegate.Add(UibKm.onClick, KmClicked);
			UilKm.enabled = true;
			UilKm.text = VehicleController.Instance.SelectedVehiacle.InitialKm.ToString();

			ButtonOk.SetActive(true);
			EventDelegate.Add(ButtonOk.GetComponent<UIButton>().onClick, OnOkButton);

			LoadingView.Instance.Disable();
		}


		public IEnumerator PopulateBrands()
		{
			LoadingView.Instance.Enable();


			Clear(LoginClear.All);

			yield return new WaitForSeconds(0.1f);

			var getBrands = new WWW(URLs.BaseURL + URLs.Slash + URLs.Brands);

			yield return getBrands;

			//Debug.Log(getBrands.text);

			jsonData = JSON.Parse(getBrands.text);

//			Debug.Log(jsonData.ToString());

			//Debug.Log(getBrands.text);
			for (int i = 0; i < jsonData.Count; i++)
			{
				brandsList.Add(new Brand(jsonData[i][0].Value.ToUpper(), jsonData[i][1].Value.ToUpper()));
				//Debug.Log(jsonData[i][0].Value.ToUpper()+"   "+ jsonData[i][1].Value.ToUpper());
			}

			UilBrands.text = Localization.Get(Strings.SelectBrand);

			EventDelegate.Add(UibBrands.onClick, BrandsClicked);

			UibBrands.enabled = true;

			yield return 0;


			LoadingView.Instance.Disable();
		}


		public IEnumerator PopulateCars(string brand)
		{
			LoadingView.Instance.Enable();


			Clear(LoginClear.CarAhead);

			carsList.Clear();

			var form = new WWWForm();
			form.AddField(URLs.Brand, brand);

			var getCars = new WWW(URLs.BaseURL + URLs.Slash + URLs.Models, form);

			yield return getCars;

			jsonData = JSON.Parse(getCars.text);

			for (int i = 0; i < jsonData.Count; i++)
			{
				carsList.Add(jsonData[i][0].Value.ToUpper());
			}

			UilCars.text = Localization.Get(Strings.SelectCar);

			EventDelegate.Add(UibCars.onClick, CarsClicked);

			UibCars.enabled = true;

			yield return 0;


			LoadingView.Instance.Disable();
		}


		public IEnumerator PopulateYear(string brand, string model)
		{
			LoadingView.Instance.Enable();


			Clear(LoginClear.YearAhead);

			yearsList.Clear();

			var form = new WWWForm();
			form.AddField(URLs.Brand, brand);
			form.AddField(URLs.Model, model);

			var getYear = new WWW(URLs.BaseURL + URLs.Slash + URLs.Years, form);

			yield return getYear;

			jsonData = JSON.Parse(getYear.text);

			for (int i = 0; i < jsonData.Count; i++)
			{
				yearsList.Add(jsonData[i][0].Value.ToUpper());
			}

			UilYears.text = Localization.Get(Strings.SelectYear);

			EventDelegate.Add(UibYears.onClick, YearsClicked);

			UibYears.enabled = true;

			yield return 0;


			LoadingView.Instance.Disable();
		}


		public IEnumerator PopulateVersion(string brand, string model, string year)
		{
			LoadingView.Instance.Enable();


			Clear(LoginClear.VersionAhead);

			versionsList.Clear();

			var form = new WWWForm();
			form.AddField(URLs.Brand, brand);
			form.AddField(URLs.Model, model);
			form.AddField(URLs.Year, year);

			var getVersion = new WWW(URLs.BaseURL + URLs.Slash + URLs.Versions, form);

			yield return getVersion;

			jsonData = JSON.Parse(getVersion.text);

			for (int i = 0; i < jsonData.Count; i++)
			{
				versionsList.Add(jsonData[i][0].Value.ToUpper());
			}

			UilVersions.text = Localization.Get(Strings.SelectVersion);

			EventDelegate.Add(UibVersions.onClick, VersionsClicked);

			UibVersions.enabled = true;

			yield return 0;


			LoadingView.Instance.Disable();
		}




		public enum LoginClear
		{
			All,
			CarAhead,
			YearAhead,
			VersionAhead,
			KmAhead,
			Button
		}


		void Clear(LoginClear flag)
		{
			if (
					(flag != LoginClear.Button && flag != LoginClear.KmAhead && flag != LoginClear.VersionAhead && flag != LoginClear.YearAhead && flag != LoginClear.CarAhead) ||
					flag == LoginClear.All
				)
			{
				//selectedBrand = "";
				selectedBrand = null;
				UibBrands.enabled = false;
				EventDelegate.Remove(UibBrands.onClick, BrandsClicked);
				UilBrands.text = Localization.Get("selectBrand");
				brandsList.Clear();
			}
			if (
					(flag != LoginClear.Button && flag != LoginClear.KmAhead && flag != LoginClear.VersionAhead && flag != LoginClear.YearAhead) ||
					flag == LoginClear.All
				)
			{
				selectedCar = "";
				UibCars.enabled = false;
				EventDelegate.Remove(UibCars.onClick, CarsClicked);
				carsList.Clear();
				UilCars.text = "";
			}
			if (
					(flag != LoginClear.Button && flag != LoginClear.KmAhead && flag != LoginClear.VersionAhead) ||
					flag == LoginClear.All
				)
			{
				selectedYear = "";
				UibYears.enabled = false;
				EventDelegate.Remove(UibYears.onClick, YearsClicked);
				yearsList.Clear();
				UilYears.text = "";
			}
			if (
					(flag != LoginClear.Button && flag != LoginClear.KmAhead) ||
					flag == LoginClear.All
				)
			{
				selectedVersion = "";
				UibVersions.enabled = false;
				EventDelegate.Remove(UibVersions.onClick, VersionsClicked);
				versionsList.Clear();
				UilVersions.text = "";
			}
			if (
					flag != LoginClear.Button ||
					flag == LoginClear.All
				)
			{
				UibKm.enabled = false;
				EventDelegate.Remove(UibKm.onClick, KmClicked);

				//UilKm.onChange -= KmChanged;
				UilKm.text = "";
				UilKm.enabled = false;
			}

			ButtonOk.SetActive(false);
			EventDelegate.Remove(ButtonOk.GetComponent<UIButton>().onClick, OnOkButton);

		}







		#region Listeners


		public void BrandsClicked()
		{
			List<string> strings = new List<string>();

			foreach (Brand b in brandsList)
			{
				strings.Add(b.Name);
			}

			UibBrands.gameObject.GetComponent<SelectView>().EnableView
			(
					new EventDelegate(BrandSelected),
					Localization.Get(Strings.SelectBrand),
			strings,
			SelectBlockInput,
			GetComponent<Camera>(),
			SelectTopLeft,
			SelectBottomRight);
		}

		public void BrandSelected()
		{

			UilBrands.text = Localization.Get(Strings.SelectBrand);

			//selectedBrand = brandsList.First(a => a.Name == UibBrands.gameObject.GetComponent<SelectView>().Selected).Id;
			selectedBrand = brandsList.First(a => a.Name == UibBrands.gameObject.GetComponent<SelectView>().Selected);

			UilBrands.text =
					//string.IsNullOrEmpty(selectedBrand) ?
					selectedBrand == null ?
						Localization.Get(Strings.SelectBrand).ToUpper() :
						//selectedBrand.Name.ToUpper();
						selectedBrand.Name.ToUpper();

			//if (selectedBrand != Localization.Get(Strings.SelectBrand) && selectedBrand != "")
			if (UilBrands.text != Localization.Get(Strings.SelectBrand) && selectedBrand != null)
			{
				Clear(LoginClear.CarAhead);
				StopCoroutine(PopulateCars(selectedBrand.Id));
				StartCoroutine(PopulateCars(selectedBrand.Id));
			}
			else
			{
				Clear(LoginClear.CarAhead);
			}
		}




		public void CarsClicked()
		{
			UibCars.gameObject.GetComponent<SelectView>().EnableView
	(
		new EventDelegate(CarSelected),
		Localization.Get(Strings.SelectCar),
		carsList,
		SelectBlockInput,
		GetComponent<Camera>(),
		SelectTopLeft,
		SelectBottomRight);
		}


		public void CarSelected()
		{
			UilCars.text = Localization.Get(Strings.SelectCar);
			selectedCar = UibCars.gameObject.GetComponent<SelectView>().Selected;

			UilCars.text =
					string.IsNullOrEmpty(selectedCar) ?
							Localization.Get(Strings.SelectCar).ToUpper() :
							selectedCar.ToUpper();

			if (selectedCar != Localization.Get(Strings.SelectCar) && selectedCar != "")
			{
				Clear(LoginClear.YearAhead);
				StopCoroutine(PopulateYear(selectedBrand.Id, selectedCar));
				StartCoroutine(PopulateYear(selectedBrand.Id, selectedCar));
			}
			else
			{
				Clear(LoginClear.YearAhead);

			}
		}




		public void YearsClicked()
		{
			UibYears.gameObject.GetComponent<SelectView>().EnableView
			(
				new EventDelegate(YearSelected),
				Localization.Get(Strings.SelectYear),
				yearsList,
				SelectBlockInput,
				GetComponent<Camera>(),
				SelectTopLeft,
				SelectBottomRight
			);

		}


		public void YearSelected()
		{
			UilYears.text = Localization.Get(Strings.SelectYear);
			selectedYear = UibYears.gameObject.GetComponent<SelectView>().Selected;

			UilYears.text =
					string.IsNullOrEmpty(selectedYear) ?
							Localization.Get(Strings.SelectYear).ToUpper() :
							selectedYear.ToUpper();

			if (selectedYear != Localization.Get(Strings.SelectYear) && selectedYear != "")
			{
				Clear(LoginClear.VersionAhead);
				StopCoroutine(PopulateVersion(selectedBrand.Id, selectedCar, selectedYear));
				StartCoroutine(PopulateVersion(selectedBrand.Id, selectedCar, selectedYear));
			}
			else
			{
				Clear(LoginClear.VersionAhead);

			}
		}




		public void VersionsClicked()
		{
			UibVersions.gameObject.GetComponent<SelectView>().EnableView
	(
		new EventDelegate(VersionSelected),
		Localization.Get(Strings.SelectVersion),
		versionsList,
		SelectBlockInput,
		GetComponent<Camera>(),
		SelectTopLeft,
		SelectBottomRight);

		}


		public void VersionSelected()
		{
			UilVersions.text = Localization.Get(Strings.SelectVersion);
			selectedVersion = UibVersions.gameObject.GetComponent<SelectView>().Selected;

			UilVersions.text =
					string.IsNullOrEmpty(selectedVersion) ?
							Localization.Get(Strings.SelectVersion).ToUpper() :
							selectedVersion.ToUpper();

			if (selectedVersion != Localization.Get(Strings.SelectVersion) && selectedVersion != "")
			{
				Clear(LoginClear.KmAhead);

				UibKm.enabled = true;
				EventDelegate.Add(UibKm.onClick, KmClicked);

				UilKm.enabled = true;
				UilKm.text = Localization.Get(Strings.SelectKm);
				//UilKm.onChange += KmChanged;
			}
			else
			{
				Clear(LoginClear.KmAhead);
			}
		}




		public void KmClicked()
		{
			SelectNumberView.EnableView
	(
		Localization.Get(Strings.SelectKm),
		new EventDelegate(KmChanged),
		GetComponent<Camera>(),
		SelectNumberTopLeft,
		SelectNumberBottomRight,
		SelectNumberBlockInput
			);
		}


		public void KmChanged()
		{
			UilKm.text = int.Parse(SelectNumberView.FinalValue).ToString();

			if (UilKm.text != "" || UilKm.text != Localization.Get(Strings.SelectKm))
			{
				ButtonOk.SetActive(true);
				EventDelegate.Add(ButtonOk.GetComponent<UIButton>().onClick, OnOkButton);
			}
			else
			{
				Clear(LoginClear.Button);
			}
		}


		public void OnOkButton()
		{


			VehicleController.Instance.SelectedVehiacle =
					new Vehicle(
					selectedBrand.Id,
					selectedCar,
					int.Parse(selectedYear),
					selectedVersion,
					float.Parse(UilKm.text),
					System.DateTime.Now
			);

			VehicleController.Instance.SelectedVehiacle.SaveVehicleLoginData();

			DeactivateView();
			DashboardView.ActivateView();
		}


		#endregion
	}


}
