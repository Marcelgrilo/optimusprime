namespace MotorTools.Views
{
	using System.Collections;
	using System.Collections.Generic;
	using Marcel.JSON;
	using MotorTools.Controllers;
	using MotorTools.Data;
	using MotorTools.Models;
	using MotorTools.Utils;
	using MotorTools.Views.Utils;
	using UnityEngine;

	public class DashboardView : IView
	{

		public IView MapView;
		[Space (30)]
		public UIDraggableCamera DragableCamera;
		public GameObject FirstObjectOnList;
		public GameObject RevisionItemPrefab;
		public Transform ObjectItemsParent;

		//public UIPopupList PopupListKmPerDay;
		public GameObject SelectNumberBlockInput;

		public Transform SelectNumberBottomRight;
		public Transform SelectNumberTopLeft;

		[Space (20)]
		public SelectNumberView
						SelectNumberView;

		public UIButton UibKmPerDayButton;
		public UILabel UilKmPerDayLabel;
		public BoxCollider UISliderCollider;

		public GameObject Footer;

		public GameObject ViewUI;
		//public GameObject LoadButton;

		private List<GameObject> itemList;

		private NGuiCalendarController myCalendarController;




		private NGuiCalendarController CalendarController
		{
			get
			{
				if (myCalendarController == null)
				{
					myCalendarController = (NGuiCalendarController) GameObject.FindObjectOfType<NGuiCalendarController>();
				}

				return myCalendarController;
			}
		}



		private List<GameObject> RevisionItemList
		{
			get
			{
				if (itemList == null)
				{
					itemList = new List<GameObject>();
				}
				return itemList;
			}
		}



		public override void ActivateView()
		{
			base.ActivateView();

			//LoadButton.SetActive (true);
			FirstObjectOnList.SetActive(false);
			DragableCamera.transform.localPosition =
					new Vector3(
					DragableCamera.transform.localPosition.x,
					0,
					DragableCamera.transform.localPosition.z
			);

			UibKmPerDayButton.enabled = true;
			EventDelegate.Add(UibKmPerDayButton.onClick, OnKmPerDayButtonClicked);
			UilKmPerDayLabel.text = VehicleController.Instance.SelectedVehiacle.PerDayKm.ToString();

			Footer.SetActive(false);

			ViewUI.SetActive(true);


			CalendarController.UpdateCalendar();
			UISliderCollider.enabled = false;

			if (ApplicationController.Instance.KMPerDay != 0)
			{
				VehicleController.Instance.SelectedVehiacle.PerDayKm = ApplicationController.Instance.KMPerDay;
				UilKmPerDayLabel.text = VehicleController.Instance.SelectedVehiacle.PerDayKm.ToString();
				StopCoroutine(LoadItems());
				StartCoroutine(LoadItems());
			}
			else
			{
				VehicleController.Instance.SelectedVehiacle.PerDayKm = 1;
				ApplicationController.Instance.KMPerDay = 1;
				UilKmPerDayLabel.text = VehicleController.Instance.SelectedVehiacle.PerDayKm.ToString();
				StopCoroutine(LoadItems());
				StartCoroutine(LoadItems());
			}

		}

		public override void DeactivateView()
		{
			base.DeactivateView();

			ViewUI.SetActive(false);

			FirstObjectOnList.SetActive(true);
			foreach (GameObject obj in RevisionItemList)
			{
				Destroy(obj);
				ObjectItemsParent.GetComponent<UITable>().repositionNow = true;
			}
			RevisionItemList.Clear();

			UibKmPerDayButton.enabled = false;
			EventDelegate.Remove(UibKmPerDayButton.onClick, OnKmPerDayButtonClicked);
			UilKmPerDayLabel.text = VehicleController.Instance.SelectedVehiacle.PerDayKm.ToString();
		}

		public override void Back()
		{
			StopAllCoroutines();
			base.Back();
		}



		public void OnEditCurrentKm()
		{
			VehicleController.Instance.SelectedVehiacle.CurrentKm = float.Parse(SelectNumberView.FinalValue);
			StopCoroutine(LoadItems());
			StartCoroutine(LoadItems());
		}



		public void OnKmPerDayChange()
		{
			if (SelectNumberView.FinalValue.Equals(VehicleController.Instance.SelectedVehiacle.PerDayKm.ToString()))
			{
				return;
			}

			VehicleController.Instance.SelectedVehiacle.PerDayKm = float.Parse(SelectNumberView.FinalValue);
			UilKmPerDayLabel.text = VehicleController.Instance.SelectedVehiacle.PerDayKm.ToString();

			ApplicationController.Instance.KMPerDay = (int) VehicleController.Instance.SelectedVehiacle.PerDayKm;

			StopCoroutine(LoadItems());
			StartCoroutine(LoadItems());
		}

		private IEnumerator LoadItems()
		{
			//LoadButton.SetActive (false);
			yield return 0;
			VehicleController.Instance.SelectedVehiacle.ResetRevisions();
			foreach (var item in RevisionItemList)
			{
				GameObject.Destroy(item);
			}
			RevisionItemList.Clear();
			yield return StartCoroutine(Populate());

			CalendarController.UpdateCalendar();
		}



		private void OnKmPerDayButtonClicked()
		{
			SelectNumberView.EnableView
			(
				Localization.Get(Strings.SelectDialyKm),
				new EventDelegate(OnKmPerDayChange),
				GetComponent<Camera>(),
				SelectNumberTopLeft,
				SelectNumberBottomRight,
				SelectNumberBlockInput
			);
		}



		public void OnCurrentKmButtonClicked()
		{
			SelectNumberView.EnableView
			(
				Localization.Get(Strings.SelectKm),
				new EventDelegate(OnEditCurrentKm),
				GetComponent<Camera>(),
				SelectNumberTopLeft,
				SelectNumberBottomRight,
				SelectNumberBlockInput
			);
		}



		private IEnumerator Populate()
		{
			myDisabledRevisionItems = 0;
			LoadingView.Instance.Enable();

			//LoadButton.GetComponent<BoxCollider>().enabled = false;

			var vehicle = VehicleController.Instance.SelectedVehiacle;

			var form = new WWWForm();
			form.AddField(URLs.Brand, vehicle.BrandId);
			form.AddField(URLs.Model, vehicle.Model);
			form.AddField(URLs.Year, vehicle.Year.ToString());
			form.AddField(URLs.Version, vehicle.Version);

			var getCarID = new WWW(URLs.BaseURL + URLs.Slash + URLs.VehicleID, form);

			yield return getCarID;

			//Debug.Log (getCarID.text);
			var caridJSON = JSON.Parse(getCarID.text);

			string carId = caridJSON[0]["id"].Value;

			// Debug.Log (getCarID.text +" "+ getCarID.url+ " " +carId); 
			VehicleController.Instance.SelectedVehiacle.CarId = carId;

			form = new WWWForm();
			form.AddField(URLs.CarID, carId);

			var getItens = new WWW(URLs.BaseURL + URLs.Slash + URLs.MaintenanceDefinitions, form);

			yield return getItens;
			//Debug.Log (getItens.url);
			//Debug.Log (getItens.text);

			var list = JSON.Parse(getItens.text);
			//			Debug.Log (list.Count); 

			FirstObjectOnList.SetActive(true);
			myFirstObjectOnListTextBuffer = FirstObjectOnList.GetComponentInChildren<UILabel>().text;
			for (int i = 0; i < list.Count; i++)
			{
				var revItem = new RevisionItem(
					list[i][Strings.Id],
					list[i][Strings.PartName],
					list[i][Strings.Months],
					list[i][Strings.Distance],
					list[i][Strings.VehicleId],
					list[i][Strings.MaintenaceType],
					list[i][Strings.KmVariation],
					list[i][Strings.NewDistance]
					);

				//				Debug.Log(list[i].ToString());

				VehicleController.Instance.SelectedVehiacle.AddRevItem(revItem);

				// instantiates a new revisionItem
				var go = Instantiate(RevisionItemPrefab);

				go.transform.SetParent(ObjectItemsParent);
				go.transform.localScale = Vector3.one;
				go.GetComponent<UIDragCamera>().draggableCamera = DragableCamera;

				// waits for the next frame to run the start method of the
				// RevisionItemController.
				yield return null;

				//
				// Adds the proper icon to the RevisionItem
				go.GetComponent<RevisionItemPanel>().SetItem
				(
						revItem,
						//Strings.RevisionIcons[Random.Range(0,Strings.RevisionIcons.Length)]
						Strings.RevisionIcons[Strings.GetRevisionIconsIndex(revItem.MaintenaceType)],
						MapView
				);

				RevisionItemList.Add(go);

				// dont show a revisionItem on its load.
				ShowRevisionItem(revItem, false);

				ObjectItemsParent.GetComponent<UITable>().repositionNow = true;
				yield return null;
			}
			//
			// the user registered revisions.
			foreach (ApplicationController.UserRegisterItem ri in ApplicationController.Instance.UserRegisterItems)
			{

				var revItem = new RevisionItem(
					ri.Id,
					ri.Name+"\n"+ ri.Description,
					ri.Month,
					ri.Km,
					carId,
					ri.MaintenaceType,
					"0",
					"0");
				VehicleController.Instance.SelectedVehiacle.AddRevItem(revItem);

				// instantiates a new revisionItem
				var go = Instantiate(RevisionItemPrefab);

				go.transform.parent = ObjectItemsParent;
				go.transform.localScale = Vector3.one;
				go.GetComponent<UIDragCamera>().draggableCamera = DragableCamera;

				// waits for the next frame to run the start method of the
				// RevisionItemController.
				yield return null;

				go.GetComponent<RevisionItemPanel>().SetItem
				(
						revItem,
						//Strings.RevisionIcons[Random.Range(0, Strings.RevisionIcons.Length)]
						Strings.RevisionIcons[Strings.GetRevisionIconsIndex(revItem.MaintenaceType)],
						MapView
				);



				RevisionItemList.Add(go);

				// dont show a revisionItem on its load.
				ShowRevisionItem(revItem, false);

				ObjectItemsParent.GetComponent<UITable>().repositionNow = true;
				yield return null;
			}

			yield return null;

			Footer.SetActive(true);

			LoadingView.Instance.Disable();
			UISliderCollider.enabled = true;

			StartCoroutine(FindNextRevision());
		}



		private int myDisabledRevisionItems;


		private string myFirstObjectOnListTextBuffer;



		public void UpdateRevisionItems()
		{
			foreach (GameObject go in RevisionItemList)
			{
				go.SetActive(false);
			}

			foreach (RevisionItem ri in VehicleController.Instance.SelectedVehiacle.RevisionsOnKm((int) VehicleController.Instance.SelectedVehiacle.CalendarSliderKm))
			{
				ShowRevisionItem(ri, true);
			}

		}



		public void ShowRevisionItem(RevisionItem otherRevisionItem, bool isShowing)
		{
			foreach (GameObject go in RevisionItemList)
			{
				if (go.GetComponent<RevisionItemPanel>().CompareRevisionItem(otherRevisionItem))
				{
					go.SetActive(isShowing);

					// if is disabling sum 1 else subtract;
					myDisabledRevisionItems += (!isShowing ? 1 : -1);
				}
			}
			FirstObjectOnList.GetComponentInChildren<UILabel>().text=myFirstObjectOnListTextBuffer;
			if (myDisabledRevisionItems == RevisionItemList.Count)
			{
				FirstObjectOnList.GetComponentInChildren<UILabel>().text = Localization.Get(Strings.NoRevisionItem);
			}
			ObjectItemsParent.GetComponent<UITable>().repositionNow = true;

		}



		public IEnumerator FindNextRevision()
		{
			//			Debug.Log(myDisabledRevisionItems);

			UISlider slider = UISliderCollider.gameObject.GetComponent<UISlider>();
			float initialValue = slider.value;
			if (RevisionItemList.Count == 0)
				yield break;

			for (float f = initialValue; f < 1; f += 0.01f)
			{
				slider.value = f;
				yield return null;
				if (myDisabledRevisionItems != RevisionItemList.Count)
				{
					yield break;
				}
			}
			yield return null;
		}



		internal void CalendarMarkerClicked(List<RevisionItem> MarkerInfo)
		{
			// disable all revision Item
			foreach (GameObject go in RevisionItemList)
			{
				go.SetActive(false);
			}

			//Show the items of the marker
			foreach (RevisionItem item in MarkerInfo)
			{
				ShowRevisionItem(item, true);
			}
		}
	}


}