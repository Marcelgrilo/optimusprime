﻿namespace MotorTools.Views
{
	using System;
	using System.Collections;
	using System.Text.RegularExpressions;
	using Data;
	using MotorTools.Controllers;
	using MotorTools.Models;
	using UnityEngine;

	public class ContactRepairShopView : IView
	{


		private RepairShop RepairShopItem
		{ get { return ApplicationController.Instance.SelectedRepairShop; } }


		#region RepairshopPanelData
		public UISprite RSMainSprite;
		public UILabel RSTitleLable;
		public UILabel RSDescriptionLabel;
		public UILabel RSDistanceLabel;
		public UILabel RSAddressLabel;
		public GameObject RSFavoriteStar;

		public UILabel RSTellTitleLable;
		public UILabel RSTellLable;
		#endregion RepairshopPanelData



		#region UserData
		public UIInput UserName;
		public UIInput UserEmail;
		public UIInput UserPhoneNumber;
		public GameObject SendEmailRequestButton;
		public GameObject CallForRepairShopButton;

		private bool _emailIsValid = false;
		private bool _telIsValid = false;
		private bool _nameIsValid = false;
		#endregion UserData



		#region Callback
		private bool _isCalling = false;
		#endregion Callback



		#region Navigation
		public IView ConfirmProfileView;
		#endregion Navigation



		#region IView
		public override void ActivateView()
		{
			base.ActivateView();
			RSTitleLable.text = RepairShopItem.Name;
			RSDescriptionLabel.text = RepairShopItem.Description;
			try
			{
				double lat1 = double.Parse(RepairShopItem.Latitude);
				double lng1 = double.Parse(RepairShopItem.Longitude);
				double lat2 = double.Parse(ApplicationController.Instance.CurrentUser.CurrentLocation.latitude.ToString());
				double lng2 = double.Parse(ApplicationController.Instance.CurrentUser.CurrentLocation.longitude.ToString());
				double value = MotorTools.Utils.Utils.GetDistanceKM
					(
					lat1, lng1, lat2, lng2
					);
				RSDistanceLabel.text = string.Format("à {0:#.##} Km", value);
			}
			catch (System.Exception)
			{
				RSDistanceLabel.text = "";
			}
			//TODO: define SpriteName for each repair shop.
			RSMainSprite.spriteName = "24Barra7";// item.WorkingPeriod;
			RSMainSprite.SetDirty();
			RSFavoriteStar.SetActive(RepairShopItem.IsFavorite);

			RSTellTitleLable.text = "Telefone: " + RepairShopItem.Name;
			RSTellLable.text = Strings.FormatPhoneNumber(RepairShopItem.Phone);

			EnableRequestButtons(false);
		}

		public override void DeactivateView()
		{
			base.DeactivateView();
		}
		#endregion IView



		#region ViewButtonEvents
		public void OnRepairShopFavorite()
		{
			this.RepairShopItem.IsFavorite = !this.RepairShopItem.IsFavorite;
			RSFavoriteStar.SetActive(this.RepairShopItem.IsFavorite);
		}

		public void OnRepairShopSendEmail()
		{
			// TODO: enviar solicitação ao servidor para enviar mensagem de email para a oficina.
			StartCoroutine(SendRepairMessage());

		}

		private IEnumerator SendRepairMessage()
		{

			LoadingView.Instance.Enable();

			//
			// guardando as informacoes dos users.
			ApplicationController.Instance.CurrentUser.Name = UserName.value;
			ApplicationController.Instance.CurrentUser.Email = UserEmail.value;
			ApplicationController.Instance.CurrentUser.Phone = UserPhoneNumber.value;

			{
				// TODO: envio da mensagem ao servidor.
			}

			LoadingView.Instance.Disable();

			yield return new WaitForSeconds(0.1f);

			ConfirmProfileView.ActivateView();

			yield return null;
		}

		public void OnRepairShopCall()
		{
			if (string.IsNullOrEmpty(RepairShopItem.Phone))
			{
				if (!string.IsNullOrEmpty(RepairShopItem.CellPhone))
					Application.OpenURL("tel://"+RepairShopItem.CellPhone);
				else
					Application.OpenURL("tel://"+RepairShopItem.Phone);
				_isCalling = true;
			}
			else
			{
				// LaterFix: Notificar que a oficina nao tem telefone cadastrado. Isto nunca deve ocorrer
			}
		}
		#endregion ViewButtonEvents



		#region UserDataValidation
		public void ValidateAllInputFields()
		{
			bool b = _emailIsValid && _nameIsValid && _telIsValid;
			EnableRequestButtons(b);
		}

		public void ValidateEmail()
		{
			// TODO: verificar no server se ja existe este email cadastrado.
			string email = UserEmail.value;
			Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
			Match match = regex.Match(email);
			_emailIsValid = match.Success;
			ValidateAllInputFields();
		}

		public void ValidateName()
		{
			_nameIsValid = !string.IsNullOrEmpty(UserName.value);
			ValidateAllInputFields();
		}

		public void ValidadePhoneNumber()
		{
			//string numericPhone = new string (UserPhoneNumber.value.ToCharArray ().Where (c => char.IsDigit (c)).ToArray ());
			UserPhoneNumber.value = Strings.FormatPhoneNumber(UserPhoneNumber.value);
			UserPhoneNumber.UpdateLabel();
			_telIsValid = UserPhoneNumber.value.Length >= 9;
			ValidateAllInputFields();
		}

		public void OnUserPhoneNumberSubmit()
		{
			UserPhoneNumber.value = Strings.FormatPhoneNumber(UserPhoneNumber.value);
			UserPhoneNumber.UpdateLabel();
		}

		private void EnableRequestButtons(bool value)
		{
			if (!string.IsNullOrEmpty(RepairShopItem.Email))
				SendEmailRequestButton.SetActive(value);
			if ((!string.IsNullOrEmpty(RepairShopItem.Phone) || !string.IsNullOrEmpty(RepairShopItem.CellPhone)))
				CallForRepairShopButton.SetActive(value);
		}
		#endregion UserDataValidation



		#region MonoBehaviour
		void OnApplicationPause(bool pause)
		{
			if (pause)
			{
				// we are in background
			}
			else
			{
				// voltando com o foco na applicacao.
				if (_isCalling)
				{
					_isCalling = false;
					//TODO: Callback de ligacao de telefone.
				}
			}
		}
		#endregion MonoBehaviour

	}
}