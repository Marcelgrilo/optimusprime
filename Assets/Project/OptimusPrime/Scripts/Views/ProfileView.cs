﻿using UnityEngine;
using System.Collections;
using MotorTools.Views;
using MotorTools.Controllers;

namespace MotorTools.Views
{
	public class ProfileView : IView
	{


		public UILabel NameLabel;
		public UILabel EmailLabel;
		public UILabel TelephoneLabel;

		public UIInput NameInput;
		public UIInput EmailInput;
		public UIInput TelephoneInput;

		//public 


		public override void ActivateView()
		{
			//
			// Inicialização dos componentes de edição dos dados do usuario.
			NameLabel.text = ApplicationController.Instance.CurrentUser.Name;
			EmailLabel.text = ApplicationController.Instance.CurrentUser.Email;
			TelephoneLabel.text = ApplicationController.Instance.CurrentUser.Phone;

			NameInput.value = ApplicationController.Instance.CurrentUser.Name;
			EmailInput.value = ApplicationController.Instance.CurrentUser.Email;
			TelephoneInput.value = ApplicationController.Instance.CurrentUser.Phone;

			IsEditModeOn = false;




			base.ActivateView();
		}

		public override void DeactivateView()
		{
			base.DeactivateView();
		}




		private bool _isEditModeOn;
		public bool IsEditModeOn
		{
			get { return _isEditModeOn; }
			set { _isEditModeOn = value; EditMode(_isEditModeOn); }
		}
		public void ToggleEditMode()
		{
			IsEditModeOn = !IsEditModeOn;
		}
		private void EditMode(bool b)
		{
			NameLabel.gameObject.SetActive(!b);
			EmailLabel.gameObject.SetActive(!b);
			TelephoneLabel.gameObject.SetActive(!b);

			NameInput.gameObject.SetActive(b);
			EmailInput.gameObject.SetActive(b);
			TelephoneInput.gameObject.SetActive(b);
		}




	}
}
