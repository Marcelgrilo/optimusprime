﻿
namespace MotorTools.Views
{
	using UnityEngine;
	using System.Collections;
	using MotorTools.Controllers;
	using System.Text.RegularExpressions;
	using Data;


	public class CompleteProfileView : IView
	{

		#region UIElements
		public UIInput UserName;
		public UIInput UserEmail;
		public UIInput UserPhoneNumber;
		public UIInput UserPassword;
		public UIInput UserConfirmPassword;
		public UIToggle AgreementButton;
		public UIButton ConfirmProfileButton;
		public UILabel DefinePasswordLabel;
		public UILabel ConfirmPasswordLabel;

		private bool _emailIsValid = false;
		private bool _telIsValid = false;
		private bool _nameIsValid = false;
		private bool _passwordValid = false;
		private bool _agree = false;
		#endregion UIElements



		// Use this for initialization
		void Start()
		{
			UserName.value = ApplicationController.Instance.CurrentUser.Name==null ? "" : ApplicationController.Instance.CurrentUser.Name;
			UserEmail.value = ApplicationController.Instance.CurrentUser.Email==null ? "" : ApplicationController.Instance.CurrentUser.Email;
			UserPhoneNumber.value = ApplicationController.Instance.CurrentUser.Phone==null ? "" : ApplicationController.Instance.CurrentUser.Phone;
		}


		public void OnCompleteProfileButtonPressed()
		{
			//TODO: navegar para a proxima view depois de cadastrar os dados do user no banco de dados.
		}


		#region UserDataValidation
		public void ValidateAllInputFields()
		{
			bool b = _emailIsValid && _nameIsValid && _telIsValid && _passwordValid && _agree;
			EnableCompleteProfileButton(b);
		}

		public void ValidateEmail()
		{
			// TODO: verificar no server se ja existe este email cadastrado.
			string email = UserEmail.value;
			Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
			Match match = regex.Match(email);
			_emailIsValid = match.Success;
			ValidateAllInputFields();
		}

		public void ValidateName()
		{
			_nameIsValid = !string.IsNullOrEmpty(UserName.value);
			ValidateAllInputFields();
		}

		public void ValidadePhoneNumber()
		{
			//string numericPhone = new string (UserPhoneNumber.value.ToCharArray ().Where (c => char.IsDigit (c)).ToArray ());
			UserPhoneNumber.value = Strings.FormatPhoneNumber(UserPhoneNumber.value);
			UserPhoneNumber.UpdateLabel();
			_telIsValid = UserPhoneNumber.value.Length >= 9;
			ValidateAllInputFields();
		}

		public void ValidatePassword()
		{
			DefinePasswordLabel.gameObject.SetActive(string.IsNullOrEmpty(UserPassword.value));
			ConfirmPasswordLabel.gameObject.SetActive(string.IsNullOrEmpty(UserConfirmPassword.value));

			if (!string.IsNullOrEmpty(UserPassword.value))
			{
				if (UserPassword.value.Length>=5 && UserConfirmPassword.value.Equals(UserPassword.value))
				{
					_passwordValid = true;
				}
				else
				{
					_passwordValid = false;
				}
			}
			else
			{
				_passwordValid = false;
			}

			ValidateAllInputFields();
		}

		public void ValidateAgreeToggle(bool b)
		{
			_agree = b;
			ValidateAllInputFields();
		}

		public void OnUserPhoneNumberSubmit()
		{
			UserPhoneNumber.value = Strings.FormatPhoneNumber(UserPhoneNumber.value);
			UserPhoneNumber.UpdateLabel();
		}

		private void EnableCompleteProfileButton(bool value)
		{
			ConfirmProfileButton.gameObject.SetActive(value);
		}
		#endregion UserDataValidation


	}
}