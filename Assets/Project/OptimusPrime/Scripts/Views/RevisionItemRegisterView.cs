﻿namespace MotorTools.Views
{
	using UnityEngine;
	using System.Collections;
	using System.Collections.Generic;
	using MotorTools.Views.Utils;
	using MotorTools.Controllers;
	using MotorTools.Data;


	public class RevisionItemRegisterView : IView 
	{
		List<string> items = new List<string>();



		public UILabel RevisionTypeLabel;
		public UILabel RevisionNameLabel;
		public UILabel RevisionDescriptionLabel;
		public UILabel RevisionKmLabel;
		public UILabel RevisionMonthLabel;

		public UIButton OKButton;


		[Space(20)]
		public SelectView SelectionView;
		public GameObject SelectBlockInput;
		public SelectNumberView
				SelectionNumberView;
		public GameObject SelectNumberBlockInput;

		[Space(5)]
		public Transform SelectNumberTopLeft;
		public Transform SelectNumberBottomRight;
		[Space(5)]
		public Transform SelectTopLeft;
		public Transform SelectBottomRight;

		public void OnRevisionTypeClicked()
		{
			
			SelectionView.EnableView
			(
				new EventDelegate(RevisionTypeSelected),
				Localization.Get(Strings.RevisionItemType),
				items,
				SelectBlockInput,
				GetComponent<Camera> (), 
				SelectTopLeft, 
				SelectBottomRight
			);
		}

		public void OnKmButtonClicked()
		{
			SelectionNumberView.EnableView
				(
					Localization.Get(Strings.Quilometragem),
					new EventDelegate(RevisionKmSelected),
					GetComponent<Camera>(),
					SelectNumberTopLeft,
					SelectNumberBottomRight,
					SelectNumberBlockInput
				);
		}

		//public void OnmMonthButtonClicked()
		//{
		//	SelectionNumberView.EnableView
		//	(
		//		Localization.Get(Strings.Periodo),
		//		new EventDelegate(RevisionMonthSelected),
		//		GetComponent<Camera>(),
		//		SelectNumberTopLeft,
		//		SelectNumberBottomRight,
		//		SelectNumberBlockInput
		//	);
		//}

		public override void Back()
		{
			StopAllCoroutines();
			base.Back();
		}


		public void OnOkButton()
		{
			StartCoroutine(RegisterRevisionItem());
		}

		public IEnumerator RegisterRevisionItem()
		{
			LoadingView.Instance.Enable();

			ApplicationController.UserRegisterItem item = new ApplicationController.UserRegisterItem();

			item.MaintenaceType = RevisionTypeLabel.text;
			item.Name = RevisionNameLabel.text;
			item.Description = RevisionDescriptionLabel.text;
			item.Km = RevisionKmLabel.text;
			item.Month = RevisionMonthLabel.text;

			yield return StartCoroutine(ApplicationController.Instance.AddUserRegisterItem(item));

			yield return null;

			LoadingView.Instance.Disable();

			Back();

		}

		private void VerifyOk()
		{
			if (
					string.IsNullOrEmpty(RevisionTypeLabel.text)||
					string.IsNullOrEmpty(RevisionNameLabel.text) ||
					RevisionNameLabel.text.Equals("Nome do item a ser revisado.") ||
					string.IsNullOrEmpty(RevisionDescriptionLabel.text) ||
					RevisionDescriptionLabel.text.Equals("Descrição do item a ser revisado.") ||
					(
						string.IsNullOrEmpty(RevisionKmLabel.text) &&
						(string.IsNullOrEmpty(RevisionMonthLabel.text) || RevisionMonthLabel.text == "0")
					)
				)
			{
				OKButton.gameObject.SetActive(false);
				return;
			}

			OKButton.gameObject.SetActive(true);
		}

		public void RevisionTypeSelected()
		{
			RevisionTypeLabel.text = SelectionView.Selected;
			Debug.Log(SelectionView.Selected);
			VerifyOk();
		}

		public void RevisionNameEdited()
		{
			VerifyOk();
		}

		public void RevisionDescriptionEdited()
		{
			VerifyOk();
		}

		public void RevisionKmSelected()
		{
			RevisionKmLabel.text = int.Parse(SelectionNumberView.FinalValue) == 0 ? "" : int.Parse(SelectionNumberView.FinalValue).ToString();

			VerifyOk();
		}

		public void RevisionMonthSelected()
		{
			VerifyOk();
		}

		void OnEnable()
		{
			RevisionTypeLabel.text = "";
			RevisionNameLabel.text = "";
			RevisionDescriptionLabel.text = "";
			RevisionKmLabel.text = "";
			RevisionMonthLabel.text = "";

			OKButton.gameObject.SetActive(false);
		}



		// Use this for initialization
		void Start () 
		{
			foreach (string s in Strings.RevisionIcons)
			{
				items.Add(Localization.Get(s).ToUpper());
			}
		}

	}
}
