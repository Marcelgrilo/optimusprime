﻿namespace MotorTools.Views.Utils
{


	using UnityEngine;
	using System.Collections;
	using System.Collections.Generic;


	public class SelectView : MonoBehaviour
	{
	
		public GameObject SView;
	
		public GameObject Container;
	
		public PopulateSelectView PopulateObj;
	
		public GameObject BlockInput;
				[Space(20)]
		public UIViewport ViewPort;
			
		public UIButton ButtonSelect{ get; set; }
		public UIButton ButtonCancel{ get; set; }
		public UILabel LabelTitle{ get; set; }
	
		public string Selected{ get ; private set; }

	
		Animator openCloseAnimator;
		Animator OpenCloseAnimator
		{
				get
				{
						if (openCloseAnimator == null)
						{
								openCloseAnimator = SView.GetComponent<Animator> ();
						}
						return openCloseAnimator;
				}
		}

		GameObject toDeactivate;
		EventDelegate onOkButton;

		public void EnableView(EventDelegate paramOnOkButton, string title, List<string> items, GameObject paramBlockInput, Camera sourceCamera, Transform topLeft, Transform bottomRight)
		{
			EnableView(null, paramOnOkButton, title, items, paramBlockInput, sourceCamera, topLeft, bottomRight);
		}

		public void EnableView(GameObject paramToDeactivate, EventDelegate paramOnOkButton, string title, List<string> items, GameObject paramBlockInput, Camera sourceCamera, Transform topLeft, Transform bottomRight)
		{
				ViewPort.sourceCamera = sourceCamera;
				ViewPort.bottomRight = bottomRight;
				ViewPort.topLeft = topLeft;

				onOkButton = paramOnOkButton;
				ButtonSelect = Container.transform.FindChild ("ButtonSelect").GetComponent<UIButton> ();
				ButtonCancel = Container.transform.FindChild ("ButtonCancel").GetComponent<UIButton> ();
				LabelTitle = Container.transform.FindChild ("LabelHeadTitle").GetComponent<UILabel> ();
	
				LabelTitle.text = title;
	
				BlockInput = paramBlockInput;
				BlockInput.SetActive (true);

				EventDelegate.Add (ButtonSelect.onClick, OnOk);
				EventDelegate.Add (ButtonCancel.onClick, OnCancel);
	
				toDeactivate = paramToDeactivate;
				if (toDeactivate != null)
				{
						toDeactivate.SetActive (false);
				}
				//Container.SetActive (true);
				SView.SetActive (true);

				PopulateObj.Populate (items);
				OpenCloseAnimator.SetBool ("isOpened", true);
		}

	
		public void OnOk ()
		{
				Selected = SView.GetComponentInChildren<SelectSpring> ().SelectedItem;
				StartCoroutine (OkButtonCommand ());
				StartCoroutine (Dispose ());
		}
	

		public void OnCancel ()
		{
				StartCoroutine (Dispose ());
		}

		IEnumerator OkButtonCommand ()
		{
				yield return new WaitForSeconds (.6f);
				onOkButton.Execute ();
		}

		IEnumerator Dispose ()
		{
				OpenCloseAnimator.SetBool ("isOpened", false);

				yield return new WaitForSeconds (.7f);
		
				EventDelegate.Remove (ButtonSelect.onClick, OnOk);
				EventDelegate.Remove (ButtonCancel.onClick, OnCancel);
			
				ButtonSelect = null;
				ButtonCancel = null;
				LabelTitle = null;
				if (toDeactivate != null)
				{
						toDeactivate.SetActive (true);
				}

				SView.SetActive (false);
			
				BlockInput.SetActive (false);
		}
	
	
	}
}
