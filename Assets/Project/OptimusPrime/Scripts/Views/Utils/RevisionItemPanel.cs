﻿namespace MotorTools.Utils
{
	using MotorTools.Data;
	using MotorTools.Models;
	using MotorTools.Views;
	using UnityEngine;

	/// <summary>
	///  The RevisionItemPanel is a MonoBehaviour that
	///  represents the panel that will be shown as a revision
	///  item on the application view.
	/// </summary>
	public class RevisionItemPanel : MonoBehaviour
	{
		#region Attributes

		
		private IView _searchServiceView;

		/// <summary>
		/// The button for search the services.
		/// </summary>
		private GameObject myButtonSearchService;

		/// <summary>
		/// This elements parent Table used to reposition this
		/// element in relation to the others.
		/// </summary>
		private UITable myParentTabble;

		/// <summary>
		/// This element widget, used to expand the panel.
		/// The elements of the panel is anchored to it and
		/// will be resized automaticaly in relation to it.
		/// </summary>
		private UIWidget myWidget;

		/// <summary>
		/// This sprite is gray or blue, dependin on its activation.
		/// </summary>
		private UISprite myColoredSprite;

		/// <summary>
		/// This sprite represents the revision item type.
		/// </summary>
		private UISprite myMainSprite;

		/// <summary>
		/// This sprite is the sprite that is created between the lines
		/// of the panel.
		/// It has an alfa when the panel is expanded.
		/// </summary>
		private UISprite myExpandedBackgroundSprite;

		/// <summary>
		/// The ui labet to show the text description of the revision item.
		/// </summary>
		private UILabel myLabelDescription;

		/// <summary>
		/// the label of the button used to expand and minimize the panel.
		/// </summary>
		private UILabel myLabelExpandButton;

		/// <summary>
		/// The camera used to drag the panel down on the minimize function.
		/// </summary>
		//private UIDragCamera myDragCamera;

		/// <summary>
		/// The state of the panel.
		/// </summary>
		private bool myIsExpanded;

		/// <summary>
		/// The original height of the panel when it is minimized.
		/// </summary>
		private int myOriginalHeight;

		/// <summary>
		/// The revision item that this panel represents.
		/// </summary>
		private RevisionItem myRevisionItem;

		#endregion Attributes
		#region Methods



		void OnEnable()
		{
		//	myDragCamera.draggableCamera.Drag(-Vector2.up);

			//myWidget.SetDirty();

		//	myParentTabble.repositionNow = true;

		//	myDragCamera.draggableCamera.Drag(-Vector2.up);
		}


		/// <summary>
		/// The method that is called to expand or minimize the panel
		/// </summary>
		public void OnUIButtonClick()
		{
			if (myIsExpanded)
			{
				ShowLess();
			}
			else
			{
				ShowMore();
			}
		}



		/// <summary>
		/// The method that is called to expand or minimize the panel
		/// </summary>
		public void OnSearchServiceButtonClick()
		{
		//	Debug.Log("call of OnSearchServiceButtonClick:" + _searchServiceView.name);
			_searchServiceView.ActivateView();
		}



		/// <summary>
		/// Presets the initial configuration of the elements opf the panel.
		/// </summary>
		/// <param name="revItem">the revision item</param>
		/// <param name="sprite">the sprite to use</param>
		public void SetItem(RevisionItem revItem, string sprite, IView view)
		{
			myRevisionItem = revItem;

			name = myRevisionItem.Id + myRevisionItem.VehicleId;

			if (myLabelDescription == null)
			{
				myLabelDescription = transform.Find("LabelDescription").GetComponent<UILabel>();
			}
			myLabelDescription.text = myRevisionItem.PartName;
			myLabelDescription.maxLineCount = 2;
			myLabelDescription.overflowMethod = UILabel.Overflow.ClampContent;

			if (myMainSprite == null)
			{
				myMainSprite = transform.Find("Sprites/MainSpriteItem").GetComponent<UISprite>();
			}
			myMainSprite.spriteName = sprite;
			myMainSprite.SetDirty();

			_searchServiceView = view;
			myButtonSearchService = transform.Find("SearchServiceButton").gameObject;
			EventDelegate.Add(myButtonSearchService.GetComponent<UIButton>().onClick, OnSearchServiceButtonClick, false);
		}

		/// <summary>
		/// Compares if the RevisionItem on this instance is equals to
		/// the other RevisionItem passed as parameter of this function.
		/// </summary>
		/// <param name="otherRevisionItem">the otherRevisionItem</param>
		/// <returns>TRUE if the otherRevisionItem is equals to this instance myRevisionItemm, FALSE otherwise.</returns>
		public bool CompareRevisionItem(RevisionItem otherRevisionItem)
		{
			return myRevisionItem.Equals(otherRevisionItem);
		}

		/// <summary>
		/// Return the panel to its original size.
		/// </summary>
		private void ShowLess()
		{
			myIsExpanded = false;

			myWidget.height = myOriginalHeight;

			myExpandedBackgroundSprite.alpha = 0.0f;

			myButtonSearchService.SetActive(false);

			myLabelExpandButton.text = Localization.Get(Strings.SeeMore);

			myLabelDescription.maxLineCount = 2;
			myLabelDescription.overflowMethod = UILabel.Overflow.ClampContent;

			myColoredSprite.color = new Color(.5f, .5f, .5f);

		//	myWidget.SetDirty();

			myParentTabble.Reposition();
			myParentTabble.repositionNow = true;

         //   myDragCamera.draggableCamera.Drag(-Vector2.up);
		}

		/// <summary>
		/// Expand the panel and show the text up to the end of the text.
		/// The expansion size is calculated to automaticaly ajust to the text size.
		/// </summary>
		private void ShowMore()
		{
			myIsExpanded = true;

			myWidget.height = (int)((myWidget.height + (float)myLabelDescription.CalculateOffsetToFit(myLabelDescription.text) * (1.0f + (float)myLabelDescription.fontSize / 100.0f)));

			myExpandedBackgroundSprite.alpha = 0.4f;

			myButtonSearchService.SetActive(true);

			myLabelExpandButton.text = Localization.Get(Strings.Minimize);

			myLabelDescription.maxLineCount = 0;

			myColoredSprite.color = new Color(.212f, .251f, .310f);

			//	myWidget.SetDirty();

			myParentTabble.Reposition();
			myParentTabble.repositionNow = true;

			//Debug.Log("button active: "+myButtonSearchService.GetComponent<UIButton>().isActiveAndEnabled);
			string str = "";
			foreach (var v in myButtonSearchService.GetComponent<UIButton>().onClick)
				str += "\n\t"+v.methodName + ":" + v.target;
			//Debug.Log("onclick: " + myButtonSearchService.GetComponent<UIButton>().onClick + "\n{" + str+"\n}");
			
			
		}

		#endregion Methods
		#region Monobehaviour Methods

		/// <summary>
		/// Loads all the elements that this class needs.
		/// </summary>
		private void Start()
		{
			myWidget = GetComponent<UIWidget>();

			//myDragCamera = GetComponent<UIDragCamera>();

			myParentTabble = transform.parent.GetComponent<UITable>();

			myButtonSearchService = transform.Find("SearchServiceButton").gameObject;
			myButtonSearchService.SetActive(false);

			myColoredSprite = transform.Find("Sprites/ColoredSpriteItem").GetComponent<UISprite>();

			myMainSprite = transform.Find("Sprites/MainSpriteItem").GetComponent<UISprite>();

			myLabelDescription = transform.Find("LabelDescription").GetComponent<UILabel>();

			myLabelExpandButton = transform.Find("ExpandButton").GetComponent<UILabel>();

			myExpandedBackgroundSprite = transform.GetChild(0).GetComponent<UISprite>();

			myIsExpanded = false;

			myOriginalHeight = myWidget.height;


			name = "";
		}



		#endregion Monobehaviour Methods

	}
}