﻿namespace MotorTools.Views.Utils
{


		using UnityEngine;
		using System.Collections;
		using System.Collections.Generic;


		public class PopulateSelectView : MonoBehaviour
		{
	
				public GameObject VoidStartItem;
				public GameObject VoidEndItem;
				public GameObject SelectionItem;
			
				public UIDraggableCamera DraggableCamera;
	

				UITable table;
				UITable Table
				{
						get
						{
								if (table == null)
								{
										table = GetComponent<UITable> ();
								}
								return table;
						}
				}


				public void Populate (List<string> items)
				{
						StartCoroutine (PopulateView (items));
				}
	

				IEnumerator PopulateView (List<string>items)
				{
						foreach (Transform t in transform)
						{
								Destroy (t.gameObject);
						}
						//yield return 0;
						//yield return 0;
						//	Table.Reposition ();
						//	yield return 0;
	
						CreateItem (VoidStartItem);
						//Table.Reposition ();
						//yield return 0;
						foreach (string s in items)
						{
								CreateItem (s);
								//Table.Reposition ();
								//yield return 0;
						}
						CreateItem (VoidEndItem);
						//yield return 0;
						Table.Reposition ();
						yield return 0;
						DraggableCamera.GetComponent<SelectSpring> ().enabled = true;
				}
	

				GameObject CreateItem (string s)
				{
						GameObject go = CreateItem (SelectionItem);
						if (!string.IsNullOrEmpty (s))
						{
								go.transform.GetChild (0).GetComponent<UILabel> ().text = s;
						}
						return go;
				}


				GameObject CreateItem (Object voidItem)
				{
						GameObject go = (GameObject)Object.Instantiate (voidItem);
						go.transform.SetParent (transform);
			
						go.transform.localScale = Vector3.one;
						go.GetComponent<UIDragCamera> ().draggableCamera = DraggableCamera;
						return go;
				}
		}

}
