﻿using System.Collections;

namespace MotorTools.Views.Utils
{


		using UnityEngine;


		public class SelectSpring : MonoBehaviour
		{
	

				Vector3 springPositioner;
			
				UIDraggableCamera dcamera;

				RaycastHit rayhit;


				public string SelectedItem
				{
						get;
						set;
				}
	

				void OnEnable ()
				{
						dcamera = GetComponent<UIDraggableCamera> ();
						dcamera.transform.localPosition = new Vector3 (dcamera.transform.localPosition.x, -270, dcamera.transform.localPosition.z);
						springPositioner = new Vector3 (transform.localPosition.x, 0, transform.localPosition.z);
				}
	

				void FixedUpdate ()
				{
						
						if (
								Physics.Raycast 
								(
										(transform.position - transform.TransformDirection (Vector3.forward)) - Vector3.up * .1f,
										transform.TransformDirection (Vector3.forward) * 2,
										out rayhit)
							)
						{

								SpringPosition.Begin 
								(
									gameObject, 
									new Vector3 
									(
									springPositioner.x, 
									rayhit.collider.gameObject.transform.localPosition.y + 60, 
									springPositioner.z), 
									100
								);

								UILabel l = rayhit.collider.gameObject.GetComponentInChildren<UILabel> ();

								if (l != null)
								{
										SelectedItem = rayhit.collider.gameObject.GetComponentInChildren<UILabel> ().text;
								}

								//Debug.Log (rayhit.collider.gameObject.name);
	
						}
				}
		}
}
