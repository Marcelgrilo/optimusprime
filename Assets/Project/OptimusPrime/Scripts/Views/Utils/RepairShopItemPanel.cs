﻿namespace MotorTools.Views
{
	using UnityEngine;
	using MotorTools.Models;
	using Marcel.DP.Message;
	using Controllers;

	public class RepairShopItemPanel : MonoBehaviour
	{
		public RepairShop Model
		{ get; set; }
		public UILabel Title;
		public UILabel Distance;
		public UILabel Descrition;
		[Space(10)]
		public UISprite
			MainSprite;
		[Space(10)]
		public UIButton
			RequestBudget;
		[Space(10)]
		public UIToggle
			StarBox;
		public UIToggle CheckBox;


		public bool IsStar
		{ get; set; }
		public bool IsCheck
		{ get { return CheckBox.value; } }


		private IView _contactRepairShopview;



		void Start()
		{
			//IsCheck = CheckBox.value;
			IsStar = StarBox.value;
		}



		public void Set(RepairShop item, IView view)
		{
			_contactRepairShopview = view;
			Model = item;
			Title.text = item.Name;
			Descrition.text = item.Description;
			Distance.text = "10km";//workshop.GPSPosition;
			try
			{
				double lat1 = double.Parse(item.Latitude);
				double lng1 = double.Parse(item.Longitude);
				double lat2 = double.Parse(ApplicationController.Instance.CurrentUser.CurrentLocation.latitude.ToString());
				double lng2 = double.Parse(ApplicationController.Instance.CurrentUser.CurrentLocation.longitude.ToString());
				double value = MotorTools.Utils.Utils.GetDistanceKM
					(
					lat1, lng1, lat2, lng2
					);
				Distance.text = string.Format("à {0:#.##} Km", value);
			}
			catch (System.Exception)
			{
				Distance.text = "";
			}
			MainSprite.spriteName = "24Barra7";// item.WorkingPeriod;
			MainSprite.SetDirty();
			StarBox.value = item.IsFavorite;
		}



		public void OnStarCheck()
		{
			IsStar = !IsStar;
			Model.IsFavorite = IsStar;
			Messenger.Broadcast<bool, int>(this.GetHashCode().ToString() + "StarListenner", IsStar, transform.GetSiblingIndex());
		}



		public void OnRequestPrice()
		{
			//Debug.Log("OnRequestPrice");
			ApplicationController.Instance.SelectedRepairShop = this.Model;
			_contactRepairShopview.ActivateView();
		}




		public void AddStarListenner(Callback<bool, int> callback)
		{
			Messenger.AddListener<bool, int>(this.GetHashCode().ToString() + "StarListenner", callback);
		}
		public void RemoveStarListenner(Callback<bool, int> callback)
		{
			Messenger.RemoveListener<bool, int>(this.GetHashCode().ToString() + "StarListenner", callback);
		}
	}
}