﻿namespace MotorTools.Views.Utils
{
		using UnityEngine;
	
		public class CircularList : MonoBehaviour
		{
	
				UIGrid items;
				Transform resetItem;

				Vector3 myOriginalPosition;
	
				UICenterOnChild centerComponent;

				public GameObject Selected
				{
						get;
						set;
				}

	
				void Start ()
				{
						items = GetComponent<UIGrid> ();
						resetItem = items.transform.FindChild ("Item 0");
						myOriginalPosition = items.transform.position;
						centerComponent = GetComponent<UICenterOnChild> ();
						centerComponent.onFinished += CenterFinished;
				}
	

				public void SendBot ()
				{
						Transform t = items.GetChild (9);
						t.SetAsFirstSibling ();
						items.Reposition ();
						items.transform.position = myOriginalPosition;
				}
	
	
				public void  SendTop ()
				{
						Transform t = items.GetChild (0);
						t.SetAsLastSibling ();
						items.Reposition ();
						items.transform.position = myOriginalPosition;
				}
	
	
				void CenterFinished ()
				{
						items.transform.position = myOriginalPosition;
						Selected = centerComponent.centeredObject;
						//Debug.Log ("finished  " + centerComponent.centeredObject);
				}
	
				
				public void ResetList ()
				{
						if (Selected != null && Selected.name != resetItem.name)
						{
								int loops = int.Parse (Selected.name.Split (' ') [1]);
								if (loops != 0)
								{		
										for (int j = 0; j < loops; j++)
										{
												SendBot ();
										}
								}
								centerComponent.CenterOn (resetItem);
						}
				}

		}
}
