﻿using System.Collections;

namespace MotorTools.Views.Utils
{
		using UnityEngine;
	
		public class SelectNumberView : MonoBehaviour
		{
	
				public CircularList [] CircularLists;

				public string FinalValue{ get; private set; }

				Animator openCloseAnimator;
				Animator OpenCloseAnimator
				{
						get
						{
								if (openCloseAnimator == null)
								{
										openCloseAnimator = GetComponent<Animator> ();
								}
								return openCloseAnimator;
						}
				}

				public UILabel TitleLabel;
				public UIButton CancelButton;
				public UIButton SelectButton;

				public GameObject BlockInput{ get; set; }
				public UIViewport ViewPort;

				EventDelegate onOkButton;


				public void EnableView (string title, EventDelegate onOkButtonDelegate, Camera sourceCamera, Transform topLeft, Transform bottomRight, GameObject paramBlockInput)
				{

						ViewPort.sourceCamera = sourceCamera;
						ViewPort.bottomRight = bottomRight;
						ViewPort.topLeft = topLeft;

						onOkButton = onOkButtonDelegate;
			
						TitleLabel.text = title;

						BlockInput = paramBlockInput;
						BlockInput.SetActive (true);

						EventDelegate.Add (SelectButton.onClick, OnOk);
						EventDelegate.Add (CancelButton.onClick, OnCancel);

						gameObject.SetActive (true);

						RepositionSelectItems ();

						OpenCloseAnimator.SetBool ("isOpened", true);

				}

				public void OnOk ()
				{
						string s = "";
						for (int i = 0; i < CircularLists.Length; i++)
						{
								if (CircularLists [i].Selected != null)
								{
										s += CircularLists [i].Selected.name.Split (' ') [1];
								}
						}
						FinalValue = s;
						StartCoroutine (OkButtonCommand ());
						StartCoroutine (Dispose ());
				}

				public void OnCancel ()
				{
						StartCoroutine (Dispose ());
				}


				IEnumerator OkButtonCommand ()
				{
						yield return new WaitForSeconds (0.6f);
						onOkButton.Execute ();
				}

				IEnumerator Dispose ()
				{
						OpenCloseAnimator.SetBool ("isOpened", false);

						yield return new WaitForSeconds (0.7f);

						EventDelegate.Remove (SelectButton.onClick, OnOk);
						EventDelegate.Remove (CancelButton.onClick, OnCancel);
			
						gameObject.SetActive (false);
			
						BlockInput.SetActive (false);
				}


				void RepositionSelectItems ()
				{
						for (int i = 0; i < CircularLists.Length; i++)
						{
								CircularLists [i].ResetList ();
						}
				}
		}
}
