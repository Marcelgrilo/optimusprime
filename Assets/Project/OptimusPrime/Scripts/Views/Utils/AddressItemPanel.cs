﻿namespace MotorTools.Views
{
	using UnityEngine;
	using System.Collections;
	using Models;
	using System;
	using Marcel.DP.Message;

	public class AddressItemPanel : MonoBehaviour
	{
		public Address Model { get; set; }

		public GameObject SelectButton;
		public GameObject TrashButton;
		public UILabel Name;
		public UISprite Line;

		private static readonly Color SelectedColor = new Color32(212,76,0,255);
		private static readonly Color UnselectedColor = new Color32(96, 96, 96,255);

		public void Set(Address a, Callback<bool,int> callbackSelect, Callback<int>  callbackTrash)
		{
			Model = a;
			Name.text = Model.UserInputAddress;
			EventDelegate.Add(SelectButton.GetComponent<UIButton>().onClick,OnSelectAddress);
			EventDelegate.Add(TrashButton.GetComponent<UIButton>().onClick, OnTrash);
			AddSelectionListenner(callbackSelect);
			AddTrashListenner(callbackTrash);
		}


		public void AddSelectionListenner(Callback<bool, int> callback)
		{
			Messenger.AddListener<bool, int>(this.GetHashCode().ToString() + "SelectAddress", callback);
		}
		public void RemoveSelectionListenner(Callback<bool, int> callback)
		{
			Messenger.RemoveListener<bool, int>(this.GetHashCode().ToString() + "SelectAddress", callback);
		}



		public void AddTrashListenner(Callback<int> callback)
		{
			Messenger.AddListener<int>(this.GetHashCode().ToString() + "Trash", callback);
		}
		public void RemoveTrashListenner(Callback< int> callback)
		{
			Messenger.RemoveListener<int>(this.GetHashCode().ToString() + "Trash", callback);
		}


		private bool isSelected = false;
		public bool IsSelected { get { return isSelected; } }
		public void SetSelected(bool enabled)
		{
			isSelected = enabled;
			if (isSelected)
			{
				// habilita
				//Debug.Log("Habilita " + transform.GetSiblingIndex());
				SelectButton.transform.GetChild(0).GetComponent<UISprite>().color = SelectedColor;
				//SelectButton.GetComponent<UIButton>().hover = SelectedColor;
				//SelectButton.GetComponent<UIButton>().pressed = SelectedColor;
				//SelectButton.GetComponent<UIButton>().disabledColor = SelectedColor;

				TrashButton.transform.GetChild(0).GetComponent<UISprite>().color = SelectedColor;
				//TrashButton.GetComponent<UIButton>().hover = SelectedColor;
				//TrashButton.GetComponent<UIButton>().pressed = SelectedColor;
				//TrashButton.GetComponent<UIButton>().disabledColor = SelectedColor;

				Name.color = SelectedColor;
				Line.color = SelectedColor;
			}
			else
			{
				//desabilita
				//Debug.Log("Desabilita " + transform.GetSiblingIndex());
				SelectButton.transform.GetChild(0).GetComponent<UISprite>().color = UnselectedColor;
				//SelectButton.GetComponent<UIButton>().hover = UnselectedColor;
				//SelectButton.GetComponent<UIButton>().pressed = UnselectedColor;
				//SelectButton.GetComponent<UIButton>().disabledColor = UnselectedColor;

				TrashButton.transform.GetChild(0).GetComponent<UISprite>().color = UnselectedColor;
				//TrashButton.GetComponent<UIButton>().hover = UnselectedColor;
				//TrashButton.GetComponent<UIButton>().pressed = UnselectedColor;
				//TrashButton.GetComponent<UIButton>().disabledColor = UnselectedColor;

				Name.color = UnselectedColor;
				Line.color = UnselectedColor;
			}
        }


		public void OnSelectAddress()
		{
			Messenger.Broadcast<bool, int>(this.GetHashCode().ToString()+ "SelectAddress", !isSelected, transform.GetSiblingIndex());
		}

		public void OnTrash()
		{
			Messenger.Broadcast<int>(this.GetHashCode().ToString() + "Trash",  transform.GetSiblingIndex());
		}

		public override int GetHashCode()
		{
			return base.GetHashCode() ^ Model.GetHashCode();
		}
	}
}