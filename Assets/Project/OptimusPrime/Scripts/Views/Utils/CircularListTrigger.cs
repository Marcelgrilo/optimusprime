﻿namespace MotorTools.Views.Utils
{
	using UnityEngine;
	
	public class CircularListTrigger : MonoBehaviour
	{
		//public bool IsTop;
		void OnTriggerEnter (Collider other)
		{
			if (other.GetComponent<UIDragScrollView> () != null)
			{
				if (name == "Top")
				{
					other.transform.parent.GetComponent<CircularList> ().SendTop ();
				}
				else
				{
					other.transform.parent.GetComponent<CircularList> ().SendBot ();
				}
			}
		}
	}
}
