﻿namespace MotorTools.Views
{
		using UnityEngine;

		public class LoadingView : Singleton<LoadingView>
		{

				EngineAnimator animator;
		
				void Start ()
				{
						animator = GetComponentInChildren<EngineAnimator> ();
				}


				public void Enable ()
				{
						gameObject.GetComponent<Camera> ().enabled = true;
						animator.EnableEngine (true);
				}


				public void Disable ()
				{
						gameObject.GetComponent<Camera> ().enabled = false;
						animator.EnableEngine (false);
				}

		}
}