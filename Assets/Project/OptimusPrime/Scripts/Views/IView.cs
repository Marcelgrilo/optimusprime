﻿namespace MotorTools.Views
{


	using UnityEngine;


	public class IView : MonoBehaviour
	{

		/// <summary>
		/// O sistema de view anterior eh controlado por uma classe singleton.
		/// </summary>
		public IView PreviousView
		{ get { return PreviousIViewManager.Instance.PreviousView; } }



		/// <summary>
		/// Resorna se o objeto esta ativo ou nao na hierarquia de objetos.
		/// </summary>
		public bool IsEnabled
		{ get { return gameObject.activeInHierarchy; } }



		/// <summary>
		/// Ativa esta view configurando a view corrente como esta e desativando qualquer outra view ativa.
		/// </summary>
		public virtual void ActivateView()
		{

			PreviousIViewManager.Instance.CurrentView = this;

			gameObject.SetActive(true);
			Show(true);

			foreach (IView auxObject in FindObjectsOfType<IView>())
			{
				if (auxObject.gameObject.GetInstanceID() != this.gameObject.GetInstanceID())
				{
					((IView) auxObject).DeactivateView();
				}
			}

			LoadingView.Instance.Disable();
		}



		/// <summary>
		/// Desativa esta view e marca esta view como a view anterior. 
		/// </summary>
		public virtual void DeactivateView()
		{
			if (IsEnabled)
				PreviousIViewManager.Instance.PreviousView = this;
			LoadingView.Instance.Enable();
			gameObject.SetActive(false);
		}



		/// <summary>
		/// Volta para a ultima view ativa.
		/// </summary>
		public virtual void Back()
		{
			if (PreviousIViewManager.Instance.PreviousView.GetInstanceID()==PreviousIViewManager.Instance.CurrentView.GetInstanceID())
				return;
			if (PreviousIViewManager.Instance.PreviousView != null)
				PreviousIViewManager.Instance.PreviousView.ActivateView();
		}


		public virtual void Show(bool flag)
		{
			gameObject.SetActive(flag);
		}



		public static void Test()
		{
			GameObject go01 = new GameObject();
			go01.name = "IViewTest01";
			GameObject go02 = new GameObject();
			go02.name = "IViewTest02";

			IView v01 = go01.AddComponent<IViewTest01>();
			IView v02 = go02.AddComponent<IViewTest02>();

			v01.ActivateView();
			Debug.Log(PreviousIViewManager.Instance.ToString());
			v01.DeactivateView();
			Debug.Log(PreviousIViewManager.Instance.ToString());
			v01.Back();
			Debug.Log(PreviousIViewManager.Instance.ToString());
			Debug.Log(" ");
			v02.ActivateView();
			Debug.Log(PreviousIViewManager.Instance.ToString());
			v02.DeactivateView();
			Debug.Log(PreviousIViewManager.Instance.ToString());
			v02.Back();
			Debug.Log(PreviousIViewManager.Instance.ToString());
			Debug.Log(" ");

			v01.ActivateView();
			Debug.Log(PreviousIViewManager.Instance.ToString());
			v02.ActivateView();
			Debug.Log(PreviousIViewManager.Instance.ToString());
			v02.Back();
			Debug.Log(PreviousIViewManager.Instance.ToString());
			v01.Back();
			Debug.Log(PreviousIViewManager.Instance.ToString());
			PreviousIViewManager.Instance.PreviousView.ActivateView();
			Debug.Log(PreviousIViewManager.Instance.ToString());

		}
	}



	public class IViewTest01 : IView
	{
		public override void ActivateView()
		{
			base.ActivateView();
			Debug.Log("IViewTest01:ActivateView");
		}
		public override void DeactivateView()
		{
			base.DeactivateView();
			Debug.Log("IViewTest01:DeactivateView");
		}
		public override void Back()
		{
			base.Back();
			Debug.Log("IViewTest01:Back");
		}
		public override string ToString()
		{
			return "IViewTest01";
		}
	}

	public class IViewTest02 : IView
	{
		public override void ActivateView()
		{
			base.ActivateView();
			Debug.Log("IViewTest02:ActivateView");
		}
		public override void DeactivateView()
		{
			base.DeactivateView();
			Debug.Log("IViewTest02:DeactivateView");
		}
		public override void Back()
		{
			base.Back();
			Debug.Log("IViewTest02:Back");
		}
		public override string ToString()
		{
			return "IViewTest02";
		}
	}




	/// <summary>
	/// Classe responsavel por gerenciar as views anterior e corrente.
	/// </summary>
	public class PreviousIViewManager
	{
		/// <summary>
		/// Instancia singleton desta classe.
		/// </summary>
		public static PreviousIViewManager _instance;
		public static PreviousIViewManager Instance
		{
			get
			{
				if (_instance == null)
					_instance = new PreviousIViewManager();
				return _instance;
			}
		}

		/// <summary>
		/// View ativa anteriormente a ciew corrente
		/// </summary>
		public IView PreviousView
		{ get; set; }
		/// <summary>
		/// View atualmente ativa.
		/// </summary>
		public IView CurrentView
		{ get; set; }

		public override string ToString()
		{
			return "\t\tprevious:" + (PreviousView==null ? "null" : PreviousView.ToString()) +" current:"+ (CurrentView == null ? "null" : CurrentView.ToString());
		}
	}


}
