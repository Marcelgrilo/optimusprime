﻿namespace MotorTools.Views
{
	using UnityEngine;
	using System.Collections;
	using MotorTools.Views.Utils;
	using MotorTools.Controllers;
	using System;

	public class MapView : IView
	{
		[SerializeField]
		public LoadMap LoadMapInstance;

		public IView ContactRepairShopView;



		public override void ActivateView()
		{
			base.ActivateView();
			LoadMapInstance.gameObject.SetActive(true);
			LoadMapInstance.MapView = this;
		}



		public override void DeactivateView()
		{
			LoadMapInstance.gameObject.SetActive(false);
			StartCoroutine(LoadMapInstance.DisableWebView());
			base.DeactivateView();
		}



		public void RepairShopSelectedOnMap(string repairShopId, LocationInfo currentLocation)
		{

			StartCoroutine(SelectRepairShopAndGoToContactRS(repairShopId, currentLocation));

		}

		private IEnumerator SelectRepairShopAndGoToContactRS(string repairShopId, LocationInfo currentLocation)
		{
			LoadMapInstance.gameObject.SetActive(false);
			//
			// busca pelas informacoes da repairshop.
			yield return StartCoroutine(ApplicationController.Instance.CoSelectRepairShop(repairShopId));

			LoadMapInstance.gameObject.SetActive(true);

			ApplicationController.Instance.CurrentUser.CurrentLocation = currentLocation;

			yield return new WaitForSeconds(.1f);

			ContactRepairShopView.ActivateView();

		}
	}
}