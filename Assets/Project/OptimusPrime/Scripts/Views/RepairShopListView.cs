﻿namespace MotorTools.Views
{
	using UnityEngine;
	using System.Collections;
	using System.Collections.Generic;
	using Marcel.JSON;
	using Data;
	using Models;
	using Controllers;

	public class RepairShopListView : IView
	{
		/// <summary>
		/// Lista dos modelos carregados.
		/// </summary>
		private List<RepairShop> _repairShopModelList;
		/// <summary>
		/// A lista de oficinas instanciadas.
		/// </summary>
		private List<RepairShopItemPanel> _repairShopPanels;
		/// <summary>
		/// Instancia do botao presente no LastItemPrefab, que é o ultimo objeto da lista.
		/// </summary>
		private GameObject _requestAllBudgetButton;

		/// <summary>
		/// Prefab do painel de repair shop.
		/// </summary>
		public GameObject RepairShopPanelPrefab;
		/// <summary>
		/// Prefab do ultimo item da lista, com o botao para selecionar todas as oficinas.
		/// </summary>
		public GameObject LastItemPrefab;
		/// <summary>
		/// View contendo a lista escrolavel de oficinas.
		/// </summary>
		public GameObject UIRepairShopView;
		/// <summary>
		/// 
		/// </summary>
		public GameObject DraggableCameraObject;
		/// <summary>
		/// Transform onde serao instanciados os paineis das oficinas.
		/// </summary>
		public Transform ListHolder;
		/// <summary>
		/// A view a ser ativada quando um botao é pressionado.
		/// </summary>
		public IView ContactRepairShopview;



		/// <summary>
		/// UnityMonoBehaviour Awake: roda antes de toda a inicialização deste script.
		/// </summary>
		public void Awake()
		{
			_repairShopPanels = new List<RepairShopItemPanel>();
			_repairShopModelList = new List<RepairShop>();
		}



		/// <summary>
		/// Ativa esta view e inicializa seus elementos.
		/// </summary>
		public override void ActivateView()
		{
			base.ActivateView();
			//
			// Ativa a view dragavel.
			UIRepairShopView.SetActive(true);
			//
			// Populates the list.
			StartCoroutine(PopulateList());
		}



		/// <summary>
		/// Ao desativar esta view.
		/// </summary>
		public override void DeactivateView()
		{
			base.DeactivateView();
			//
			// Stop this coroutine.
			StopCoroutine(PopulateList());
			//
			// Desativa a view dragavel.
			UIRepairShopView.SetActive(false);
		}



		/// <summary>
		/// Volta para a tela anterior.
		/// </summary>
		public override void Back()
		{
			base.Back();
		}



		/// <summary>
		/// Verifica se ha pelomenos um checkmark ativo para habilitar o botao, caso contrario mantém ele desabilitado.
		/// </summary>
		public void OnAnyCheckMarkClicked()
		{
			foreach (var v in _repairShopPanels)
			{
				if (v.IsCheck)
				{
					ApplicationController.Instance.SelectedRepairShop=v.Model;
					_requestAllBudgetButton.SetActive(true);
					return;
				}
			}
			_requestAllBudgetButton.SetActive(false);
		}



		/// <summary>
		/// Quando se faz a requisição de varias oficinas.
		/// </summary>
		public void OnRequestPrices()
		{
			//Debug.Log("OnRequestPrices clicked");
			if (ApplicationController.Instance.SelectedRepairShop!=null)
			{
				ContactRepairShopview.ActivateView();
			}

		}




		/// <summary>
		/// Inicializa as listas de oficinas.
		/// </summary>
		/// <returns>Ienumerator da corotina.</returns>
		private IEnumerator PopulateList()
		{
			LoadingView.Instance.Enable();
			//
			// Desliga a camera para que o usuario nao observe a remocao dos itens de revisao antigos.
			DraggableCameraObject.GetComponent<Camera>().enabled = false;
			yield return null;
			//
			// Limpa a lista de repairShop models.
			_repairShopModelList.Clear();
			//
			// Remove todos os itens na lista de paineis
			_repairShopPanels.Clear();
			//
			// Garante que nenhum item esteja no transform onde serao instanciados os novos itens.
			foreach (Transform v in ListHolder)
			{
				GameObject.Destroy(v.gameObject);
			}
			// 
			// Agora que nao possui mais itens na lista nem no transform liga a camera para que o usuario acompanhe o carregamento de itens.
			DraggableCameraObject.GetComponent<Camera>().enabled = true;
			yield return null;
			//
			// inicia o carregamento do banco de dados 
			JSONNode node = new JSONClass();
			{
				//
				// Este bloco é o bloco de placeholder, deverá ser removido quando a função final no banco de dados ja existir.
				// aqui serao carregados na mao um por um dos elementos cadastrados.
				// o que será recebido na requisição final será o json node montado neste bloco.

				//
				// [22:30:22] Marcos Arantes da Silva: repair-shops -> busca todas as oficinas
				// [22:30:38] Marcos Arantes da Silva: repair-shops/lat/long -> busca as oficinas proximas ao lat long passados.
				for (int i = 1; i < 5; i++)
				{
					//
					// inicializando os forms.
					var form = new WWWForm();
					form.AddField(Strings.Id, i);
					//
					// monta a requisicao com url e formulario.
					var request = new WWW(URLs.BaseURL + URLs.Slash + URLs.RepairShop + URLs.Slash + ApplicationController.Instance.CurrentUser.CurrentLocation.latitude+ URLs.Slash + ApplicationController.Instance.CurrentUser.CurrentLocation.longitude, form);
					//
					// espera pela requisicao.
					yield return request;
					//	Debug.Log(request.text);
					//
					// adiciona o resultado no json... 
					node.Add(JSON.Parse(request.text));

				}
			}// fim do bloco de placeholder
			 //
			 // parse do json recebido do banco de dados.
			for (int i = 0; i < node.Count; i++)
			{
				yield return null;
				RepairShop r = RepairShop.CreateRepairShop(node[i]);

				if (r == null)
				{
					continue;
				}

				_repairShopModelList.Add(r);

				GameObject panel = Object.Instantiate(RepairShopPanelPrefab);

				panel.GetComponent<RepairShopItemPanel>().Set(r, ContactRepairShopview);

				_repairShopPanels.Add(panel.GetComponent<RepairShopItemPanel>());

				panel.transform.SetParent(ListHolder);
				panel.transform.localScale = Vector3.one;

				panel.GetComponent<UIDragCamera>().draggableCamera = DraggableCameraObject.GetComponent<UIDraggableCamera>();

				EventDelegate.Add(panel.transform.FindChild("Checkbox").gameObject.GetComponent<UIButton>().onClick, OnAnyCheckMarkClicked);

				ListHolder.GetComponent<UITable>().repositionNow = true;
			}

			GameObject lastItem = Object.Instantiate(LastItemPrefab);

			_requestAllBudgetButton = lastItem.GetComponentInChildren<UIButton>().gameObject;


			lastItem.transform.SetParent(ListHolder);
			lastItem.transform.localScale = Vector3.one;

			lastItem.GetComponent<UIDragCamera>().draggableCamera = DraggableCameraObject.GetComponent<UIDraggableCamera>();

			ListHolder.GetComponent<UITable>().repositionNow = true;

			_requestAllBudgetButton.SetActive(false);

			EventDelegate.Add(_requestAllBudgetButton.GetComponent<UIButton>().onClick, OnRequestPrices);

			yield return null;

			LoadingView.Instance.Disable();
		}

	}
}