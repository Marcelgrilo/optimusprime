﻿using UnityEngine;
using System.Collections;
using System;
using MotorTools.Data;

namespace MotorTools.Views
{
	public class WebcamView : Singleton<WebcamView>
	{

		public WebCamTexture WebcamTextureInstance
		{ get; set; }
		public WebCamTexture FrontWebcamTextureInstance
		{ get; set; }
		public WebCamTexture RearWebcamTextureInstance
		{ get; set; }

		public string ScreenShotType
		{ get; set; }

		//public UITexture InAppTexture;
		public MeshRenderer InAppMeshRenderer;

		private bool _isFront = false;
		private Texture2D _snapshot;
		private string _frontCamName = "";
		private string _rearCamName = "";
		private Quaternion baseRotation;


		public void Start()
		{
			WebCamDevice[] devices = WebCamTexture.devices;
			Debug.Log("nOfCameras: " + devices.Length);
			baseRotation = InAppMeshRenderer.transform.rotation;
			for (int i = 0; i < devices.Length; i++)
			{
				if (devices[i].isFrontFacing)
				{
					_frontCamName = devices[i].name;
					FrontWebcamTextureInstance = new WebCamTexture(_frontCamName);
				}

				if (!devices[i].isFrontFacing)
				{
					_rearCamName = devices[i].name;
					RearWebcamTextureInstance = new WebCamTexture(_rearCamName);

				}
			}
			//StartCoroutine(CoLateUpdate());

		}


		//IEnumerator CoLateUpdate()
		//{
		//	while (gameObject.activeSelf)
		//	{

		//		if (WebcamTextureInstance!=null)
		//		{
		//			yield return new WaitForEndOfFrame();
		//			if (WebcamTextureInstance.didUpdateThisFrame)
		//			{
		//				//InAppMeshRenderer.transform.rotation = baseRotation * Quaternion.AngleAxis(-WebcamTextureInstance.videoRotationAngle, Vector3.up);
		//				//yield return new WaitForEndOfFrame();
		//				//int needsToBeRotatedDegrees = WebcamTextureInstance.videoRotationAngle;
		//				InAppMeshRenderer.transform.eulerAngles = new Vector3(0, 0, -WebcamTextureInstance.videoRotationAngle);
		//			}
		//		}
		//		yield return new WaitForSeconds(1);
		//	}
		//}

		public void Enable()
		{
			PreviousIViewManager.Instance.CurrentView.Show(false);
			gameObject.GetComponent<Camera>().enabled = true;
			_isFront = false;
#if UNITY_EDITOR
			Invoke("TurnOnFrontCamera", 0.5f);
#else
			Invoke("TurnOnRearCamera", 0.5f);
#endif
		}

		public void Disable()
		{
			PreviousIViewManager.Instance.CurrentView.Show(true);
			WebcamTextureInstance.Stop();
			FrontWebcamTextureInstance.Stop();
			RearWebcamTextureInstance.Stop();
			gameObject.GetComponent<Camera>().enabled = false;
			StopAllCoroutines();
		}


		public void OnCaptureButton()
		{
			StartCoroutine(TakeSnapshot());
		}

		public void OnSwitchCameraButton()
		{
			WebcamTextureInstance.Stop();
			FrontWebcamTextureInstance.Stop();
			RearWebcamTextureInstance.Stop();
			_isFront=!_isFront;
			if (_isFront)
			{
				TurnOnFrontCamera();
			}
			else
			{
				TurnOnRearCamera();
			}
		}



		public IEnumerator TakeSnapshot()
		{
			Color32[] data = new Color32[WebcamTextureInstance.width * WebcamTextureInstance.height];

			//
			// tenta 60 vezes a captura.
			for (int i = 0; i < 60; i++)
			{
				yield return new WaitForEndOfFrame();
				if (WebcamTextureInstance.didUpdateThisFrame)
				{
					Debug.Log("WebcamTextureInstance.GetPixels32(data);");
					WebcamTextureInstance.GetPixels32(data);
					Debug.Log("_snapshot = new Texture2D(WebcamTextureInstance.width, WebcamTextureInstance.height, TextureFormat.RGB24, false);");
					_snapshot = new Texture2D(WebcamTextureInstance.width, WebcamTextureInstance.height, TextureFormat.RGB24, false);
					Debug.Log("_snapshot.SetPixels32(data);");
					_snapshot.SetPixels32(data);
					Debug.Log("byte[] bytes = _snapshot.EncodeToPNG();");
					byte[] bytes = _snapshot.EncodeToPNG();
					Debug.Log("string filename = Strings.ScreenShotName(WebcamTextureInstance.width, WebcamTextureInstance.height, ScreenShotType);");
					string filename = Strings.ScreenShotName(WebcamTextureInstance.width, WebcamTextureInstance.height, ScreenShotType);
					Debug.Log("////////////////////////////////////////////////\n>>>>>>>>>>>>>>>> System.IO.File.WriteAllBytes(  "+filename+" , bytes);");
					System.IO.File.WriteAllBytes(filename, bytes);
					Debug.Log(string.Format("Took screenshot to: {0}", filename));
					Disable();
					yield break;
				}
			}
			Disable();
			yield break;
		}


		public void TurnOnFrontCamera()
		{
			SelectDevices();
			StartCoroutine(TurnOn(_frontCamName, FrontWebcamTextureInstance));
		}



		public void TurnOnRearCamera()
		{
			SelectDevices();
			StartCoroutine(TurnOn(_rearCamName, RearWebcamTextureInstance));
		}



		public void SelectDevices()
		{
			WebCamDevice[] devices = WebCamTexture.devices;

			for (int i = 0; i < devices.Length; i++)
			{
				//Debug.Log("... " +i +" devices[i].name:" +devices[i].name);

				if (devices[i].isFrontFacing)
				{
					_frontCamName = devices[i].name;
					//	Debug.Log("that's a front camera .. " +_frontCamName);
				}

				if (!devices[i].isFrontFacing)
				{
					_rearCamName = devices[i].name;
					//	Debug.Log("that's a rear camera .. " +_rearCamName);
				}
			}
		}



		private IEnumerator TurnOn(string camName, WebCamTexture webcam)
		{
			yield return new WaitForEndOfFrame();
			gameObject.GetComponent<Camera>().enabled = false;
			yield return new WaitForEndOfFrame();
			gameObject.GetComponent<Camera>().enabled = true;
			yield return new WaitForEndOfFrame();
			yield return new WaitForEndOfFrame();
			if (camName == "")
			{ Debug.Log("no such camera as " +camName); yield break; }
			else
			{ Debug.Log("powering up this cam name: " +camName); }

#if UNITY_EDITOR
			// handle incredibly bizarre editor (on Mac, anyway) bug
			camName = "";
#endif

			yield return new WaitForEndOfFrame();
			WebcamTextureInstance = webcam;
			yield return new WaitForEndOfFrame();
			//Debug.Log(" cam Initialized ");
			// for me, setting requested sizes is a completely fail by Unity,
			// it does absolutely nothing, unfortunately.
			// just do not pass in anything, and the "Jason solution" below
			// will give you the actual sizes the camera device is producting

			//InAppTexture.material.mainTexture = WebcamTexture;
			//InAppTexture.Update();
			yield return new WaitForEndOfFrame();
			//	yield return new WaitForSeconds(2);
			WebcamTextureInstance.Play();
			yield return new WaitForEndOfFrame();

			//yield return new WaitForSeconds(2);
			InAppMeshRenderer.GetComponent<Renderer>().material.mainTexture = WebcamTextureInstance;
			yield return new WaitForEndOfFrame();

			if (WebcamTextureInstance.isPlaying)
				Debug.Log("isPlaying is now true ...........");

			yield return StartCoroutine(WorkAroundRisibleUnityBug());

			float scaleY = webcam.videoVerticallyMirrored ? -1.0f : 1.0f;
			if (_isFront)
				scaleY*=-1;
			//InAppMeshRenderer.transform.localScale = new Vector3(800, scaleY * 800, 0.0f);
			//float screenWidth = Screen.width>720?
			InAppMeshRenderer.transform.localScale = new Vector3(Screen.width, Screen.width*scaleY*((float) WebcamTextureInstance.height/(float) WebcamTextureInstance.width), 1.0f);
			Debug.Log(">#>#>#>>>> "+ (Screen.width*scaleY*(WebcamTextureInstance.height/WebcamTextureInstance.width)));
			int needsToBeRotatedDegrees;
			needsToBeRotatedDegrees = WebcamTextureInstance.videoRotationAngle;
			//Debug.Log("needsToBeRotatedDegrees ..........." + needsToBeRotatedDegrees);
			// Unity doco is, of course!, wrong, you need the negative of this
			InAppMeshRenderer.transform.eulerAngles = new Vector3(0, 0, -needsToBeRotatedDegrees);
			//InAppMeshRenderer.transform.rotation = baseRotation * Quaternion.AngleAxis(-WebcamTextureInstance.videoRotationAngle, Vector3.up);
			// Consider, similarly, using videoVerticallyMirrored here.
			// I've found it unnecessary. if needed, get scale, Y by -1, set scale.

		}



		private IEnumerator WorkAroundRisibleUnityBug()
		{
			while (WebcamTextureInstance.width < 100)
			{
				Debug.Log("the width/height values are not yet ready.");
				Debug.Log(WebcamTextureInstance.width +" " +WebcamTextureInstance.height);
				yield return new WaitForEndOfFrame();
			}

			Debug.Log("################the width/height values are now meaningful.");
			Debug.Log(WebcamTextureInstance.width +" " +WebcamTextureInstance.height);

			// YOU COULD NOW CORRECTLY SET ASPECT RATIOS, TRIM, ETC
			// for example hers's some code that deals with the very subtle problem of
			// "showing only a square" of the camera image.
			// note that everything about Unity tiling/etc is completely fucked-up, so this is tricky.
			// some shaders EFFECTIVELY DO NOT WORK when you change tiling
			// also editor work is basically impossible, 
			// you have to build to an android every time to check.
			CorrectlySetPlaneSoThatItShowsACorrectSquareImage(WebcamTextureInstance.width, WebcamTextureInstance.height);

			yield break;
		}



		private void CorrectlySetPlaneSoThatItShowsACorrectSquareImage(float incomingWidth, float incomingHeight)
		{
			// to use this routine.  the PLANE holding the texture should in fact BE SQUARE.
			// REPEAT  the PLANE holding the texture should in fact BE SQUARE.
			// so transform scale x/y will be the same in the editor.
			// also assume that tiling (scale) on the texture, STARTS AS 1/1 as normal.

			if (incomingWidth > incomingHeight)
			{
				// width is bigger than height. this is the usual case with say Android cameras
				// (even if it's a 'portrait' image: they come in rotated usually. do this first then rotate if needed)
				// we will "take" and show only a square portion of the image:

				float newTilingFactor = incomingHeight / incomingWidth;    // typically say 0.75

				Debug.Log(">>>>>>>>>>>>>usual situation, raw shape is landscape. newTilingFactor " +newTilingFactor);

				// Grid.hud.OK("newTilingFactor " +newTilingFactor);

				//InAppTexture.material.mainTextureScale = new Vector2(newTilingFactor, 1);
				InAppMeshRenderer.GetComponent<Renderer>().material.mainTextureScale = new Vector2(newTilingFactor, 1);

				//InAppMeshRenderer.transform.localScale = new Vector3(600, 600*(newTilingFactor>1 ? 1/newTilingFactor : newTilingFactor), 1);
			}
		}
	}
}
