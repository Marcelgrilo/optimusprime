﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DebugTest : MonoBehaviour
{
		public UIViewport vp;
		Text myText;
		// Use this for initialization
		void Start ()
		{
				myText = GetComponent<Text> ();
		}
	
		// Update is called once per frame
		void Update ()
		{
				myText.text = vp.TopOffset.ToString ();
		}
}
